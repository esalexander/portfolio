<!DOCTYPE html>
<html lang="es">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
  <title>Scrum - DeVS</title>

  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="<?php echo base_url(); ?>assets/css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
</head>
<body>
  <nav class="light-blue darken-3" role="navigation">
    <div class="nav-wrapper container"><a id="logo-container" href="<?php echo base_url(); ?>" class="brand-logo">SCRUM</a>
      <ul class="right hide-on-med-and-down">
        <li><a href="<?php echo base_url(); ?>welcome/">Inicio</a></li>
        <li><a href="<?php echo base_url(); ?>welcome/proyecto">Proyectos</a></li>
        <li><a href="<?php echo base_url(); ?>welcome/sprint_list">Sprints</a></li>
        <li><a href="<?php echo base_url(); ?>welcome/registrar">Registrar</a></li>
        <li><a class="btn modal-trigger waves-effect waves-light" data-target="modal1" >Login</a></li>
      </ul>

      <ul id="nav-mobile" class="side-nav">
        <li><a href="<?php echo base_url(); ?>welcome/">Inicio</a></li>
        <li><a href="#">Proyectos</a></li>
        <li><a href="#">Sprints</a></li>
        <li><a href="#">Registrar</a></li>
        <li><a href="#" class="btn modal-trigger" data-target="modal1">Login</a></li>
      </ul>
      <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
    </div>
  </nav>
  <div id="index-banner" class="parallax-container">
    <div class="section no-pad-bot">
      <div class="container">
        <br><br>
        <h1 class="header center light-text">SCRUM DEVS</h1>
        <div class="row center">
          <h5 class="header col s12 light">Scrum es un framework de buenas prácticas<br> para trabajar colaborativamente en un proyecto</h5>
        </div>
        <div class="row center">
          <a href="#" id="download-button" class="btn-large waves-effect waves-light orange lighten-1">INICIAR</a>
        </div>
        <br><br>

      </div>
    </div>
    <div class="parallax"><img src="<?php echo base_url(); ?>assets/img/header.jpg" alt="Unsplashed background img 1"></div>
  </div>


  <div class="container">
    <div class="section">

      <!--MAIN CONTENT-->
          <?php $this->load->view($main_content); ?>
      <!--MAIN CONTENT-->    


    </div>
    <br><br>
  </div>

  <footer class="page-footer blue-grey lighten-1">
    <div class="container">
      <div class="row">
        <div class="col l9 s12">
          <h5 class="white-text">SCRUM Framework</h5>
          <p class="grey-text text-lighten-4">En Scrum un proyecto se ejecuta en bloques temporales cortos y fijos (iteraciones que normalmente son de 2 semanas, aunque en algunos equipos son de 3 y hasta 4 semanas, límite máximo de feedback y reflexión). Cada iteración tiene que proporcionar un resultado completo, un incremento de producto final que sea susceptible de ser entregado con el mínimo esfuerzo al cliente cuando lo solicite.</p>


        </div>
        <div class="col l3 s12">
          <h5 class="white-text">Connect</h5>
          <ul>
            <li><a class="white-text" href="#!">Scrum.org</a></li>
            <li><a class="white-text" href="#!">scrumaster.com</a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
      Hecho por <a class="orange-text text-lighten-3" href="">ELiAS</a>
      </div>
    </div>
  </footer>


  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/materialize.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/init.js"></script>

  </body>
</html>
