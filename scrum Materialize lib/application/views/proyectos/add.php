 <div class="row">
        <div class="col l10  s12 m8 offset-l1">
          <div class="card  blue-grey lighten-5 accent-4 z-depth-5">
            <div class="card-content light-gray-text">
              <span class="card-title">Agregar un Proyecto</span>
              <p>I am a very simple card. I am good at containing small bits of information.
              I am convenient because I require little markup to use effectively.</p>

              
              <div class="row">
                <form class="col s12">

                  <div class="row">
                    <div class="input-field col s12">
                      <input id="first_name" type="text" class="validate">
                      <label for="first_name">Nombre</label>
                    </div>

                    <div class="input-field col s12">
                      <input id="last_name" type="text" class="validate">
                      <label for="last_name">Descripcion</label>
                    </div>
                  </div>

                  <div class="row">
                    <div class="input-field col s12">
                      <input id="fechainicial" type="text" class="datepicker">
                      <label for="fechainicial">Fecha Inicial</label>
                    </div>
                  </div>

                  <div class="row">
                    <div class="input-field col s12">
                      <input id="fechafinal" type="text" class="datepicker">
                      <label for="fechafinal">Fecha Final</label>
                    </div>
                  </div>

                  <div class="row">
                    <div class="file-field input-field">
                    <div class="btn">
                      <span>Imagen</span>
                      <input type="file">
                    </div>
                    <div class="file-path-wrapper">
                      <input class="file-path validate" type="text">
                    </div>
                  </div>
                  </div>

                </form>
              </div>
        


            </div>
            <div class="card-action">
              <div class=" row right">
                <a href="#">Guardar</a>
                <a href="#">Cancelar</a>
              </div>
              <br/><br/>
            </div>
          </div>
        </div>
</div>