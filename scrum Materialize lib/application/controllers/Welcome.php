<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{
		$data['main_content'] = 'inicio';
		$this->load->view('layout/main',$data);
		$this->load->view('usuarios/login');
	}

	public function registrar()
	{
		$data['main_content'] = 'usuarios/registrar';
		$this->load->view('layout/main',$data);	
	}

	public function proyecto()
	{
		
		$data['main_content'] = 'proyectos/cards';
		$this->load->view('layout/main',$data);
		$this->load->view('usuarios/login');	
	}

	public function proyecto_add()
	{
		$data['main_content'] = 'proyectos/add';
		$this->load->view('layout/main',$data);	
	}

	public function sprint_add()
	{
		$data['main_content'] = 'sprints/add';
		$this->load->view('layout/main',$data);	
	}

	public function sprint_list()
	{
		$data['main_content'] = 'sprints/list';
		$this->load->view('layout/main',$data);	
	}
}
