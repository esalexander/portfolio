<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex, nofollow">

    <title>Bali</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <style type="text/css">

/********************************/
/*       style webpage       */
/********************************/
body {
	background-color: #EEE;
}

.thumbnail > img{
	height: 350px !important;
}

hr{
	display: block;
    height: 1px;
    border: 0;
    border-top: 1px solid #ccc;
    margin: 2em 0;
    padding: 0;
}

.content-divider{
	height: 2px;
	border-top: 2px solid #EBEBEB;
	    border-top-width: 1px;
	    border-top-color: rgb(235, 235, 235);
	display: block;
	position: relative;
	top: 1px;
	width: 100%;
	margin-top: 3%
}

.card {
    /* Add shadows to create the "card" effect */
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
    transition: 0.3s;
}

/* On mouse-over, add a deeper shadow */
.card:hover {
    opacity: 0.3;
    box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
}

.spacing{
    margin: 20px 0px;
}

.feautured{
    display: block;
    margin: 20px 0px;
}

.caption .title{
	color: black;
}

.caption .subtitle{
	color: gray;
}

.caption .divider{
	border-bottom: 2px solid #eee;
	height: 20px;
	list-style: none outside none;
	width: 100%;
	clear: both;
}

.caption .precio{
	color: #c33f70;
	font-size: 2em;
    padding-left: 2px;
    font-weight: bold; 
}

.caption a:hover{
    text-decoration: none;
}

.caption .add{
    width: 50%;
	color:gray;
	font-size: 2em;
    padding-top: 0px;
    text-align: right;
    padding-right: 0px;
}

/********************************/
/*       Fade Bs-carousel       */
/********************************/
.fade-carousel {
    position: relative;
    height: 100vh;
}
.fade-carousel .carousel-inner .item {
    height: 100vh;
}
.fade-carousel .carousel-indicators > li {
    margin: 0 2px;
    background-color: #ffffff;
    border-color: #f3f3f3;
    opacity: .7;
}
.fade-carousel .carousel-indicators > li.active {
  width: 10px;
  height: 10px;
  opacity: 1;
}

/********************************/
/*          Hero Headers        */
/********************************/
.hero {
    position: absolute;
    top: 50%;
    left: 50%;
    z-index: 3;
    color: #fff;
    text-align: center;
    text-transform: uppercase;
    text-shadow: 1px 1px 0 rgba(0,0,0,.75);
      -webkit-transform: translate3d(-50%,-50%,0);
         -moz-transform: translate3d(-50%,-50%,0);
          -ms-transform: translate3d(-50%,-50%,0);
           -o-transform: translate3d(-50%,-50%,0);
              transform: translate3d(-50%,-50%,0);
}
.hero h1 {
    font-size: 6em;    
    font-weight: bold;
    margin: 0;
    padding: 0;
}

.fade-carousel .carousel-inner .item .hero {
    opacity: 0;
    -webkit-transition: 2s all ease-in-out .1s;
       -moz-transition: 2s all ease-in-out .1s; 
        -ms-transition: 2s all ease-in-out .1s; 
         -o-transition: 2s all ease-in-out .1s; 
            transition: 2s all ease-in-out .1s; 
}
.fade-carousel .carousel-inner .item.active .hero {
    opacity: 1;
    -webkit-transition: 2s all ease-in-out .1s;
       -moz-transition: 2s all ease-in-out .1s; 
        -ms-transition: 2s all ease-in-out .1s; 
         -o-transition: 2s all ease-in-out .1s; 
            transition: 2s all ease-in-out .1s;    
}

/********************************/
/*            Overlay           */
/********************************/
.overlay {
    position: absolute;
    width: 100%;
    height: 100%;
    z-index: 2;
    background-color: #080d15;
    opacity: .7;
}

/********************************/
/*          Custom Buttons      */
/********************************/
.btn.btn-lg {padding: 10px 40px;}
.btn.btn-hero,
.btn.btn-hero:hover,
.btn.btn-hero:focus {
    color: #f5f5f5;
    background-color: transparent;
    border-color: #ffffff;
    outline: none;
    margin: 20px auto;
}

/********************************/
/*       Slides backgrounds     */
/********************************/
.fade-carousel .slides .slide-1, 
.fade-carousel .slides .slide-2,
.fade-carousel .slides .slide-3 {
  height: 100vh;
  background-size: cover;
  background-position: center center;
  background-repeat: no-repeat;
}
.fade-carousel .slides .slide-1 {
  background-image: url(http://localhost/bali/assets/img/slide1.jpg); 
}
.fade-carousel .slides .slide-2 {
  background-image: url(http://localhost/bali/assets/img/slide2.png);
}
.fade-carousel .slides .slide-3 {
  background-image: url(http://localhost/bali/assets/img/slide3.png);
}

/********************************/
/*          Media Queries       */
/********************************/
@media screen and (min-width: 980px){
    .hero { width: 980px; }    
}
@media screen and (max-width: 640px){
    .hero h1 { font-size: 4em; }    
}
    </style>
    <script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        window.alert = function(){};
        var defaultCSS = document.getElementById('bootstrap-css');
        function changeCSS(css){
            if(css) $('head > link').filter(':first').replaceWith('<link rel="stylesheet" href="'+ css +'" type="text/css" />'); 
            else $('head > link').filter(':first').replaceWith(defaultCSS); 
        }
        $( document ).ready(function() {
          var iframe_height = parseInt($('html').height()); 
          window.parent.postMessage( iframe_height, 'https://bootsnipp.com');
        });
    </script>
</head>
<body>

<!-- Navigation 
navbar navbar-default navbar-static-top
navbar navbar-inverse navbar-fixed-top
-->
    <nav class="navbar navbar-default bg-faded navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">BALI</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="#">About</a>
                    </li>
                    <li>
                        <a href="#">Services</a>
                    </li>
                    <li>
                        <a href="#">Contact</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

 <div class="container-fluid" style="width: 100% !important; padding-left: 0px!important; padding-right: 0px !important;">
	<div class="carousel fade-carousel slide" data-ride="carousel" data-interval="4000" id="bs-carousel">
  <!-- Overlay -->
  <div class="overlay"></div>

  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#bs-carousel" data-slide-to="0" class="active"></li>
    <li data-target="#bs-carousel" data-slide-to="1"></li>
    <li data-target="#bs-carousel" data-slide-to="2"></li>
  </ol>
  
  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <div class="item slides active">
      <div class="slide-1"></div>
      <div class="hero">
        <hgroup>
            <h1>We are creative</h1>        
            <h3>Get start your next awesome project</h3>
        </hgroup>
        <button class="btn btn-hero btn-lg" role="button">See all features</button>
      </div>
    </div>
    <div class="item slides">
      <div class="slide-2"></div>
      <div class="hero">        
        <hgroup>
            <h1>We are smart</h1>        
            <h3>Get start your next awesome project</h3>
        </hgroup>       
        <button class="btn btn-hero btn-lg" role="button">See all features</button>
      </div>
    </div>
    <div class="item slides">
      <div class="slide-3"></div>
      <div class="hero">        
        <hgroup>
            <h1>We are amazing</h1>        
            <h3>Get start your next awesome project</h3>
        </hgroup>
        <button class="btn btn-hero btn-lg" role="button">See all features</button>
      </div>
    </div>
  </div> 
</div>
</div>

<!-- Page Content -->
    <div class="container">
    
    <div class="row spacing">
    <div class="col-md-4 col-sm-3" >
        <div class="card">
          <img src="http://localhost/bali/assets/img/bali.jpg" alt="Avatar" style="width:100%">
          
        </div> 
    </div>
        
        <div class="col-md-4 col-sm-3" >
        <div class="card">
          <img src="http://localhost/bali/assets/img/bali.jpg" alt="Avatar" style="width:100%">
          
        </div> 
    </div>

    <div class="col-md-4 col-sm-3" >
        <div class="card">
          <img src="http://localhost/bali/assets/img/bali.jpg" alt="Avatar" style="width:100%">
          
        </div> 
    </div>
    </div>



    <div class="row feautured">
    	<div class="col-md-3">
    		<h4>FEAUTURED PRODUCTS</h4>
    		<h5>The products that are selected</h5>
    	</div>
    	<div class="col-md-9">
    	<hr />
    	
    	</div>
    	
    </div>

<!-- Page Features -->
        <div class="row text-center">

            <div class="col-md-4 col-sm-6 hero-feature">
                <div class="thumbnail">
                    <img src="http://placehold.it/800x500" alt="">
                    <div class="caption">
                        <h3 class="title">Feature Label</h3>
                        <p class="subtitle">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                        <div class="divider"></div>

                        <div class="footer-card">
                        <div class="pull-left">
                            <a href="#" class="precio">$145</a>
                        </div>

                        <div class="pull-rigth">
                            <a href="#" class="add pull-rigth btn btn-deadd-to-cart-btn add-to-cart glyphicon glyphicon-plus-sign"></a>
                        </div>                        
                             
                        </div>
                        <!--<p>
                            <a href="#" class="precio">$145</a> <a href="#" class="add pull-rigth btn btn-deadd-to-cart-btn add-to-cart glyphicon glyphicon-plus-sign"></a>
                        </p>-->
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-sm-6 hero-feature">
                <div class="thumbnail">
                    <img src="http://placehold.it/800x500" alt="">
                    <div class="caption">
                         <h3 class="title">Feature Label</h3>
                        <p class="subtitle">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                        <div class="divider"></div>

                        <div class="footer-card">
                        <div class="pull-left">
                            <a href="#" class="precio">$145</a>
                        </div>

                        <div class="pull-rigth">
                            <a href="#" class="add pull-rigth btn btn-deadd-to-cart-btn add-to-cart glyphicon glyphicon-plus-sign"></a>
                        </div>                        
                             
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-sm-6 hero-feature">
                <div class="thumbnail">
                    <img src="http://placehold.it/800x500" alt="">
                    <div class="caption">
                         <h3 class="title">Feature Label</h3>
                        <p class="subtitle">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                        <div class="divider"></div>

                        <div class="footer-card">
                        <div class="pull-left">
                            <a href="#" class="precio">$145</a>
                        </div>

                        <div class="pull-rigth">
                            <a href="#" class="add pull-rigth btn btn-deadd-to-cart-btn add-to-cart glyphicon glyphicon-plus-sign"></a>
                        </div>                        
                             
                        </div>
                    </div>
                </div>
            </div>

            

        </div>
        <!-- /.row -->

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Your Website 2017</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->
	<script type="text/javascript">
	
	</script>
</body>
</html>
