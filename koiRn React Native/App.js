/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
} from 'react-native';

import {Root, MainScreenNavigator} from './config/router';

class App extends Component{
  render(){
    return <MainScreenNavigator />
  }
}

export default App;
