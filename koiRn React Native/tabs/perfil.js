import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Image,
    Text,
    Alert,
    Button,
    AppRegistry,
    TouchableOpacity
} from 'react-native';

import { StackNavigator } from 'react-navigation';


export default class Perfil extends Component{

    constructor(props){
        super(props);
    }

    _onPressButton = () =>  {
        this.props.navigation.navigate('login');
        
    }

    render(){
        return (
            <View style={ styles.container } >
                <Image
                style={styles.imagePerfil}
                source={require('../src/img/img_perfil.png')} />

                <TouchableOpacity
                    onPress={this._onPressButton}
                > 
                <Image 
                    style={styles.buttonPerfil} 
                    source={require('../src/img/btn_verperfil.png')} /> 
                </TouchableOpacity>
                
            </View>

        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: '#fff'
    },
    imagePerfil:{
        width: null,
        height: 140,
        margin:30
    },
    buttonPerfil: {
        width:350,
        height: 50,
        alignSelf: 'center'
    }
}
);

AppRegistry.registerComponent('Inicio',() => Inicio);