/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image
} from 'react-native';

import Screen from '../src/components/social/Screen';

export default class Social extends React.Component {

    static navigationOptions ={
        tabBarLabel: 'Social',
        tabBarIcon: ({tintColor}) => (
            <Image 
                source={require('../src/icons/socialicon.png')}
                style={[styles.icon,{tintColor:'white'}]}
            />
        ),
    }

  render(){
    return <Screen />
  }

}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        fontSize: 30,
    },
    icon:{
        width:18,
        height:18,
    }

});

