import React, {Component} from 'react';
import {
    View,
    Image,
    Text,
    StyleSheet,
    ActivityIndicator,
    FlatList,
    TouchableOpacity
} from 'react-native';



class Menu extends Component{

    constructor(){
        super();
        this.state = {
            isLoading: true,
            data:[
                {'id': 1, 'categoria': 'ENTRADAS','content':'Menu de entradas', 'image': require('../src/img/menu/entradas_mn.png') },
                {'id': 2, 'categoria': 'Washimis','content':'Menu de entradas', 'image': require('../src/img/menu/washimi_mn.png') },
                {'id': 3, 'categoria': 'Spicy Washimis','content':'Menu de entradas', 'image': require('../src/img/menu/spicywa_mn.png') },
                {'id': 4, 'categoria': 'Sashimis','content':'Menu de entradas' , 'image':require('../src/img/menu/sashimi_mn.png')},
                {'id': 5, 'categoria': 'Nigiris','content':'Menu de entradas' , 'image': require('../src/img/menu/nigiris_mn.png')}
            ],
        }
    }
  
    _onItemPress(){
        this.props.navigation.navigate('Categoria');
    }

    _renderMessage(item){
        return(
        <TouchableOpacity style={styles.message} onPress={() => this._onItemPress()}>  
        <Image style = {styles.headerBackground} source={item.image} >
        </Image>
            <View style={styles.header}>

            <Text style={styles.txtTitle} > {item.categoria} </Text>
            <Text style={styles.txtSubtitle} > {item.content} </Text>

            </View>

        
        </TouchableOpacity>
        )

    }

    render(){
        return (
            <View style={styles.flat}>
            <FlatList
                style={styles.main}
                keyExtractor = {(x,i) => i}
                data = {this.state.data}
                renderItem = { ({item}) => this._renderMessage(item) }
            />
                
            </View>


        );
    }
}

const styles = StyleSheet.create({
    main:{

    },
    image:{
        width: null,
        height: 140,
        margin:30
    },
    headerBackground: {
        width: 100+ '%',
        position: "absolute",
        top: 0, left: 0, bottom: 0, right: 0,
      },
    header:{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 20,
        backgroundColor: 'rgba(0,0,0,0.5)' 
    },
    message:{
        width: 100+ '%',
        borderBottomWidth: 1,
        borderColor: 'rgb(71,77,89)'
    },
    txtTitle:{
        color: 'white',
        fontFamily: 'helvetica',
        fontSize: 14,
        margin: 5,
        fontWeight: 'bold',
    },
    txtSubtitle:{
        color: 'white',
        fontFamily: 'helvetica',
        fontSize: 12,
        margin: 5,
    },
    flat:{
        width: 100+ '%',
        height: 100+'%'
    }


});

export default Menu;