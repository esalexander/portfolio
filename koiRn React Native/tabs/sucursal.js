import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  FlatList,
  Image,
  Alert,
  TouchableOpacity
} from 'react-native';

const data_sucursal =[
    {
        key: 1,
        name: 'KOI Plaza Madero',
        direccion: 'Centro Comercial Las Azaleas',
        image: 'http://lafabricadbm.net/img/react/icon_las_azaleas.png'
    },
    {
        key: 2,
        name: 'KOI Las Azaleas',
        direccion: 'Centro Comercial Las Azaleas',
        image: 'http://lafabricadbm.net/img/react/icon_las_azaleas.png'
    },
    {
        key: 3,
        name: 'KOI Plaza Futura',
        direccion: 'Centro Comercial Las Azaleas',
        image: 'http://lafabricadbm.net/img/react/icon_las_azaleas.png'
    },
    {
        key: 4,
        name: 'KOI Santa Rosa ',
        direccion: 'Centro Comercial Las Azaleas',
        image: 'http://lafabricadbm.net/img/react/icon_las_azaleas.png'
    },
];


export default class Sucursal extends Component {

    constructor(props){
        super(props)
    }

    _renderItem(item){
        return (
            <TouchableOpacity style={styles.containerRow} onPress={() => this._detailSucursal()}>
            <Image 
                style = {styles.imageItem} source = {{uri: item.image}}
             />
                <View style={styles.containerCol}>
                <Text>{item.name}</Text>
                <Text>{item.direccion}</Text>
                </View>
            
            </TouchableOpacity>
        );
    };

    _detailSucursal = () => {
        this.props.navigation.navigate('single');
    };

    renderSeparator = () => {
        return (
          <View
            style={{
              height: 1,
              width: "86%",
              backgroundColor: "#CED0CE",
              marginLeft: "14%"
            }}
          />
        );
      };

  render() {
    return (
      <View style={styles.container}>
     
     <View>
        <View>
            <Image
            style={styles.imageSucursal}
            source={require('../src/img/slide_sucursales.png')} />
        </View>
        
        <FlatList 
                ItemSeparatorComponent={this.renderSeparator}
                renderItem = {({item}) => this._renderItem(item) }
                keyExtractor = {(x,i) => i}
                data = { data_sucursal }
        />
        </View>
        
        

      
      </View>
    );
  }
}

const styles = StyleSheet.create({
    container: {
      flex:1,
    },
    containerRow:{
        padding: 18,
        flexDirection: 'row'
    },
    containerCol:{
        padding: 18,
        flexDirection: 'column'
    },
    imageItem:{
        width:105, 
        height: 105,
        alignSelf: 'stretch'
    },
    title: {
      color: '#ffffff'
    },
    imageSucursal:{
        width: null,
        alignSelf: 'auto'
    },
    imagen: {
        width: null,
        height: 24,
    }
    
});