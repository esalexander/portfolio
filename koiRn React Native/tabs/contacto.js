/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image
} from 'react-native';

import List from '../src/components/contacto/list'

export default class Contacto extends React.Component {

    static navigationOptions ={
        tabBarLabel: 'Contacto',
        tabBarIcon: ({tintColor}) => (
            <Image 
                source={require('../src/icons/contacticon.png')}
                style={[styles.icon,{tintColor:'white'}]}
            />
        ),
    }

  render(){
    return <List />
  }

}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        fontSize: 30,
    },
    icon: {
        width: 24,
        height: 24,
    },

});

