import React from 'react';
import {
    StyleSheet, Image
} from 'react-native';
import { TabNavigator, StackNavigator } from 'react-navigation';

import Menu from '../tabs/menu';
import Categoria from '../src/components/categoria'
import Sucursal from '../tabs/sucursal';
import Contacto from '../tabs/contacto';
import Social from '../tabs/social';
import Perfil from '../tabs/perfil';

import singleSucursal from '../src/components/sucursal/single';

import loginPerfil from '../src/components/perfil/login';

export const menuStack = StackNavigator({
    Menu: {
        screen: Menu,
        navigationOptions: ({ navigation }) => ({
            title: 'Menu',
        }),
    },
    Categoria: {
        screen: Categoria,
        navigationOptions: ({ navigation }) => ({
            title: 'Producto',
        }),
    },
});

export const sucursalStack = StackNavigator({
    Tab2:{
        screen: Sucursal,
        navigationOptions: ({ navigation }) => ({
            title: 'Sucursal',
      }),
    },
    single: {
        screen: singleSucursal,
        navigationOptions: ({ navigation }) => ({
            title: 'Sucursal',
      }),
        
    }

});

export const contactoStack = StackNavigator({
    Tab3:{
        screen: Contacto,
        navigationOptions: ({ navigation }) => ({
            title: 'Contacto',
      }),
    },
});

export const SocialStack = StackNavigator({
    Tab4:{
        screen: Social,
        navigationOptions: ({ navigation }) => ({
            title: 'Social',
      }),
    },
});

export const perfilStack = StackNavigator({
    Tab5:{
        screen: Perfil,
        navigationOptions: ({ navigation }) => ({
            title: 'Perfil',
      }),
    },
    login : {
        screen: loginPerfil,
        navigationOptions: ({ navigation }) => ({
            title: 'Login',
      }),
    }

});

export const MainScreenNavigator = TabNavigator({
    Menu: {
        screen: menuStack,
        navigationOptions: {
            tabBarIcon: ({tintColor}) => (
                <Image 
                    source={require('../src/icons/menuicon.png')}
                    style={[styles.icon,{tintColor:'white'}]}
                />
            ),
        }
    },
    Sucursal: {
        screen: sucursalStack,
        navigationOptions: {
            tabBarIcon: ({tintColor}) => (
                <Image 
                    source={require('../src/icons/sucursalesicon.png')}
                    style={[styles.icon,{tintColor:'white'}]}
                />
            ),
    
        }
    },
    Contacto: {
        screen: contactoStack
    },
    Social: {
        screen: SocialStack
    },
    Perfil: {
        screen: perfilStack,
        navigationOptions: {
            tabBarIcon: ({tintColor}) => (
                <Image 
                    source={require('../src/icons/usericon.png')}
                    style={[styles.icon,{tintColor:'white'}]}
                />
            ),
        }
    }
},
{
 tabBarPosition:'bottom',
 swipeEnabled: true,
 tabBarOptions: {
   showIcon: true,
    activeTintColor:'white',
    labelStyle: {
      fontSize: 8,
      padding:0,
    },
    style: {
      backgroundColor: 'red',
    },
 }
});

export const Root = StackNavigator({
    Tabs:{
        screen: MainScreenNavigator,
    },
});


const styles = StyleSheet.create({
        icon:{
            width:24,
            height:24,
        }
    
});