import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    View,
    Image,
    Text,
    TextInput,
    Button,
    TouchableOpacity,
    ScrollView
} from 'react-native';

import Container from './Container';


export default class Login extends Component{
    render(){
        return (
            <View style={ styles.container } >
                <Image
                style={styles.imagePerfil}
                source={require('../../img/rollwith.png')} />

                <ScrollView style={styles.scroll}>

                <Container>
                <TextInput
                    placeholder='Usuario'
                    style={styles.textInput}
                />
                </Container>
                <Container>
                    <TextInput
                        placeholder='Password'
                        secureTextEntry={true}
                        style={styles.textInput}
                    />
                </Container>

                <TouchableOpacity
                    onPress={this._onPressButton}
                > 
                <Image 
                    style={styles.buttonPerfil} 
                    source={require('../../img/btn_entrar.png')} /> 
                </TouchableOpacity>

                <TouchableOpacity
                    onPress={this._onPressButton}
                > 
                <Image 
                    style={styles.buttonPerfil} 
                    source={require('../../img/btn_registrar.png')} /> 
                </TouchableOpacity>

                
                </ScrollView>

                
            </View>

        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: '#fff'
    },
    imagePerfil:{
        width: null,
        height: 40,
        margin:30
    },
    buttonPerfil: {
        width:350,
        height: 50,
        alignSelf: 'center',
        marginBottom: 5,
    },
    scroll: {
        backgroundColor: 'white',
        padding: 30,
        flexDirection: 'column'
    },
    textInput: {
        height: 80,
        fontSize: 20,
        backgroundColor: '#FFF'
    },
    
}
);

AppRegistry.registerComponent('Login',() => Login);