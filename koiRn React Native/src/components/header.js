import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Image,
    Text,
    TouchableHighlight
} from 'react-native';




export default class Header extends React.Component{
    render(){
        return (
            <View style={ styles.container } >
                <Image
                style={styles.imagePerfil}
                source={require('../img/img_perfil.png')} />

                <TouchableHighlight
                     
                    > 
                <Image 
                    style={styles.buttonPerfil} 
                    source={require('../img/btn_verperfil.png')} /> 
                </TouchableHighlight>
                
            </View>

        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: '#fff'
    },
    imagePerfil:{
        width: null,
        height: 140,
        margin:30
    },
    buttonPerfil: {
        width:350,
        height: 50,
        alignSelf: 'center'
    }
}
);