import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Image,
    Text,
    Linking,
    Alert,
    TouchableOpacity
} from 'react-native';



class BtnSocial extends Component {
    constructor(props){
        super(props);
    }

    _handlePress = () => {
        Alert.alert('Abrir url red social' );
    }

    render (){
        return (
            <View>
                <TouchableOpacity
                    onPress={this._handlePress}
                > 
                <Image 
                    style={styles.buttonSocial} 
                    source={this.props.perfil} /> 
                </TouchableOpacity>
            </View>
        );
    }
        
}


const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: '#fff'
    },
    imageSocial:{
        width: null,
        height: 140,
    },
    buttonSocial: {
        width:360,
        height: 60,
        alignSelf: 'center',
        margin:10
    }
}
);

export default BtnSocial;