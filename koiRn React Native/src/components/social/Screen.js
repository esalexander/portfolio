import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Image,
    Text,
    Linking,
    TouchableHighlight,
    ScrollView
} from 'react-native';

import BtnSocial from './btnsocial';


export default class Screen extends React.Component{
    
    render(){
        return (
            <View style={ styles.container } >
                <Image
                style={styles.imageSocial}
                source={require('../../img/slide_social.png')} />

                <ScrollView>

                <BtnSocial perfil={require('../../img/btn_facebook.png')} />

                <BtnSocial perfil={require('../../img/btn_twitter.png')} />

                <BtnSocial perfil={require('../../img/btn_instagram.png')} />

                <BtnSocial perfil={require('../../img/btn_youtube.png')} />

                <BtnSocial perfil={require('../../img/btn_tripadvisor.png')} />

                </ScrollView>
                
            </View>

        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: '#fff'
    },
    imageSocial:{
        width: null,
        height: 140,
    },
    buttonSocial: {
        width:360,
        height: 60,
        alignSelf: 'center',
        margin:10
    }
}
);