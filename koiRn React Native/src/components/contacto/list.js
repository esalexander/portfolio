import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  FlatList,
  Image
} from 'react-native';

const show_first =[
    {
        key: 1,
        name: 'KOI Plaza Madero',
        tel: '2246-0193',
        image: 'http://lafabricadbm.net/img/react/icon_plazafutura.png'
    },
    {
        key: 2,
        name: 'KOI Las Azaleas',
        tel: '2263-722',
        image: 'http://lafabricadbm.net/img/react/icon_plazafutura.png'
    },
    {
        key: 3,
        name: 'KOI Plaza Futura',
        tel: '2264-4292',
        image: 'http://lafabricadbm.net/img/react/icon_plazafutura.png'
    },
    {
        key: 4,
        name: 'KOI Santa Rosa ',
        tel: '2525-7505',
        image: 'http://lafabricadbm.net/img/react/icon_plazafutura.png'
    },
];


export default class List extends Component {

    constructor(props){
        super(props)
    }

    _renderItem(item){
        return (
            <View style={styles.containerRow}>
            <Image style = {styles.imageItem} source = {{uri: item.image}} />
                <View style={styles.containerCol}>
                <Text>{item.name}</Text>
                <Text style={styles.txtTel}>{item.tel}</Text>
                </View>
            
            </View>
        );
    };

    renderSeparator = () => {
        return (
          <View
            style={{
              height: 1,
              width: "86%",
              backgroundColor: "#CED0CE",
              marginLeft: "14%"
            }}
          />
        );
      };

  render() {
    return (
      <View style={styles.container}>
        <View>
        <Image
                style={styles.imageSucursal}
                source={require('../../img/slide_contacto.png')} />
        
        <FlatList 
                ItemSeparatorComponent={this.renderSeparator}
                renderItem = {({item}) => this._renderItem(item) }
                data = { show_first }
        />
        
        </View>

      
      </View>
    );
  }
}

const styles = StyleSheet.create({
    container: {
      flex:1,
    },
    containerRow:{
        padding: 18,
        flexDirection: 'row'
    },
    containerCol:{
        padding: 18,
        flexDirection: 'column'
    },
    imageItem:{
        width:105, 
        height: 105,
        alignSelf: 'stretch'
    },
    txtTel: {
      color: 'red'
    },
    imageSucursal:{
        width: null,
        alignSelf: 'auto'
    },
    imagen: {
        width: null,
        height: 24,
    }
    
});