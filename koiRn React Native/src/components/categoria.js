import React, {Component} from 'react';
import {
    View,
    Image,
    Text,
    Alert,
    StyleSheet,
    ActivityIndicator,
    FlatList,
    TouchableOpacity
} from 'react-native';



class Categoria extends Component{

    constructor(){
        super();
        this.state = {
            isLoading: true,
            data:[],
        }
    }

    componentDidMount(){
        this.fetchData();
    }

    fetchData = async () => {
        const response = await fetch("https://randomuser.me/api?results=500");
        const json = await response.json();
        this.setState({ data: json.results, isLoading: false });
    };
 

    _onItemPress(){
        Alert.alert('ver detalle de producto');
    }

    _renderMessage(item){
        return(
            <TouchableOpacity style={styles.containerRow} onPress={() => this._onItemPress()}>
            <Image 
                style = {styles.imageItem} source = {{uri: item.picture.large}}
            />
                <View style={styles.containerCol}>
                <Text>{`${item.name.first} ${item.name.last}`}</Text>
                <Text>{`${item.location.city}`}</Text>
                </View>

            </TouchableOpacity>
        )

    }

    render(){
        if (this.state.isLoading) {
            return (
                <View style={{flex: 1, paddingTop: 20}}>
                    <ActivityIndicator />
                </View>
            );
        }

        return (

            <View style={styles.flat}>
            <FlatList
                style={styles.main}
                keyExtractor = {(x,i) => i}
                data = {this.state.data}
                renderItem = { ({item}) => this._renderMessage(item) }
            />
                
            </View>


        );
    }
}

const styles = StyleSheet.create({
    main:{

    } ,
    message:{
       width: 100+ '%',
        borderBottomWidth: 1,
        borderColor: 'rgb(71,77,89)'
    },
    txt:{
        color: 'rgb(12,0,51)',
        fontFamily: 'helvetica',
        fontSize: 12,
        margin: 5,
    },
    flat:{
        width: 100+ '%',
        height: 100+'%'
    },

    containerRow:{
        padding: 18,
        flexDirection: 'row'
    },
    containerCol:{
        padding: 18,
        flexDirection: 'column'
    },
    imageItem:{
        width:85, 
        height: 85,
        borderRadius: 40,
        alignSelf: 'stretch'
    },
    title: {
      color: '#ffffff'
    },


});

export default Categoria;