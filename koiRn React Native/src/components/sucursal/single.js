import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Image,
    Text,
    ScrollView,
    FlatList,
    TouchableHighlight
} from 'react-native';

import Slide from './slider';

const data_horario =[
    {
        key: 1,
        name: 'KOI Plaza Madero',
        dias: 'Lunes A Jueves',
        horas: '12 pm- 3pm, 6pm - 10pm'
    },
    {
        key: 2,
        name: 'KOI Plaza Madero',
        dias: 'Viernes - Sabado',
        horas: '3pm - 11pm'
    },
    {
        key: 3,
        name: 'KOI Plaza Madero',
        dias: 'Domingo',
        horas: '12 pm- 3pm, 6pm - 10pm'
    },
];


export default class Single extends React.Component{
    constructor(props){
        super(props)
    }

    _renderItem(item){
        return (
            <View style={styles.containerRow}>
                <View style={styles.containerCol}>
                <Text>{item.dias}</Text>
                <Text>{item.horas}</Text>
                </View>
            
            </View>
        );
    };

    renderSeparator = () => {
        return (
          <View
            style={{
              height: 1,
              width: "86%",
              backgroundColor: "#CED0CE",
              marginLeft: "10%"
            }}
          />
        );
      };


    render(){
        return (
            <View style={ styles.container } >
                
                
                <Slide />
                <ScrollView >
                <TouchableHighlight 
                    > 
                <Image 
                    style={styles.buttonPerfil} 
                    source={require('../../img/btn_verperfil.png')} /> 
                </TouchableHighlight>

                <Text style={styles.textDireccion}>Boulevar Santa Elena, Antiguo Cuscatlan, La Libertad</Text>

                <View style={styles.tel}>
                    <Text style={styles.textTel}>22460193</Text>
                    <TouchableHighlight 
                        style={styles.buttonLlamar} 
                    > 
                    <Image 
                        
                        source={require('../../img/llamaricon.png')} /> 
                    </TouchableHighlight>
                    

                </View>

                <FlatList 
                ItemSeparatorComponent={this.renderSeparator}
                renderItem = {({item}) => this._renderItem(item) }
                keyExtractor = {(x,i) => i}
                data = { data_horario }
                />

                </ScrollView>
                
            </View>

        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: '#fff'
    },
    containerRow:{
        padding: 18,
        flexDirection: 'row'
    },
    containerCol:{
        padding: 18,
        flexDirection: 'column'
    },
    imageSingle:{
        width: null,
        height: 140,
        margin:30
    },
    buttonPerfil: {
        width:350,
        height: 50,
        alignSelf: 'center'
    },
    tel:{
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    textTel:{
        color: 'red',
        marginLeft:15
    },
    buttonLlamar: {
        width:60,
        height: 40,
        marginRight:15
        
    },
    textDireccion: {
        color: 'gray',
        padding: 18,
        flexDirection: 'column'
    }

}
);