import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image
} from 'react-native';
 
import Swiper from 'react-native-swiper';
 
var styles = StyleSheet.create({
  wrapper: {
  },
  slide1: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#9DD6EB',
  },
  slide2: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#97CAE5',
  },
  slide3: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#92BBD9',
  },
  text: {
    color: '#fff',
    fontSize: 30,
    fontWeight: 'bold',
  },
  image:{
    flex: 1,
    height:400
}
})
 
export default class extends Component{
  render() {
    return (
      <Swiper style={styles.wrapper} showsButtons={true} autoplay={true}>
        <View style={styles.slide1}>
        <Image style={ styles.image} source={require('../../img/slide_azaleas.png')} />
        </View>
        <View style={styles.slide2}>
        <Image style={ styles.image} source={require('../../img/slide_madero.png')} />
        </View>
        <View style={styles.slide3}>
        <Image style={ styles.image} source={require('../../img/slide_futura.png')} />
        </View>
      </Swiper>
    );
  }
}