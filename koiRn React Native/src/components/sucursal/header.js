import React, {Component} from 'react';
import {
    Text,
    View,
    StyleSheet,
    TouchableWithoutFeedback
} from 'react-native';


const styles = StyleSheet.create ({
    navBar:{
        backgroundColor: 'red',
        flexDirection: 'row',
        height: 60,
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    titleApp:{
        color: 'white',
        fontWeight: '400'

    }

});

class Header extends Component{
    render(){
        return (
            <View style={styles.navBar}>

            <Text style={styles.titleApp} >APP</Text>

            <Text style={styles.titleApp} >KOI SUSHI</Text>

            </View>
        );
    }
}

export default Header