$(document).ready(function(){
	var user_id = $('#h_v').attr("value");
	var meses = [];
	var likes = [];
	var talk = [];
	var alcance = [];
	$.ajax({
		url: "http://localhost/estadisticas/marcas/getchart",
		method: "POST",
		data: {user_id:user_id},
		dataType: "json",
		success: function(data) {
			//console.log(data);
			var len = data.length;

			for (var i = 0; i < len; i++) {
				meses.push(data[i].inicio_fecha);
				likes.push(data[i].num_likes);
				talk.push(data[i].num_talking);
				alcance.push(data[i].num_alcance)
			}

			var chartdata = {
				labels: meses,
				datasets : [
					{
						label: "Fans",
						fill: false,
						lineTension: 0.1,
						backgroundColor: "rgba(59, 89, 152, 0.75)",
						borderColor: "rgba(59, 89, 152, 1)",
						pointHoverBackgroundColor: "rgba(59, 89, 152, 1)",
						pointHoverBorderColor: "rgba(59, 89, 152, 1)",
						data: likes
					},
					{
						label: "talking",
						fill: false,
						lineTension: 0.1,
						backgroundColor: "rgba(29, 202, 255, 0.75)",
						borderColor: "rgba(29, 202, 255, 1)",
						pointHoverBackgroundColor: "rgba(29, 202, 255, 1)",
						pointHoverBorderColor: "rgba(29, 202, 255, 1)",
						data: talk
					},
					{
						label: "alcance",
						fill: false,
						lineTension: 0.1,
						backgroundColor: "rgba(211, 72, 54, 0.75)",
						borderColor: "rgba(211, 72, 54, 1)",
						pointHoverBackgroundColor: "rgba(211, 72, 54, 1)",
						pointHoverBorderColor: "rgba(211, 72, 54, 1)",
						data: alcance
					}
				]
			};

        	var options = {
				title : {
					display : true,
					position : "top",
					text : "Line Graph",
					fontSize : 18,
					fontColor : "#111"
				},
				legend : {
					display : true,
					position : "bottom"
				}
			};

			var ctx = $("#mycanvas");

			var barGraph = new Chart(ctx, {
				type: 'line',
				data: chartdata,
				options : options
			});
		},
		error: function(data) {
			console.log(data);
		}
	});

	$('[data-toggle="tooltip"]').tooltip();

	$('.datepicker').datepicker({
		format: 'yyyy/mm/dd',
        language: 'en'
      });

});