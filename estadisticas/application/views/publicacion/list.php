<h1>Estadisticas de  PUBLICACIONES</h1>
<p>Lorem ipsum dolor sit amet, ea etiam ocurreret duo. Liber legere graeco an eum. Exerci lobortis nam id, mea scripserit theophrastus an. Ius vide etiam signiferumque at. Ei prima nihil viderer eam, probatus volutpat petentium vim in, omittam convenire usu ex.</p>
<?php if($this->session->userdata('logged_in')) : ?>
<br />

<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading">Datos de PUBLICACIONES</div>

  <!-- Table -->
  <table class="table table-responsive table-hover">
    <table class="table table-striped" width="50%" cellspacing="5" cellpadding="5">
    <tr>
        <th>Fecha Inicio</th>
        <th>Fecha FIN</th> 
        <th>CLICKS</th>
        <th>COMENTARIOS</th>
        <th>SHARED</th>
        <th>MARCA</th>
        <th>DETALLE</th>
    </tr>
    <?php if(isset($datos)) : ?>
    <?php foreach($datos as $dato) : ?>
    <tr>
        <td><?php echo $dato->inicio_fecha; ?></td>
        <td><?php echo $dato->fecha_ultima; ?></td>
        <td><?php echo $dato->num_click; ?></td>
        <td><?php echo $dato->num_coment; ?></td>
        <td><?php echo $dato->num_shared; ?></td>
        <td><?php echo $dato->nombre; ?></td>
        <td><a href="<?php echo base_url();  ?>marcas/getting_chart/<?php echo $dato->idmarca; ?>" class="add pull-rigth btn btn-deadd-to-cart-btn add-to-cart glyphicon glyphicon glyphicon-signal" id="<?php echo $dato->idmarca; ?>" data-toggle="tooltip" title="Grafico"></a>
        </td>
    </tr>
    <?php endforeach; ?>
    <?php endif; ?>
</table>

</div>

<?php endif; ?>