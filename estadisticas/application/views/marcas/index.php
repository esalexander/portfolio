<?php if($this->session->flashdata('registered')) : ?>
    <?php echo '<p class="text-success">' .$this->session->flashdata('registered') . '</p>'; ?>
<?php endif; ?>
<?php if($this->session->flashdata('asiganado')) : ?>
    <?php echo '<p class="text-success">' .$this->session->flashdata('asiganado') . '</p>'; ?>
<?php endif; ?>
<h1>Listado de Marcas Administradas</h1>
<p>Lorem ipsum dolor sit amet, ea etiam ocurreret duo. Liber legere graeco an eum. Exerci lobortis nam id, mea scripserit theophrastus an. Ius vide etiam signiferumque at. Ei prima nihil viderer eam, probatus volutpat petentium vim in, omittam convenire usu ex.</p>
<?php if($this->session->userdata('logged_in')) : ?>
<br />

<div class="col-md-12">

<a href="<?php echo base_url(); ?>marcas/singles" class="btn btn-info pull-right">Asignadas ya</a>
<a href="<?php echo base_url(); ?>marcas/add" class="btn btn-info pull-right">Agregar</a>
<a href="<?php echo base_url(); ?>marcas/asignar" class="btn btn-info pull-right">Asignar</a>
<a href="<?php echo base_url(); ?>marcas/getting_chart" class="btn btn-info pull-right">Grafico</a>    
 
</div>

<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading">Marcas</div>

  <!-- Table -->
  <table class="table table-responsive table-hover">
    <table class="table table-striped" width="50%" cellspacing="5" cellpadding="5">
    <tr>
        <th>Nombre</th>
        <th>Email</th> 
        <th>Cliente</th>
        <th>Telefono</th>
    </tr>
    <?php if(isset($marcas)) : ?>
    <?php foreach($marcas as $marca) : ?>
    <tr>
        <td><?php echo $marca->nombre; ?></td>
        <td><?php echo $marca->email; ?></td>
        <td><?php echo $marca->cliente; ?></td>
        <td><?php echo $marca->telefono; ?></td>
    </tr>
    <?php endforeach; ?>
    <?php endif; ?>
</table>

</div>



<?php endif; ?>