<div class="row">
        <div class="col-md-8 col-md-offset-1 col-sm-6 col-sm-offset-2">
          <div class="card card-signup" style="margin-top: 60px;">
          <div class="header header-primary text-center">
                <h4 class="card-title">Registro de MARCA</h4>
                <p>Please fill out the form below to create an marca</p></p>

          </div>

          <!--Display Errors-->
          <?php echo validation_errors('<p class="text-error">'); ?>
          <?php echo form_open_multipart('marcas/agregar'); ?>
          <!--Field: Nombre-->
          <p>
          <?php echo form_label('Nombre:'); ?>
          <?php
          $data = array(
                        'name'        => 'nombre',
                        'class'       => 'form-control',
                        'value'       => set_value('nombre')
                      );
          ?>
          <?php echo form_input($data); ?>
          </p>

          <!--Field: Email Address-->
          <p>
          <?php echo form_label('Email Address:'); ?>
          <?php
          $data = array(
                        'name'        => 'email',
                        'class'       => 'form-control',
                        'value'       => set_value('email')
                      );
          ?>
          <?php echo form_input($data); ?>
          </p>

          <!--Field: Cliente-->
          <p>
          <?php echo form_label('Cliente Asignado:'); ?>
          <?php
          $data = array(
                        'name'        => 'cliente',
                        'class'       => 'form-control',
                        'value'       => set_value('cliente')
                      );
          ?>
          <?php echo form_input($data); ?>
          </p>

          <!--Field: Username-->
          <p>
          <?php echo form_label('Telefono:'); ?>
          <?php
          $data = array(
                        'name'        => 'telefono',
                        'class'       => 'form-control',
                        'value'       => set_value('telefono')
                      );
          ?>
          <?php echo form_input($data); ?>
          </p>

          <p>
          <?php echo form_label('Imagen:'); ?>
          <?php
          $data = array(
                'name' => 'marca_image',
                'value' => set_value('image')
            );
          ?>
          <?php echo form_upload($data); ?>
          </p>

          <!--Submit Buttons-->
          <?php $data = array("value" => "Registrar",
                              "name" => "submit",
                              "class" => "btn btn-primary"); ?>
          <p>
              <?php echo form_submit($data); ?>
          </p>
          <?php echo form_close(); ?>

</div>
</div>
</div>