
<h1>Asignar Marcas  a Empleado</h1>
<p>Lorem ipsum dolor sit amet, ea etiam ocurreret duo. Liber legere graeco an eum. Exerci lobortis nam id, mea scripserit theophrastus an. Ius vide etiam signiferumque at. Ei prima nihil viderer eam, probatus volutpat petentium vim in, omittam convenire usu ex.</p>
<?php if($this->session->userdata('logged_in')) : ?>
<br />

<h1>Selecciona la Marca y Usuario</h1>
<p>Please fill out the form below to create an marca</p>
<!--Display Errors-->
<?php echo validation_errors('<p class="text-error">'); ?>
<?php echo form_open('marcas/procces_asignacion'); ?>
<div class="col-md-5" style="margin-right: 25px;">
	<div class="row">

	<?php if(isset($marcas)) : ?>
	<select name="arraymarcas[]" multiple class="form-control">
	    <?php foreach($marcas as $marca) : ?>
			  <option value="<?php echo $marca->idmarca ?>"><?php echo $marca->nombre ?></option>
		<?php endforeach; ?>
	</select>
	<?php endif; ?>	
		
	</div>
	
</div>

<div class="col-md-6">
	<div class="row">
			
			<?php if(isset($users)) : ?>
			<select name="arrayusuarios" multiple class="form-control">
		    <?php foreach($users as $user) : ?>
				  <option value="<?php echo $user->id ?>"><?php echo $user->first_name . ' '.$user->last_name  ?></option>
				<?php endforeach; ?>
			</select>
			<?php endif; ?>
		
	</div>
</div>

<!--Submit Buttons-->
<?php $data = array("value" => "Asignar",
                    "name" => "submit",
                    "class" => "btn btn-primary"); ?>
<p>
    <?php echo form_submit($data); ?>
</p>
<?php echo form_close(); ?>

<?php endif; ?>