<?php if($this->session->flashdata('registered')) : ?>
    <?php echo '<p class="text-success">' .$this->session->flashdata('registered') . '</p>'; ?>
<?php endif; ?>
<?php if($this->session->flashdata('asiganado')) : ?>
    <?php echo '<p class="text-success">' .$this->session->flashdata('asiganado') . '</p>'; ?>
<?php endif; ?>
<h1>Listado de Marcas Administradas</h1>
<p>Lorem ipsum dolor sit amet, ea etiam ocurreret duo. Liber legere graeco an eum. Exerci lobortis nam id, mea scripserit theophrastus an. Ius vide etiam signiferumque at. Ei prima nihil viderer eam, probatus volutpat petentium vim in, omittam convenire usu ex.</p>
<?php if($this->session->userdata('logged_in')) : ?>
<br />

<div class="row text-center">
    <?php if(isset($marcas)) : ?>
    <?php foreach($marcas as $marca) : ?>
            <div class="col-md-4 col-sm-6 hero-feature">
                <div class="thumbnail">
                    <a href="<?php echo base_url();  ?>marcas/fetch_single_data/<?php echo $marca->idmarca; ?>">
                    <?php if($marca->image != '') :?>
                        <img src="<?php echo base_url(); ?>/upload/marcas/<?php echo $marca->image ?>" class="img-responsive img-thumbnail" width="150px" height="150px" alt="">
                    <?php else: ?>
                        <img src="http://placehold.it/800x500" class="img-responsive img-thumbnail" alt="">
                    <?php endif; ?>    
                    <div class="caption">
                        <h3 class="title"><?php echo $marca->nombre; ?></a></h3>
                        <p class="subtitle">
                        <?php echo $marca->email; echo '<br />'; 
                                echo 'Cliente:'.$marca->cliente;
                        ?></p>
                        <div class="divider"></div>

                        <div class="footer-card">
                        <div class="pull-left">
                            <a href="<?php echo base_url();  ?>import/form_upload/<?php echo $marca->idmarca; ?>" class="add pull-rigth btn btn-deadd-to-cart-btn add-to-cart glyphicon glyphicon-check" id="<?php echo $marca->idmarca; ?>" data-toggle="tooltip" title="Datos"></a>
                        </div>

                        <div class="pull-left">
                            <a href="<?php echo base_url();  ?>import/form_publica/<?php echo $marca->idmarca; ?>" class="add pull-rigth btn btn-deadd-to-cart-btn add-to-cart glyphicon glyphicon glyphicon-list" id="<?php echo $marca->idmarca; ?>" data-toggle="tooltip" title="Publicaciones"></a>
                        </div>

                        <div class="pull-rigth">
                            <a href="<?php echo base_url();  ?>marcas/getting_chart/<?php echo $marca->idmarca; ?>" class="add pull-rigth btn btn-deadd-to-cart-btn add-to-cart glyphicon glyphicon glyphicon-signal" id="<?php echo $marca->idmarca; ?>" data-toggle="tooltip" title="Grafico"></a>
                        </div>                        
                             
                        </div>
                    </div>
                </div>
            </div>
    <?php endforeach; ?>
    <?php endif; ?>
    
            <!--<div class="col-md-4 col-sm-6 hero-feature">
                <div class="thumbnail">
                    <img src="http://placehold.it/800x500" alt="">
                    <div class="caption">
                         <h3 class="title">Feature Label</h3>
                        <p class="subtitle">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                        <div class="divider"></div>

                        <div class="footer-card">
                        <div class="pull-left">
                            <a href="#" class="add pull-rigth btn btn-deadd-to-cart-btn add-to-cart glyphicon glyphicon-plus-sign"></a>
                        </div>

                        <div class="pull-rigth">
                            <a href="#" class="add pull-rigth btn btn-deadd-to-cart-btn add-to-cart glyphicon glyphicon-plus-sign"></a>
                        </div>                        
                             
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-sm-6 hero-feature">
                <div class="thumbnail">
                    <img src="http://placehold.it/800x500" alt="">
                    <div class="caption">
                         <h3 class="title">Feature Label</h3>
                        <p class="subtitle">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                        <div class="divider"></div>

                        <div class="footer-card">
                        <div class="pull-left">
                            <a href="#" class="add pull-rigth btn btn-deadd-to-cart-btn add-to-cart glyphicon glyphicon-plus-sign"></a>
                        </div>

                        <div class="pull-rigth">
                            <a href="#" class="add pull-rigth btn btn-deadd-to-cart-btn add-to-cart glyphicon glyphicon-plus-sign"></a>
                        </div>                        
                             
                        </div>
                    </div>
                </div>
            </div>-->

            

        </div>
        <!-- /.row -->

<?php endif; ?>