<h1>Estadisticas Total de Marca</h1>
<p>Lorem ipsum dolor sit amet, ea etiam ocurreret duo. Liber legere graeco an eum. Exerci lobortis nam id, mea scripserit theophrastus an. Ius vide etiam signiferumque at. Ei prima nihil viderer eam, probatus volutpat petentium vim in, omittam convenire usu ex.</p>
<?php if($this->session->userdata('logged_in')) : ?>
<br />

<?php if(isset($paginas)) : ?>
    
    <div class="card card-nav-tabs text-center">
    <div class="header header-success">
        <h5>Datos Actuales</h5>        
    </div>
    <div class="content" style="margin: auto; max-width: 440px;">
    <div class="row">
    <ul class="nav nav-pills" role="tablist">
    <?php 
        //print_r($paginas);
        if(count($paginas)>1){
            $objlast = end($paginas);
            $objprev = prev($paginas);
            $comunidad = $objlast->num_likes;
            $fans = $objlast->num_likes - $objprev->num_likes;
            //print_r($fans);
        ?>  
                            <!--
                                color-classes: "nav-pills-primary", "nav-pills-info", "nav-pills-success", "nav-pills-warning","nav-pills-danger"
                            -->
                            <li>
                                <a href="#dashboard" role="tab" data-toggle="tooltip" title="Comunidad">
                                    <i class="material-icons">dashboard</i>
                                    Comunidad <br/><?php echo ($comunidad > 0 ? $comunidad : false);?>
                                </a>
                            </li>
                            <li>
                                <a href="#schedule" role="tab" data-toggle="tooltip" title="Fans">
                                    <i class="material-icons">schedule</i>
                                    Fans Nuevos <br/><?php echo ($fans >= 0 ? $fans : false);?>
                                </a>
                            </li>
                            <li>
                                <a href="#tasks" role="tab" data-toggle="tooltip" title="Comentarios">
                                    <i class="material-icons">list</i>
                                    Comentarios
                                </a>
                            </li>
                            <li>
                                <a href="#payments" role="tab" data-toggle="tooltip" title="Shared">
                                    <i class="material-icons">attach_money</i>
                                    Shared
                                </a>
                            </li>
                        </ul>
             <?php
    }
    ?>             
        </div>

        <div class="row">
        <input type="hidden" name="filtro" value="<?php echo $this->uri->segment(3); ?>"/>
        <!--Start Form-->
        <?php 
        $filtro = $this->session->flashdata('filtro'); 
        //echo $filtro; 
        $url_form = 'marcas/fetch_single_data/'.$filtro;
        ?>
        <?php
        $attributes = array('id' => 'filter_form',
                                  'class' => 'form-inline'); ?>
        <?php echo form_open($url_form,$attributes); ?>
                  
                  <div class="form-group">
                    <input name="startdate" class="datepicker form-control" type="text" data-date-format="yyyy/mm/dd" placeholder="Selec Date"/>
                  </div>
                  <div class="form-group">
                    <input name="enddate" class="datepicker form-control" type="text" data-date-format="yyyy/mm/dd" placeholder="Select Date"/>
                  </div>
                  <button type="submit" class="btn btn-default">Filtrar</button>
                
        <?php echo form_close(); ?>        
        </div>
          
    </div>       
</div>

<br />
<br />
<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading">Fan Page</div>
  <!-- Table -->
  <table class="table table-responsive table-hover">
    <table class="table table-striped" width="50%" cellspacing="5" cellpadding="5">
    <tr>
        <th>Fecha Inicio</th>
        <th>Fecha FIN</th> 
        <th>FANS</th>
        <th>TALKING ABOUT</th>
        <th>ALCANCE</th>
        <th>DETALLE</th>
    </tr>
    <?php foreach($paginas as $pagina) : ?>
    <tr>
        <td><?php echo $pagina->inicio_fecha; ?></td>
        <td><?php echo $pagina->fecha_ultima; ?></td>
        <td><strong><?php echo $pagina->num_likes; ?></strong></td>
        <td><?php echo $pagina->num_talking; ?></td>
        <td><?php echo $pagina->num_alcance; ?></td>
        <td><a href="<?php echo base_url();  ?>marcas/getting_chart/<?php echo $pagina->idmarca; ?>" class="add pull-rigth btn btn-deadd-to-cart-btn add-to-cart glyphicon glyphicon glyphicon-signal" id="<?php echo $pagina->idmarca; ?>" data-toggle="tooltip" title="Grafico"></a>
        </td>
    </tr>
    <?php endforeach; ?>
    <?php endif; ?>
</table>

</div>

<br />

<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading">Publicaciones</div>

  <!-- Table -->
  <table class="table table-responsive table-hover">
    <table class="table table-striped" width="50%" cellspacing="5" cellpadding="5">
    <tr>
        <th>Fecha Inicio</th>
        <th>Fecha FIN</th> 
        <th>CLICKS</th>
        <th>COMENTARIOS</th>
        <th>SHARED</th>
        <th>DETALLE</th>
    </tr>
    <?php if(isset($publicaciones)) : ?>
    <?php foreach($publicaciones as $publicacion) : ?>
    <tr>
        <td><?php echo $publicacion->inicio_fecha; ?></td>
        <td><?php echo $publicacion->fecha_ultima; ?></td>
        <td><?php echo $publicacion->num_click; ?></td>
        <td><?php echo $publicacion->num_coment; ?></td>
        <td><?php echo $publicacion->num_shared; ?></td>
        <td><a href="<?php echo base_url();  ?>marcas/getting_chart/<?php echo $publicacion->idmarca; ?>" class="add pull-rigth btn btn-deadd-to-cart-btn add-to-cart glyphicon glyphicon glyphicon-signal" id="<?php echo $publicacion->idmarca; ?>" data-toggle="tooltip" title="Grafico"></a>
        </td>
    </tr>
    <?php endforeach; ?>
    <?php endif; ?>
</table>

</div>

<?php endif; ?>