        <div class="container">
          <div class="row">
              <div class="col-md-8 col-md-offset-2" style="margin-top:10%;">
                  <?php echo form_open_multipart('import/importexcel');?>
                    
                  <div class="form-group">
                    <input type="file" class="form-control" name="excel" size="20" />
                  </div>
                  

                  <div class="form-group">
                    <label class="control-label">Escribe el nombre de la tabla.</label><br />
                    <input type="text" class="form-control" name="table" />
                  </div>
                  
                  <div class="form-group">
                      <label>Escribe, separados por una coma, los campos de tu tabla excepto los autoincrementales, ejemplo id etc.</label>
                      <input type="text" class="form-control" name="fields" style="width:600px" />
                  </div>
                  

                  <input type="submit" class="btn btn-default" value="Importar" />

                <?php echo form_close() ?>
 
              </div>
          </div>
      </div>
