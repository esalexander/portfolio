<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>LAFaBRICADBM</title>

    <!-- Material Design fonts -->
  <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700">
  <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/icon?family=Material+Icons">

  <!-- Bootstrap -->
  <link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

  <!-- Bootstrap Material Design -->
  
  <!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();  ?>assets/css/bootstrap-material-design.min.css">-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();  ?>assets/css/material-kit.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();  ?>assets/css/ripples.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();  ?>assets/css/bootstrap-datepicker.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();  ?>assets/css/style.css">
  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">ESTADISTICAS DBM</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="<?php echo base_url();  ?>">Inicio</a></li>
            <li><a href="<?php echo base_url();  ?>marcas/">Marcas</a></li>
            <li><a href="<?php echo base_url();  ?>pagina/data">Paginas</a></li>
            <li><a href="<?php echo base_url();  ?>publicacion/list_data">Publicaciones</a></li>
            <li><a href="<?php echo base_url();  ?>marcas/singles">Administrar</a></li>
            <li><a href="<?php echo base_url();  ?>users/register">Register</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <div class="container-fluid wrapper">
    <div class="row-fluid columns">

          <div class="col-md-3 col-sm-12">
            <div class="well sidebar-nav">
            <div style="">
                <!--SIDEBAR CONTENT-->
                <?php $this->load->view('users/login'); ?>
                    </div>
                </div><!--/.well -->
          </div><!--/col-->
        

        <div class="col-md-9 col-sm-12 content-area">
          <!--MAIN CONTENT-->
          <?php $this->load->view($main_content); ?>
        </div><!--/col-->

    </div><!--/row-->


      

      <footer class="footer">
        <div class="container-fluid pie">
          <p>&copy; 2017 LAFaBRICADBM.</p>
        </div>
      </footer>

    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url();  ?>assets/js/jquery.min.js"></script>
    <!--  -->
    <script src="<?php echo base_url();  ?>assets/js/bootstrap.js"></script>
    <script src="<?php echo base_url();  ?>assets/js/material.min.js"></script>
    <script src="<?php echo base_url();  ?>assets/js/ripples.min.js"></script>
    <script src="<?php echo base_url();  ?>assets/js/material-kit.js"></script>
    <script src="<?php echo base_url();  ?>assets/js/bootstrap-datepicker.js"></script>
    <!--<script src="<?php echo base_url();  ?>assets/js/bootstrap-datepicker.es.min.js"></script>-->
    
    <script src="<?php echo base_url();  ?>assets/js/chart.min.js"></script>
    <script src="<?php echo base_url();  ?>assets/js/linegraph.js"></script>
    <script src="<?php echo base_url();  ?>assets/js/app.js"></script>
    <!--<script type="text/javascript">
    $(function(){
      $('.datepicker').datepicker({
        language: 'es'
      });
    });
  
    </script>-->
  </body>
</html>