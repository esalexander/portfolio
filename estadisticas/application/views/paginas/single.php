<h1>Estadisticas de la Marca</h1>
<p>Lorem ipsum dolor sit amet, ea etiam ocurreret duo. Liber legere graeco an eum. Exerci lobortis nam id, mea scripserit theophrastus an. Ius vide etiam signiferumque at. Ei prima nihil viderer eam, probatus volutpat petentium vim in, omittam convenire usu ex.</p>
<?php if($this->session->userdata('logged_in')) : ?>
<br />

<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading">Datos</div>

  <!-- Table -->
  <table class="table table-responsive table-hover">
    <table class="table table-striped" width="50%" cellspacing="5" cellpadding="5">
    <tr>
        <th>Fecha Inicio</th>
        <th>Fecha FIN</th> 
        <th>LIKES</th>
        <th>TALKING ABOUT</th>
        <th>ALCANCE</th>
        <th>Creado</th>
    </tr>
    <?php if(isset($datos)) : ?>
    <?php foreach($datos as $dato) : ?>
    <tr>
        <td><?php echo $dato->inicio_fecha; ?></td>
        <td><?php echo $dato->fecha_ultima; ?></td>
        <td><?php echo $dato->num_likes; ?></td>
        <td><?php echo $dato->num_talking; ?></td>
        <td><?php echo $dato->num_alcance; ?></td>
        <td><?php echo $dato->created_at; ?></td>
    </tr>
    <?php endforeach; ?>
    <?php endif; ?>
</table>

</div>

<?php endif; ?>