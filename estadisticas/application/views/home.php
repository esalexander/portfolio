<!--Display Messages-->
<?php if($this->session->flashdata('registered')) : ?>
    <?php echo '<p class="text-success">' .$this->session->flashdata('registered') . '</p>'; ?>
<?php endif; ?>
<?php if($this->session->flashdata('logged_out')) : ?>
    <?php echo '<p class="text-success">' .$this->session->flashdata('logged_out') . '</p>'; ?>
<?php endif; ?>
<?php if($this->session->flashdata('need_login')) : ?>
    <?php echo '<p class="text-error">' .$this->session->flashdata('need_login') . '</p>'; ?>
<?php endif; ?>

<h1>Bienvenido!</h1>
<p>Lorem ipsum dolor sit amet, ea etiam ocurreret duo. Liber legere graeco an eum. Exerci lobortis nam id, mea scripserit theophrastus an. Ius vide etiam signiferumque at. Ei prima nihil viderer eam, probatus volutpat petentium vim in, omittam convenire usu ex.</p>
<?php if($this->session->userdata('logged_in')) : ?>
<br />

<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading">Usuarios</div>

  <!-- Table -->
  <table class="table table-responsive table-hover">
    <table class="table table-striped" width="50%" cellspacing="5" cellpadding="5">
    <tr>
        <th>Usuario</th>
        <th>Email</th> 
        <th>Creado</th>
    </tr>
    <?php if(isset($users)) : ?>
    <?php foreach($users as $user) : ?>
    <tr>
        <td><?php echo $user->username; ?></td>
        <td><?php echo $user->email; ?></td>
        <td><?php echo $user->register_date; ?></td>
    </tr>
    <?php endforeach; ?>
    <?php endif; ?>
</table>

</div>



<?php endif; ?>