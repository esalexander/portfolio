<?php

/**
* 
*/
class Pagina extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

	function data()
	{
		$this->load->model('pagina_model');
		//$data['datos'] = $this->pagina_model->fetch_data();
		$data['datos'] = $this->pagina_model->fetch_data_paginas();
		$data['main_content'] = 'paginas/paginasView';
        $this->load->view('layouts/main',$data);
	}

	function single_data($codigo)
	{
		$this->load->model('pagina_model');
		$data['datos'] = $this->pagina_model->fetch_data($codigo);
		$data['main_content'] = 'paginas/paginasView';
        $this->load->view('layouts/main',$data);
	}


}