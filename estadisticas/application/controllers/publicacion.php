<?php

/**
* 
*/
class Publicacion extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function list_data()
	{
		$this->load->model('publicacion_model');
		$data['datos'] = $this->publicacion_model->fetch_all_data();
		$data['main_content'] = 'publicacion/list';
        $this->load->view('layouts/main',$data);
	}


}