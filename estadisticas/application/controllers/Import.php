<?php
defined("BASEPATH") OR die("El acceso directo al script está prohibido");
 
class Import extends CI_Controller
{
 
  public function __construct()
  {
    parent::__construct();
    
  }
 
  public function form_upload($codigo)
  {
    $data['marcaid'] = $codigo;
    $data['main_content'] = 'import/datos';
    $this->load->view('layouts/main',$data);
  }

  public function form_publica($codigo)
  {
    $data['marcaid'] = $codigo;
    $data['main_content'] = 'import/publicacion';
    $this->load->view('layouts/main',$data);
  }

  
  public function load_dato()
  {
    $config = array(
      'upload_path' => FCPATH.'upload/',
      'allowed_types' => 'xls'
      );
    
    $this->load->library('upload',$config);
    if($this->upload->do_upload('file'))
    {
    $data = $this->upload->data();
    //@chmod($data['full_path'],0777);
        
    error_reporting(0); 

    $file = $data['full_path'];
    $file_name = $data['file_name'];
    $codigo = $this->input->post('codigo');
       
    //load the excel library
    $this->load->library('excel');

      //read file from path
    $objPHPExcel = PHPExcel_IOFactory::load($file);
    $worksheet = $objPHPExcel->getSheet(0);
    $lastrow = $worksheet->getHighestRow();

    $numLikes = 0;
    $sumTalking = 0;
    $sumReaching = 0;
     
     $dataArrReaching = array();
     $dataArrLikes = array();
     $dataArrDates = array();

    for ($row=3; $row <= $lastrow ; $row++) { 
      if(!is_null($worksheet->getCell('A'.$row)->getValue()))
      {
        
        $val= $worksheet->getCell('A'.$row)->getValue();
        $val = date('Y-m-d',PHPExcel_Shared_Date::ExcelToPHP($val));
        $dataArrDates [$row] = $val;
        $temp = $worksheet->getCell('B'.$row)->getValue();
        $dataArrLikes[$row] = $temp;
        $temp = $worksheet->getCell('E'.$row)->getValue();
        $sumTalking = $sumTalking + $temp;
        $mivar = $worksheet->getCell('H'.$row)->getCalculatedValue();
        $sumReaching = $sumReaching + $mivar;
        $dataArrReaching[$row] = $mivar;
        }
    }
    $lastdate = end(array_values($dataArrDates));
    
    $numLikes = end(array_values($dataArrLikes));

    $user_id = $this->session->userdata('user_id');
    $data = array(
        'inicio_fecha' => $dataArrDates[3],
        'fecha_ultima' => $lastdate,
        'num_likes' => $numLikes,
        'num_talking' => $sumTalking,
        'num_alcance' => $sumReaching,
        'idusuario' => $user_id,
        'idmarca'=> $codigo,
        'filedata' => $file_name
      );
    
    $this->load->model('import_model');
    if($this->import_model->insert_data('tbl_paginas',$data)){
         redirect('pagina/data');
    }else{
         redirect('home/index');
    }
  
  }
}

public function load_publicacion()
  {
    $config = array(
      'upload_path' => FCPATH.'upload/',
      'allowed_types' => 'xls'
      );
    
    $this->load->library('upload',$config);
    if($this->upload->do_upload('file'))
    {
      $data = $this->upload->data();
        //@chmod($data['full_path'],0777);
        
      error_reporting(0); 

      $file = $data['full_path'];
      $file_name = $data['file_name'];
      $codigo = $this->input->post('codigo');
       
      //load the excel library
      $this->load->library('excel');
       
      //read file from path
    $objPHPExcel = PHPExcel_IOFactory::load($file);
    $worksheet = $objPHPExcel->getSheet(0);
    $lastrow = $worksheet->getHighestRow();

    $numClick_columnP = 0;

    
    $numComents_columnJ = 0;
    $$numLikes_columnK = 0;
    $numShared_columnL = 0;
    
    $dataArrDates = array();
    for ($row=2; $row <= $lastrow ; $row++) { 
      
        $val= $worksheet->getCell('G'.$row)->getValue();
        $val = date('Y-m-d',PHPExcel_Shared_Date::ExcelToPHP($val));
        $dataArrDates [$row] = $val;
        $temp = $worksheet->getCell('P'.$row)->getValue();
        $numClick_columnP = $numClick_columnP + $temp;
    }

    $lastdate = end(array_values($dataArrDates));

    $worksheet = $objPHPExcel->getSheet(1);
    $lastrow = $worksheet->getHighestRow();
    for ($row=1; $row <= $lastrow ; $row++) { 
        $temp = $worksheet->getCell('J'.$row)->getValue();
        $numComents_columnJ = $numComents_columnJ + $temp;
        $temp = $worksheet->getCell('K'.$row)->getValue();
        $numLikes_columnK = $numLikes_columnK + $temp;
        $temp = $worksheet->getCell('L'.$row)->getCalculatedValue();
        $numShared_columnL = $numShared_columnL + $temp;
    }
    $user_id = $this->session->userdata('user_id');
    
    $data = array(
        'inicio_fecha' => $dataArrDates[2],
        'fecha_ultima' => $lastdate,
        'num_click' => $numClick_columnP,
        'num_coment' => $numComents_columnJ,
        'num_like' => $numLikes_columnK,
        'num_shared' => $numShared_columnL,
        'idmarca'=> $codigo,
        'idusuario' => $user_id,
        'filedata' => $file_name
      );

    $this->load->model('import_model');
    if($this->import_model->insert_data('tbl_publicaciones',$data)){
         redirect('publicacion/list_data');
    }else{
         redirect('home/index');
    }
  
  }
}

}
 
  