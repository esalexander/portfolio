<?php

/**
* 
*/
class Marcas extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

	function index()
	{
		$this->load->model('marcas_model');
		$data['marcas'] = $this->marcas_model->fetch_marcas();
		$data['main_content'] = 'marcas/index';
		//$data['main_content'] = 'marcas/list';
        $this->load->view('layouts/main',$data);
	}

	function singles()
	{
		if(!$this->session->userdata('logged_in')){
            redirect('home/index');
        }
		$this->load->model('marcas_model');
		$user_id = $this->session->userdata('user_id');
		$data['marcas'] = $this->marcas_model->fetch_brands($user_id);
		$data['main_content'] = 'marcas/list';
        $this->load->view('layouts/main',$data);
	}

	function fetch_single_data($codigo = null)
	{
		$this->load->model('marcas_model');
		$this->session->set_flashdata('filtro', $codigo);
		//$data['filtro']; =
		$start_date = $this->input->post('startdate');
		$end_date = $this->input->post('enddate');
		if (!is_null($start_date) && !is_null($end_date)) {
			$filtro = $this->session->flashdata('filtro');
			$data['paginas'] = $this->marcas_model->fetch_single_marca($filtro,$start_date,$end_date);
			//$data['publicaciones'] = $this->marcas_model->fetch_single_public($filtro);
			$data['main_content'] = 'marcas/total';
        	$this->load->view('layouts/main',$data);
		}else{
		$data['paginas'] = $this->marcas_model->fetch_single_marca($codigo);
		//$data['publicaciones'] = $this->marcas_model->fetch_single_public($codigo);
		$data['main_content'] = 'marcas/total';
        $this->load->view('layouts/main',$data);
        }
	}

	/*function fetch_filtered_data()
	{
		$this->load->model('marcas_model');
		$filtro = $this->input->post('filtro');
		$start_date = $this->input->post('startdate');
		$end_date = $this->input->post('enddate');
		if (!is_null($start_date) && !is_null($end_date)) {
		
			$data['paginas'] = $this->marcas_model->fetch_single_marca($filtro,$start_date,$end_date);
			//$data['publicaciones'] = $this->marcas_model->fetch_single_public($filtro);
			$data['main_content'] = 'marcas/total';
        	$this->load->view('layouts/main',$data);
		}
	}*/

	function add()
	{
		if($this->session->userdata('logged_in')){
            //redirect('marcas/index');
        }
        $data['main_content'] = 'marcas/agregar';
        $this->load->view('layouts/main',$data);
	}



	function agregar()
	{
        /*if($this->session->userdata('logged_in')){
            redirect('marcas/index');
        }*/
        $this->form_validation->set_rules('nombre','Nombre','trim|required');
        $this->form_validation->set_rules('email','Email','trim|required|valid_email');
        $this->form_validation->set_rules('cliente','Cliente','trim|required');
        $this->form_validation->set_rules('telefono','Telefono','trim|required|min_length[8]');
        
        if($this->form_validation->run() == FALSE){
            //Load view and layout
            $data['main_content'] = 'marcas/index';
            $this->load->view('layouts/main',$data);
        //Validation has ran and passed    
        } else {
        	$this->load->model('marcas_model');
            if($this->marcas_model->add_brand()){
                $this->session->set_flashdata('registered', 'Nuevo Dato registrado');
                //Redirect to index page with error above
                redirect('marcas/index');
            }
        }

	}

	function asignar()
	{
		$this->load->model('marcas_model');
		$data['marcas'] = $this->marcas_model->fetch_marcas();
		$this->load->model('user_model');
		$data['users'] = $this->user_model->fetch_users();
		$data['main_content'] = 'marcas/asignar';
        $this->load->view('layouts/main',$data);
	}

	function procces_asignacion()
	{
		/*$this->form_validation->set_rules('arraymarcas','Nombre','trim|required');
        //$this->form_validation->set_rules('arraymarcas','Email','trim|required');
        if($this->form_validation->run() == FALSE)
        {
        	redirect('home/index');
        }else{*/
	        	$this->load->model('marcas_model');
	            if($this->marcas_model->add_asignacion()){
	                $this->session->set_flashdata('registered', 'Nuevo Dato asignado');
	                //Redirect to index page with error above
	                redirect('marcas/index');
	            }
        //}

	}

	/*function getting_chart()
	{
		
		$data['main_content'] = 'marcas/chart';
        $this->load->view('layouts/main',$data);
	}*/
	function getting_chart($codigo)
	{
		$data['codigo'] = $codigo;
		$data['main_content'] = 'marcas/chart';
        $this->load->view('layouts/main',$data);
	}

	function getchart()
	{
		$this->load->model('marcas_model');
		$codigo = $this->input->post('user_id');
		$datos =  $this->marcas_model->get_single_chart($codigo);
		print_r (json_encode($datos));
	}



	
}