<?php

class Home extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index(){
        if($this->session->userdata('logged_in')){
            //Get the logged in users id
            $user_id = $this->session->userdata('user_id');
            $this->load->model('user_model');
            $data['users'] = $this->user_model->fetch_users(); 
        }

       
        $data['main_content'] = 'home';
        $this->load->view('layouts/main',$data);
    }
}