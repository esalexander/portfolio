<?php

/**
* 
*/
class pagina_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function fetch_data()
	{
		$result = $this->db->get('tbl_paginas');
		return $result->result();
	}

	public function fetch_data_paginas()
	{
		$this->db->select('*');
		$this->db->from('tbl_paginas');
		$this->db->join('tbl_marcas','tbl_marcas.idmarca = tbl_paginas.idmarca');
		$result = $this->db->get();
		return $result->result();
	}
}