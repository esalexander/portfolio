<?php

/**
* 
*/
class publicacion_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function fetch_data()
	{
		$result = $this->db->get('tbl_publicaciones');
		return $result->result();
	}

	public function fetch_all_data()
	{
		$this->db->select('*');
		$this->db->from('tbl_publicaciones');
		$this->db->join('tbl_marcas','tbl_marcas.idmarca = tbl_publicaciones.idmarca');
		$result = $this->db->get();
		return $result->result();
	}
}