<?php
defined("BASEPATH") OR exit("El acceso directo al script no está permitido!!!");
/**
* 
*/
class Marcas_model extends CI_Model
{
	public function __construct()
	{
	    parent::__construct();
	}

	public function add_brand()
	{
		 $new_brand_insert = array(
            'nombre'       => $this->input->post('nombre'),
            'email'        => $this->input->post('email'),
            'cliente'      => $this->input->post('cliente'),
            'telefono'     => $this->input->post('telefono'),
            'image'=>$this->upload_image()
        );
        
        $insert = $this->db->insert('tbl_marcas', $new_brand_insert);
        return $insert;
	}

	public function add_asignacion()
	{
		$brands = $this->input->post('arraymarcas');
		    foreach($brands as $brand) {
		    $data = array(
		    	'idmarca' => $brand,
		        'id' => $this->input->post('arrayusuarios')
		        
		    );
		    $this->db->insert('brand_assignadas', $data);
		    $i++;
		    }
		if($i>0){
			return true;
		}else{
			return false;
		}    		  
	}

	public function fetch_marcas()
	{
		$query = $this->db->get('tbl_marcas');
		return $query->result();
	}

	public function fetch_single_marca($codigo,$start_date = null, $end_date = null )
	{
		$this->db->where('idmarca',$codigo);

		if (is_null($start_date) && is_null($end_date)) {
			$query = $this->db->get('tbl_paginas');
		}else{
			//$start_date =  date_format(date_create($start_date), 'Y-m-d');
			//$end_date =  date_format(date_create($end_date), 'Y-m-d');
			//$start_date =  date('Y-m-d',$start_date);
			//$end_date =  date('Y-m-d',$end_date);

			//$this->db->where('fecha_ultima >=', $start_date);
			//$this->db->or_where('fecha_ultima <=', $end_date);
			//$start_date='2016-09-01';
			//$end_date='2017-07-31';
			//date('Y-m-d', strtotime($start_date))

			$this->db->where('(fecha_ultima BETWEEN "'.$start_date. '" and "'. $end_date.'")');
			$query = $this->db->get('tbl_paginas');
		}
		//print_r($query);
		return $query->result();
	}

	public function fetch_single_public($codigo)
	{
		$this->db->where('idmarca',$codigo);
		$query = $this->db->get('tbl_publicaciones');
		return $query->result();
	}

	public function fetch_brands($id)
	{
		$this->db->select('*');
		$this->db->from('tbl_marcas');
		$this->db->join('brand_assignadas', 'tbl_marcas.idmarca = brand_assignadas.idmarca');
		$this->db->where('brand_assignadas.id',$id);
		$query = $this->db->get();
		return $query->result();
	}

	/*public function get_single_chart()
	{
		$this->db->select('inicio_fecha');
		$this->db->select('num_likes');
		$query = $this->db->get('tbl_paginas');
		return $query->result();
	}*/

	public function get_single_chart($codigo)
	{
		$this->db->select('inicio_fecha');
		$this->db->select('num_likes');
		$this->db->select('num_talking');
		$this->db->select('num_alcance');
		$this->db->from('tbl_paginas');
		$this->db->where('idmarca',$codigo);
		$query = $this->db->get();
		return $query->result();
	}

	function upload_image()  
      {  
           if(isset($_FILES["marca_image"]))  
           {  
                $extension = explode('.', $_FILES['marca_image']['name']);  
                $new_name = rand() . '.' . $extension[1];  
                $destination = './upload/marcas/' . $new_name;  
                move_uploaded_file($_FILES['marca_image']['tmp_name'], $destination);  
                return $new_name;  
           }  
      }


}