<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('slide_model');
	}
	
	public function index()
	{
		$data['slide'] = $this->slide_model->get(); 
		$data['menus'] = $this->slide_model->get_menu();
		$data['noticias'] = $this->slide_model->get_noticia();
		$data['main_content'] = 'home/home';
		$this->load->view('layout/main_layout',$data);
	}
	
	public function contacto()
	{
	    $data['main_content'] = 'home/contacto';
	    $this->load->view('layout/main_layout',$data);
	}
}