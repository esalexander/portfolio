<?php
class Album extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('album_model');
    }
    
    function index()
    {
        $data['albumes'] = $this->album_model->get_albumes();
        $data['main_content'] = 'album/index';
        $this->load->view('layout/main_layout',$data);
    }
    
    function detail($id)
    {
        $data['galerias'] = $this->album_model->get_galerias($id);
        $data['main_content'] = 'album/detail';
        $this->load->view('layout/main_layout',$data);
    }
}