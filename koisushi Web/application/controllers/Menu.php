<?php
class Menu extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('menu_model');
	}
	
	function index()
	{
	    $data['categorias'] = $this->menu_model->get_menu();
	    $data['main_content'] = 'menu/index';
		$this->load->view('layout/main_layout',$data);
	}
	
}