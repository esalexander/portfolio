<?php
class Eventos extends  CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('evento_model');
    }
    
    function index()
    {
        $data['eventos'] = $this->evento_model->get_slide_eventos();
        $data['main_content'] = 'evento/index';
        $this->load->view('layout/main_layout',$data);
    }
}