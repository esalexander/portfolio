<?php

class Dashboard extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        //$this->load->model('user_model');
    }
    
    
    function dash_page()
    {
        if(!$this->session->userdata('logged_in')){
            redirect('home/index');
        }
        $data['main_content'] = 'dashboard/menu_options';
        //$data['main_content'] = 'dashboard/add_categoria';
        $this->load->view('dashboard/dashboard_main',$data);
    }
    
    function add_menu()
    {
        if(!$this->session->userdata('logged_in')){
            redirect('home/index');
        }
        $data['main_content'] = 'dashboard/add_categoria';
        $this->load->view('dashboard/dashboard_main',$data);
    }
}