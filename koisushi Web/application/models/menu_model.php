<?php
class menu_model extends CI_Model{
    
    function __construct()
    {
        parent::__construct();
    }
    
    function get_menu()
    {
        $query = $this->db->get('categorias');
        return $query->result();
    }
}
?>