<?php
class album_model extends CI_Model
{
    function get_albumes()
    {
        $query = $this->db->get('albumes');
        return $query->result();
    }
    
    function get_galerias($codigo)
    {
        $this->db->where('idAlbum',$codigo);
        $query = $this->db->get('galeria');
        return $query->result();
    }
}