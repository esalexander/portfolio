<?php
class slide_model extends CI_Model{
	
	function __construct()
	{
		parent::__construct();
	}
	
	function get()
	{
		$query = $this->db->get('slide_home');
		return $query->result();
	}
	
	function get_menu()
	{
		$query = $this->db->get('categorias');
		return $query->result();
	}
	
	function get_noticia()
	{
		$query = $this->db->get('noticias');
		return $query->result();
	}
}
?>