<?php
class user_model extends CI_Model
{
    public function login_user($username,$password)
    {
        //Secure password
        $enc_password = md5($password);
        
        //Validate
        $this->db->where('usuario',$username);
        $this->db->where('password',$enc_password);
        
        $result = $this->db->get('usuarios');
        if($result->num_rows() == 1){
            return $result->row(0)->idUsuario;
        } else {
            return false;
        }
    }
}