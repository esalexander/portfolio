<?php
class evento_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    function get_slide_eventos()
    {
        $this->db->order_by('orden');
        $query = $this->db->get('slide_eventos');
        return $query->result();
    }
    
    function get_eventos()
    {
        //$this->db->order_by();
        $this->db->get('slide_eventos');
    }
}