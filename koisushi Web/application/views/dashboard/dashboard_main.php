<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Men�</title>
<!-- Bootstrap core CSS -->
<link rel="stylesheet" href="<?= base_url();?>assets/css/stilo_admin.css"/>
<link href="<?= base_url();?>assets/css/bootstrap.css" rel="stylesheet">
<!--<link rel="stylesheet" href="css/stilo_admin.css"/>-->
<!-- Custom styles for this template -->
<!--<link href="css/signin.css" rel="stylesheet">-->
<!--<link href="css/login-admin-pages.css" rel="stylesheet">-->
    
<!-- Web Fonts  -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">
<style type="text/css">
body{  background: #000; background-image:url(http://localhost/koisushi/assets/images/bg.jpg); background-position:center center; background-repeat:no-repeat}
header{ background:#000;/* background-image:url(images/fondo-medio-login.jpg); background-position:bottom;*/ padding-top:0.5em;}
header div img{ padding-bottom:1.5em}
#wellcome{ background:#dd0200; background-image:url(http://localhost/koisushi/assets/images/sombra-wellcome.png); background-position: center center; background-repeat:no-repeat;}
.max-width{ max-width:100%;}
.top{ margin-top:3em;max-width: 800px;}
.margin-botton{ margin-bottom:0.8em;}
#menu_opcion_all{ margin:0px; padding:0px}
#menu_opcion_all li{ list-style:none; display:inline-block}
.contenido{ margin-top:30px; padding:0px;}
</style>
</head>

<body>
<header>
	<div class="container">
    	<img src="<?= base_url();?>assets/images/logo-admin.png" class="max-width" style="max-width:150px; height:auto">
        <div style="float:right; height:100%;"><span id='sesionUser'> 
        <?php echo '<h3>Welcome '. $this->session->userdata('username'). '</h3>'; ?> 
        </span> 
        <a href="<?= base_url();?>/users/logout" style="color:#fff; font-size:1.5em">Salir</a>
        </div>
    </div>
</header>    
<div id="wellcome">
	<div class="container">
    	<img src="<?= base_url();?>assets/images/welcome.png" class="max-width">
    </div>
</div>
<div class="container contenido" style="">
	
	<?php $this->load->view($main_content);?>
</div>
</body>
</html>
