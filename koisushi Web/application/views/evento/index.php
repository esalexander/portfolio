<?php

if(!empty($eventos) and is_array($eventos)):
?>
            <div class="gallery responsive">
            	<div class="mask">
                	<ul class="slideset">
                    <?php
                    foreach($eventos as $event) :
                    ?>
                        <li class="slide" data-transition="slidedown" data-slotamount="10" data-masterspeed="300" data-thumb="http://placehold.it/320x145">
                        	<img src="<?php echo base_url(); ?>uploaded/slide_eventos/<?= $event->foto; ?>" />
                            <div class="caption text-box lfl lfl" data-x="-91" data-y="140" data-speed="300" data-start="800" data-easing="easeOutExpo">							
                            <h1>
                            	<?=(!empty($event->titulo_1))?"<span>".$event->titulo_1."</span>":""?>
                                <?=(!empty($event->titulo_2))?"<span>".$event->titulo_2."</span>":""?>
                            </h1>
                            <div style="" class="tp-leftarrow tparrows default"></div>
                            <div style="" class="tp-rightarrow tparrows default"></div>
                            </div>
                        </li>
                    	<?php
						endforeach;
					?>
                    </ul>
              </div>
              
            <?php endif; ?>
            
            <div class="container">   
			<article class="single-post">
				<div class="two-columns">
					
				</div>
                
                <div class="two-columns form-block">
				<!-- column -->
				<div id="event_text" class="column">
					<h2 style="color:#000">Haz tu reservaci�n para eventos</h2>
					<p>
                    	Contamos con nuestro servicio de Catering o Eventos para sus reuniones corporativas,
                        presentaciones,juntas directivas, celebraciones y cocteles, ya sea dentro o fuera de las instalaciones de su 
                        empresa. 
                    </p>
                    <p>    
                        Contamos con una extensa carta de nuestros platos m�s exquisitos, preparados especialmente para cubrir la necesidad y calidad de su evento.
                    </p>
                    <p>
                        Consumo minimo $100
                    </p>
                    <div class="video-container">
                    <iframe src="https://player.vimeo.com/video/116711402" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                    </div>
				</div>
				<!-- column -->
				<div class="column">
					<h2 style="color:#000">Reservaciones</h2>
					<p class="error"></p>
					<!-- form -->
					<form id="contact" class="form" action="#">
						<fieldset>
							<div class="row">
								<label for="name">Nombre</label>
								<input type="text" name="name">
							</div>
							<div class="row">
								<label for="email2">Email</label>
								<input type="email" name="email">
							</div>
							<div class="row">
								<label for="telefono">Telefono</label>
								<input type="text" name="telefono">
							</div>
							<div class="row">
								<label for="evento">Tipo de evento</label><br>
								<select name="tipo_evento">
										<option value="boda">boda</option>
										<option value="cumplea�os">cumplea�os</option>
										<option value="oficina">oficina</option>
										<option value="otros">otros</option>
								</select>	
								
							</div>
							
							<div class="row">
								<label for="Lugar">Direcci&oacute;n</label>
								<input type="text" name="direccion">
							</div>
							<div class="row">
								<label for="n_personas">N�mero de personas</label>
								<input type="text" name="n_personas">
							</div>
							<div class="row">
								<label for="fecha">Fecha del evento</label>
								<input type="text" name="fecha" id="fecha">
							</div>
							
							<div class="row">
								<label for="message">Mensaje</label>
								<textarea name="message" cols="30" rows="10"></textarea>
							</div>
							<div class="row wrap">
								<input type="submit" value="ENVIAR" style="color:#FFF">
							</div>
						</fieldset>
					</form>
					<p class="success"><span>Gracias</span><br>nos contactaremos lo m�s pronto</p>
				</div>
			</div>
                
                
				
			</article>
		</div>
            
            