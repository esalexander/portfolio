<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
	<meta name="google-site-verification" content="2T-3m9QRik2nhkJJz-VeNhqjkxBtzpB00ZywhACCuyc" />
    <title>KOI</title>
	<link media="all" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/slider-revolution.css">
	<link media="all" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/all.css">
	<link media="all" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/colors/red/style.css" class="color">
    
    <link href='http://fonts.googleapis.com/css?family=Allerta' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/fonts/stylesheet.css" type="text/css" charset="utf-8" />
    
    
    <!-- Owl Carousel Assets -->
    <link href="<?php echo base_url(); ?>assets/carrucel/owl-carousel/owl.carousel.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/carrucel/owl-carousel/owl.theme.css" rel="stylesheet">

    <!-- Prettify -->
    <link href="<?php echo base_url(); ?>assets/carrucel/assets/js/google-code-prettify/prettify.css" rel="stylesheet">
    
    
    <!-- slider sidebar -->
	<link rel="stylesheet" href="ResponsiveSlides.js-master/responsiveslides.css">
  	<link rel="stylesheet" href="ResponsiveSlides.js-master/demo/demo.css">
    
    <style type="text/css">
          ul#menu li a{ font-family:Arial, Helvetica, sans-serif}
		  #buscar{ height:30px; background: rgba(255,255,255,0.1); border: none; padding-left:0.5em; color:#FFF}
		  .div_img_slide{ position:relative;}
		  .div_info_slide{ position:absolute; top:0em; width:100%; background:rgba(0,0,0,0.7); color:#FFF; padding:1em;}
		  .rslides_tabs{ background:#000; }
		  .div_info_slide h3{ color:#FFF; font-family:Arial, Helvetica, sans-serif;}
		  #slider1, .slider1, .slider{ max-height:243px;}
		  
		  .carousel .slide a.hover,
			.article a.hover{
				background:#fff /*F33CA4*/;
				color:#1a1a1a;
			}
			
			.tp-bannershadow{ display:none;}

    </style>
</head>
<body>
	<div id="wrapper">
		
		<!-- panel -->
		<?php 
			$this->load->view('layout/header.php');
		?>
		
		<!-- contenido -->
		<?php
		$this->load->view($main_content);
		?>
					
			
			
		</div>
		
		<!-- footer -->
		<?php $this->load->view('layout/footer.php'); ?>
	</div>
	
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-1.9.1.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins.all.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/twitter/jquery.tweet.min.js"></script>
	
	<script src="<?php echo base_url(); ?>assets/carrucel/owl-carousel/owl.carousel.js"></script>
    <script src="<?php echo base_url(); ?>assets/carrucel/assets/js/google-code-prettify/prettify.js"></script>
	<script src="<?php echo base_url(); ?>assets/carrucel/assets/js/application.js"></script>
    
    <!-- slider sidebar -->
	<script src="<?php echo base_url(); ?>assets/js/ResponsiveSlides.js-master/responsiveslides.min.js"></script>
    
	
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.main.js"></script>
    
    <script>
    $(document).ready(function() {
      $("#owl-demo").owlCarousel({
        autoPlay: 3000,
        items : 4,
        itemsDesktop : [1199,3],
        itemsDesktopSmall : [979,3]
      });
	  
	  
	  // Slideshow 1
	  $("#slider1").responsiveSlides({
	  		maxwidth: 800,
	    	speed: 800,
			auto: true,             // Boolean: Animate automatically, true or false
		  	//speed: 500,            // Integer: Speed of the transition, in milliseconds
		  	timeout: 4000,          // Integer: Time between slide transitions, in milliseconds
		  	pager: true,           // Boolean: Show pager, true or false
		  	nav: false,             // Boolean: Show navigation, true or false
		  	random: false,          // Boolean: Randomize the order of the slides, true or false
		  	pause: false,           // Boolean: Pause on hover, true or false
		  	pauseControls: true,    // Boolean: Pause when hovering controls, true or false
		  	prevText: "Previous",   // String: Text for the "previous" button
		  	nextText: "Next",       // String: Text for the "next" button
		  	maxwidth: "",           // Integer: Max-width of the slideshow, in pixels
		  	navContainer: "",       // Selector: Where controls should be appended to, default is after the 'ul'
		  	manualControls: "",     // Selector: Declare custom pager navigation
		  	namespace: "rslides",   // String: Change the default namespace used
		  	before: function(){},   // Function: Before callback
		  	after: function(){}     // Function: After callback
	  });

    });
    </script>
    
    
		
</body>
</html>