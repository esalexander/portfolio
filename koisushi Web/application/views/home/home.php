<?php
					//if(!empty($slide) and is_array($slide)):
					?>
                        <!-- gallery -->
                    <div class="gallery responsive">
                        <div class="mask">
                            <ul class="slideset">
                        <?php
						foreach($slide as $slid) :
						?>
                            <li class="slide" data-transition="slidedown" data-slotamount="10" data-masterspeed="300">
                                <img src="<?php echo base_url(); ?>uploaded/slide_home/<?php echo $slid->foto; ?>" >
                                <div class="caption text-box lfl lfl" data-x="-91" data-y="140" data-speed="300" data-start="800" data-easing="easeOutExpo">							
                                    <h1>
                                    	<?php echo (!empty($slid->titulo_1))?"<span>".$slid->titulo_1."</span>":""; ?>
                                        <?php echo (!empty($slid->titulo_2))?"<span>".$slid->titulo_2."</span>":""; ?>
                                        <!--<span>ANGEL</span> <span>ROLL</span>-->
                                    </h1>
                                    <a class="more" href="menu_detalle.php?c=<?php echo $slid->idCategoria; ?>" style="font-family:Arial, Helvetica, sans-serif; background:#E51937">VER MÁS</a>
                                    <div style="background:#E51937" class="tp-leftarrow tparrows default"></div>
                                    <div style="background:#E51937" class="tp-rightarrow tparrows default"></div>
                                </div>
                            </li>
                    	<?php
						endforeach;
						?>
                        		</ul>
                            </div>
                        </div>	
                        <?php	
					//endif;
		?>
		<!-- container -->
		<div class="container" >
	

			<!-- widget-blocks -->
			<div class="widget-blocks" style="margin-bottom: 0px;">
				<!-- block -->
				<div class="block" style="padding-bottom: 0px;">
                   <header class="title-box" style="margin-bottom:0px; margin-bottom: 0px; padding-bottom: 10px; padding-top: 15px;"><h3 style="color:#FFF"><a href="promociones.php" style="text-decoration:none; color:#FFF">PROMOCIONES</a></h3></header>
                    <div>
                    	
                        	
                        
                       <div class="links-box" style="position:absolute; bottom:10px; right:10px;">
                            <!-- button-box -->
                            <div class="button-box">
                                <a href="#"><img src="images/btn-like.png" width="64" height="26" alt="image description"></a>
                            </div>
                        </div>
                        
					</div>
				</div>
				<!-- block -->
				<div class="block">
					<div class="holder">
						<header class="title-box" style="margin-bottom: 10px;">
							<!-- <a class="link" href="#">Follow us on Twitter</a> -->
							<h2 style="font-family:Arial, Helvetica, sans-serif"><a href="menus.php" style=" text-decoration:none; color:#FFF">menÚ</a></h2>
						</header>
                        
                        <div>
                            <?php
								if(is_array($menus) and !empty($menus)):
								?>
                                 <div id="owl-demo" class="owl-carousel">
								 <?php
								foreach($menus as $menu) :
								?>
										<?php
										if(!empty($menu->foto)):
										?>
											<div class="item">
                                            	
                                                <a href="menu_detalle.php?c=<?php echo $menu->idCategoria; ?>" title="<?php echo $menu->nombre; ?>" >
                                                	<img src="<?php echo base_url(); ?>uploaded/categorias_web/thumb_<?php echo $menu->foto; ?>" alt="<?php echo $menu->nombre; ?>" style="width:130px; height:auto; padding:0.2em">
                                                </a>
                                                <div style="width:100%; text-align:center"><span style="font-size:11px"><?php echo $menu->nombre; ?></span></div>
                                            </div>
										<?php
										endif;	
									endforeach; 
								 ?>	
                                 </div>
                                <?php	
								endif;
							?>
                        </div>
						
					</div>
				</div>
			</div>