<?php

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!--<link rel="shortcut icon" href="../../assets/ico/favicon.png">-->

    <title>Login de usuarios</title>

    <!-- Bootstrap core CSS -->
    <link href="<?= base_url();?>assets/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <!--<link href="css/signin.css" rel="stylesheet">-->
    <link href="<?= base_url();?>assets/css/login-admin-pages.css" rel="stylesheet">
    
    <!-- Web Fonts  -->
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">
    

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="../../assets/js/html5shiv.js"></script>
      <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
        #cargando{
                text-align:center;
                display:none;
				color:#FFF	
        }
    </style>
  </head>

  <body>
  
  	<div id="container-table">
	<div id="table">
         <div id="fondo-logo-middle">   
            <div id="barra-top-login">
                            <!--Start Form-->
                <?php $attributes = array('id' => 'login_form',
                                          'class' => 'form-signin'); ?>
                <?php echo form_open('users/login',$attributes); ?>
               <!-- <form class="form-signin" id="miForm" action="javascript:void(0);">  -->
                    <div id="center-form-barra">
                        <div class="row">
                            <div class="col-md-2 col-lg-2 pos-relative"><img src="<?= base_url();?>assets/images/ingresar.png" id="img-ingresar" class="max-width"></div>
                            <div class="col-md-4 col-lg-4"><input type="text" class="form-control" id="username" name="username" placeholder="Usuario" autofocus></div>
                            <div class="col-md-4 col-lg-4"> <input type="password" class="form-control" id="password" name="password" placeholder="Password"></div>
                            <div class="col-md-2 col-lg-2"> <button class="btn-entrar" type="submit"><img src="<?= base_url();?>assets/images/entrar.png"></button></div>
                        </div>
                    </div>
               <?php echo form_close();?>
           </div> 
           <div id="central-content">    
               <div ><img src="<?= base_url();?>assets/images/logo-admin.png" style="padding-top:2em; padding-bottom:2em;"></div>
               <div class="texto-login">
               </div>
           </div>
           
	  </div>
            
        </div>
    </div>
 	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-1.9.1.min.js"></script>
 	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>
  </body>
</html>