<!-- container -->
		<div class="container">
			<!-- breadcrumbs -->
			<!--<nav class="breadcrumbs">
				<ul>
					<li><a href="#">Home</a></li>
					<li>Galer�a</li>
				</ul>
			</nav>
			<h1>GALER�A <span style="font-size:28px; color:#666">Albumes</span></h1>-->
            
            
            <h1 style="display:inline-block; font-size:35px; margin-bottom: 0px;">GALER�A</h1>
            <nav class="breadcrumbs" style="display:inline-block; padding-left:1em;  margin-bottom: 0px;">
				<ul>
					<li>ALBUMES</li>
				</ul>
			</nav>
            
			<!-- news -->
			<div id="news" class="articles-section">
				<?php
				if(is_array($albumes) and !empty($albumes)):
				  foreach ($albumes as $album):
				  ?>
                        <!-- article -->
                        <article class="article">
                            <div class="holder">
                                <a href="<?php echo base_url();?>album/detail/<?= $album->idAlbum ?>" class="over_menu">
                                    <!--<img src="http://placehold.it/326x183" width="326" height="183" alt="image description">-->
                                    <img src="<?= base_url(); ?>uploaded/albumes/<?= $album->foto ?>">
                                    <div class="text" style="padding-bottom: 15px; padding-top: 15px;">
                                        <h2 style="font-family: Tahoma, Geneva, sans-serif"><?=$album->nombre?></h2>
                                        <!--<time class="date" datetime="2012-05-27">MAY 27TH, 2012</time>
                                        <span class="more">More</span>-->
                                    </div>
                                </a>
                            </div>
                        </article>
                        <?php	
						endforeach;
				?>
				<?php endif;?>
                
				
			</div>
		
