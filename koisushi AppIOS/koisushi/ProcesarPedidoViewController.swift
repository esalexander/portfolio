//
//  ProcesarPedidoViewController.swift
//  koisushi
//
//  Created by PROGRAMACIÓN on 19/1/17.
//  Copyright © 2017 Elias velasco. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ProcesarPedidoViewController: UIViewController, UITableViewDataSource, UITableViewDelegate,UITextFieldDelegate {

    @IBOutlet weak var txtTelefono: UITextField!
    @IBOutlet weak var txtNombrePedido: UITextField!
    
    @IBOutlet weak var procesarTable: UITableView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var carritos = [Carrito]()
    var carService: CarritoService?
    
    var user = User()
    let preft = UserDefault()
    var direcciones = [Direccion]()
    //var items: [String] = ["Casa", "Oficina", "Hospital"]
    var items = [String]()
    var itemsCheck = [String]()
    var arrRes = [[String:AnyObject]]()
    var pedidoTelString = ""
    var pedidoTitleString = ""
    var pedidoDireccionString = ""
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.user = Variables.user
        
        if user.username != nil{
            if Reachability.isConnectedToNetwork() == true{
                fetchDirecciones(user.cod_generado_user)
            }else{
                Reachability.displayMessage("KOI Sushi", message: "No hay conexión a Internet")
            }
            
        }else if UserDefault.isLoggin(preft)() == true{
            let codigoUser = UserDefault.gettingCodigoUser(preft)()
            if codigoUser != "" {
                
                if Reachability.isConnectedToNetwork() == true{
                    fetchDirecciones(codigoUser)
                }else{
                    Reachability.displayMessage("KOI Sushi", message: "No hay conexión a Internet")
                }
            }
        }
        self.procesarTable.register(UITableViewCell.self, forCellReuseIdentifier: "procesarCell")
        
        let app = Reachability.getContext()
        carService = CarritoService(context: app)
        self.carritos = carService!.getAll()
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        /*let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ProcesarPedidoViewController.dismissKeyboard))
        scrollView.addGestureRecognizer(tap)*/
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.direcciones.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:UITableViewCell = self.procesarTable.dequeueReusableCell(withIdentifier: "procesarCell")! as UITableViewCell
        cell.accessoryType = UITableViewCellAccessoryType.none
        
        let view = UIView()
        view.backgroundColor = UIColor.lightGray.withAlphaComponent(0.1)
        cell.selectedBackgroundView = view
        let direccionObj = self.direcciones[indexPath.row]
        cell.textLabel?.text = direccionObj.titleDir
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath)
        
        if (cell?.accessoryType == UITableViewCellAccessoryType.none){
            cell!.accessoryType = UITableViewCellAccessoryType.checkmark;
            
            let direccionObj = self.direcciones[indexPath.row]
            
            itemsCheck.append(direccionObj.detailDir)
            //print("Se agrego " + direccionObj.detailDir)
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath as IndexPath)?.accessoryType = .none
        let direccionObj = self.direcciones[indexPath.row]
        
        if itemsCheck.contains(direccionObj.detailDir) {
            //print("Se elimino " + direccionObj.detailDir)
            itemsCheck.remove(at: 0)
        }
    }
    
    func countCheck(){
        if itemsCheck.count == 1{
            print("Seleccion de un valor \(itemsCheck[0]) ")
        }else{
            print("debe de seleccionar solo una direccion")
        }
    }
    
    func dismissKeyboard() {
        scrollView.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtTelefono {
            textField.resignFirstResponder()
            txtNombrePedido.becomeFirstResponder()
        }
        if textField == txtNombrePedido{
            textField.resignFirstResponder()
            }
        
        return true
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        scrollView.setContentOffset(CGPoint(x: 0, y: 320), animated: true)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
    
    func clearFields(){
        self.txtTelefono.text = ""
        self.txtNombrePedido.text = ""
        self.items.removeAll()
        self.itemsCheck.removeAll()
    }
    
    func fetchDirecciones(_ userCodigo: String){
        let urlDatos : String = "URLALSERVIDOR+PUERTO+PARA"
        Alamofire.request(urlDatos).responseJSON { (responseData) -> Void in
            if((responseData.result.value) != nil){
                
                let swiftyJsonVar = JSON(responseData.result.value!)
                //print(swiftyJsonVar)
                if let resData = swiftyJsonVar.arrayObject {
                    self.arrRes = resData as! [[String:AnyObject]]
                }
                if self.arrRes.count > 0 {
                    self.direcciones = [Direccion]()
                    for dirDictionary in self.arrRes {
                        
                        let direccionObj = Direccion()
                        direccionObj.idDir = dirDictionary["idDir"] as! String
                        direccionObj.titleDir = dirDictionary["titleDir"] as! String
                        direccionObj.detailDir = dirDictionary["detailDir"] as! String
                        //let detailDir = dirDictionary["detailDir"] as! String
                        
                        //self.items.append(detailDir)
                        /*if self.itemsCheck.count >= 1 {
                            self.itemsCheck.removeAll()
                        }*/
                        self.direcciones.append(direccionObj)
                        
                    }
                    self.procesarTable.reloadData()
                }
            }
        }
    }
    
    
    @IBAction func goingDireccionAction(_ sender: Any) {
        self.deleteUitableElements()
        if itemsCheck.count >= 1{
            itemsCheck.removeAll()
        }
        self.performSegue(withIdentifier: "addDireccionSeg", sender: self)
    }
    
    func deleteUitableElements(){
        if direcciones.count >= 1{
            self.direcciones.removeAll()
            self.procesarTable.reloadData()
        }
    }
    
    @IBAction func goingSendingAction(_ sender: Any) {
        if self.carritos.count >= 1{
                self.datosAEnviar()
        }else{
            Reachability.displayMessage("KOI Sushi", message: "Debe de seleccionar producto")
        }
        
    }
    
    func datosAEnviar(){
        var varBool : Bool = false
        pedidoTelString = txtTelefono.text!
        pedidoTitleString = txtNombrePedido.text!
        
        let telString = pedidoTelString.trimmingCharacters (
            in: CharacterSet.whitespacesAndNewlines
        )
        
        let titleString = pedidoTitleString.trimmingCharacters (
            in: CharacterSet.whitespacesAndNewlines
        )
        
        if itemsCheck.count < 1{
            Reachability.displayMessage("KOI Sushi", message: "Debe de Seleccionar una Direccion")
        
        }else if pedidoTelString.isEmpty || pedidoTitleString.isEmpty{
            Reachability.displayMessage("KOI Sushi", message: "Hay algun campo vacio")
            
        }else if telString.characters.count > 8 || telString.characters.count < 8   {
            Reachability.displayMessage("KOI Sushi", message: "Numero de telefono debe de ser de 8 digitos")
        }else if titleString.characters.count < 5 {
            Reachability.displayMessage("KOI Sushi", message: "Titulo del pedido debe tener longitud mas de 5 digitos No espacios")
        }else if Reachability.isConnectedToNetwork() == true{
                varBool = true
        }else{
            Reachability.displayMessage("KOI Sushi", message: "No hay conexión a Internet")
        }
            
        if varBool == true{
            
                self.pedidoTelString = telString
                self.pedidoTitleString = titleString
                self.pedidoDireccionString = itemsCheck[0]
                self.clearFields()
                self.performSegue(withIdentifier: "sendingSegue", sender: self)
        }
        
    }

    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "addDireccionSeg"){
            _ = segue.destination as! AgregarDireccionViewController
        }else if (segue.identifier == "sendingSegue"){
            let sendingVC = segue.destination as! SendingJsonViewController
            sendingVC.titlePedido = self.pedidoTitleString
            sendingVC.telefono = self.pedidoTelString
            sendingVC.direccion = self.pedidoDireccionString
            
            
        }
    }
    

}
