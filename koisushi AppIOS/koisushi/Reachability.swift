//
//  Reachability.swift
//  koisushi
//
//  Created by Elias velasco on 10/1/17.
//  Copyright © 2017 Elias velasco. All rights reserved.
//
import SystemConfiguration
import CoreData



open class Reachability {
        
    class func isConnectedToNetwork() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        /*let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            SCNetworkReachabilityCreateWithAddress(nil, UnsafePointer($0))
        }*/
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self,capacity:1)
                {
             SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        })else{
            return false
        }
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    
    class func displayMessage(_ title: String, message: String){
        let ac = UIAlertController(title: title, message: message, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title:"OK", style: .default, handler: nil))
        let rootVC = UIApplication.shared.keyWindow?.rootViewController
        rootVC?.present(ac, animated: true){}
    }
    
    class func displayActionSheet(_ title: String, message: String){
        let myController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.actionSheet)
        myController.addAction(UIAlertAction(title: "Option 1", style: UIAlertActionStyle.default, handler: { action in
            //println("Option 1")
        }))
        myController.addAction(UIAlertAction(title: "Option 2", style: UIAlertActionStyle.default, handler: { action in
            //println("Option 2")
        }))
        myController.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        let rootVC = UIApplication.shared.keyWindow?.rootViewController
        rootVC?.present(myController,animated: true){}
    }
    
    
    class func getContext() -> NSManagedObjectContext{
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
        
    }
    
    
}
