//
//  DetalleLugarViewController.swift
//  mimaps
//
//  Created by Elias velasco on 8/12/16.
//  Copyright © 2016 Elias velasco. All rights reserved.
//

import UIKit

class DetalleLugarViewController: UIViewController {

    @IBOutlet weak var btnHowTo: UIButton!
    @IBOutlet weak var imgLugar: UIImageView!
    @IBOutlet weak var telefonoLugar: UILabel!
    @IBOutlet weak var btnLlamar: UIButton!
    @IBOutlet weak var direccionLugar: UILabel!
    
    @IBOutlet weak var lblHorariosLunes: UILabel!
    @IBOutlet weak var lblHorariosJueves: UILabel!
    @IBOutlet weak var lblHorariosSabados: UILabel!
    
    @IBOutlet weak var lblHorariosTextLunes: UILabel!
    @IBOutlet weak var lblHorariosTextJueves: UILabel!
    @IBOutlet weak var lblHorariosTextSabados: UILabel!
    
    var googleMapsString : String! = ""
    var dirNavegadorString: String! = ""
    var telefonoString: String! = ""
    
    
    
    var Lugar : Lugar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        btnHowTo.layer.cornerRadius = 10
        if let titulo = Lugar?.titulo{
            self.title = titulo
        }
        
        if let image = Lugar?.imageDetail{
            imgLugar.image = UIImage(named: "\(image)")
        }
        direccionLugar.text = Lugar?.direccion
        googleMapsString = Lugar?.direccionGoogleMaps
        dirNavegadorString = Lugar?.direccionNavegador
        lblHorariosLunes.text = Lugar?.horarioLunes
        lblHorariosJueves.text = Lugar?.horarioJueves
        lblHorariosSabados.text = Lugar?.horarioSabado
        telefonoLugar.text = "+503 " + (Lugar?.telefono)!
        telefonoString = Lugar!.telefono
        
        /*if ((Lugar?.titulo.compare("KOI Plaza Madero")) != nil){
            
        }else{
            lblHorariosTextLunes.text = "ALGO LOCO"
        }*/
        
        if(Lugar?.titulo.caseInsensitiveCompare("KOI Las Azaleas") == ComparisonResult.orderedSame){
            lblHorariosTextLunes.text = "Lunes - Martes"
            lblHorariosTextJueves.text = "Miércoles - Jueves - Domingo"
            lblHorariosTextSabados.text = "Viernes - Sabado"
            lblHorariosSabados.text = "sab 12:pm a 11pm"
            
        }

        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(DetalleLugarViewController.callToPlace))
        tap.numberOfTapsRequired = 1
        telefonoLugar.addGestureRecognizer(tap)
    }
    
    @IBAction func comoLlegarAction(_ sender: AnyObject) {
        
        let testURL = URL(string: "comgooglemaps://")!

        if UIApplication.shared.canOpenURL(testURL) {
            let directionsRequest = googleMapsString
            open(scheme: directionsRequest!)
        } else {
            self.openUrlWebBrowser()
        }
    }
    
    func openUrlWebBrowser(){
        let directionsRequest = dirNavegadorString
        open(scheme: directionsRequest!)
    }
    
    func callToPlace(){
        if let phonePosition = self.telefonoString{
            print(phonePosition)
            if let url = URL(string: "tel://\(phonePosition)"), UIApplication.shared.canOpenURL(url){
            open(scheme: "tel://\(phonePosition)")
            }else {
            print("No se puede por el dispositivo")
        
            }
        }
    }
    

    @IBAction func callButtonAction(_ sender: AnyObject) {
        
        self.callToPlace()
    }
    
    func open(scheme: String) {
        if let url = URL(string: scheme) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:],
                                          completionHandler: {
                                            (success) in
                                            //print("Open \(scheme): \(success)")
                })
            } else {
                let success = UIApplication.shared.openURL(url)
                print("Open \(scheme): \(success)")
            }
        }
    }

}
