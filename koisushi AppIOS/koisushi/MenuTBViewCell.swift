//
//  MenuTBViewCell.swift
//  mimaps
//
//  Created by Elias velasco on 24/11/16.
//  Copyright © 2016 Elias velasco. All rights reserved.
//

import UIKit

class MenuTBViewCell: UITableViewCell {
    
    
    @IBOutlet weak var imgBackMenu: UIImageView!

    @IBOutlet weak var titleBackMenu: UILabel!
    
    @IBOutlet weak var subTitleBackMenu: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
