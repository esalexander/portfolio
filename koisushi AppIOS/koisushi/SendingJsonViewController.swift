//
//  SendingJsonViewController.swift
//  koisushi
//
//  Created by Elias velasco on 1/2/17.
//  Copyright © 2017 Elias velasco. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import MessageUI

class SendingJsonViewController: UIViewController{
    
    @IBOutlet weak var lblConfirmacion: UILabel!
    @IBOutlet weak var imagenConfirmacion: UIImageView!
    
    var usuario: String!
    var telefono : String!
    var direccion : String!
    var titlePedido: String!
    
    var usuarioEmail: String! = ""
    
    var carritos = [Carrito]()
    var carService: CarritoService?
    var user =  User()
    let preft = UserDefault()
    var network : Bool! = false
    var itemPayString: String!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        let app = Reachability.getContext()
        carService = CarritoService(context: app)
        self.completeGetCarritos()
        self.logginCheck()
    }
    
    func completeGetCarritos(){
        self.carritos = carService!.getAll()
    }
    
    func logginCheck() {
        if UserDefault.isLoggin(preft)() == true{
            
            let codigoUser = UserDefault.gettingCodigoUser(preft)()
            if codigoUser != "" {
                if Reachability.isConnectedToNetwork() == true{
                    self.usuario = codigoUser
                    self.usuarioEmail = UserDefault.gettingEmailUser(preft)()
                    //print("llega aqui")
                    self.sendingAlamofireValue()
                    
                }else{
                    Reachability.displayMessage("KOI Sushi", message: "No hay conexión a Internet")
                }
                
            }
        }else if let codigo = Variables.user.cod_generado_user {
                self.usuario = codigo
                if let correo = Variables.user.diruser{
                    self.usuarioEmail = correo
                }
                self.sendingAlamofireValue()
        }else{
            
        }
    }
    
    func sendingAlamofireValue(){
        let urlString : String = "URLALSERVIDOR+PUERTO+PARA"
        
        var parameters = Parameters()
        var i = 0
        if carritos.count >= 1 {
            for carro in carritos {
                let itemString = carro.codigo! + "-" + "\(carro.cantidad!)" + "-" + "\(carro.comentario!)"
                
                parameters.updateValue(itemString, forKey: "item\(i)")
                i = i + 1
            }
        }
        parameters.updateValue(telefono, forKey: "tel")
        parameters.updateValue(direccion, forKey: "address")
        parameters.updateValue(usuario, forKey: "usuario")
        parameters.updateValue(titlePedido, forKey: "nickpedido")
        
        //debugPrint(parameters)
        
        Alamofire.request(urlString,method: .post,parameters: parameters).responseJSON { (responseData) -> Void in
            if((responseData.result.value) != nil){
                
                let swiftyJsonVar = JSON(responseData.result.value!)
                //print(swiftyJsonVar)
                
                let resultado  = swiftyJsonVar["resultado"].stringValue
                if resultado == "1"{
                    UIView.animate(withDuration: 0.5, animations: {
                        self.imagenConfirmacion.image = UIImage(named:"done")
                        self.lblConfirmacion.text = "Su pedido se ha Realizado...Espere nuestro llamado"
                        self.imagenConfirmacion.alpha = 1
                        self.lblConfirmacion.alpha = 1
                    })
                    self.carService!.deleteAllRequest()
                }else if resultado == "0"{
                    UIView.animate(withDuration: 0.5, animations: {
                        self.imagenConfirmacion.image = UIImage(named:"fail")
                        self.lblConfirmacion.text = "Hubo un problema...en el registro del pedido"
                        self.imagenConfirmacion.alpha = 1
                        self.lblConfirmacion.alpha = 1
                    })
                }
            }
        }
    }//fin sendingAlamofireValues
 
    
    
    
}
