//
//  locationcell.swift
//  mimaps
//
//  Created by Elias velasco on 1/11/16.
//  Copyright © 2016 Elias velasco. All rights reserved.
//

import UIKit

class locationcell: UITableViewCell {
    
    @IBOutlet weak var palceImage: UIImageView!
    
    @IBOutlet weak var placeTitle: UILabel!

    @IBOutlet weak var placeSubTitle: UILabel!
}
