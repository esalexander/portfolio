//
//  Lugar.swift
//  koisushi
//
//  Created by Elias velasco on 22/1/17.
//  Copyright © 2017 Elias velasco. All rights reserved.
//

import UIKit

class Lugar {
    var titulo : String!
    var direccion : String!
    var image: String!
    var imageDetail: String!
    var horarioLunes: String!
    var horarioJueves: String!
    var horarioSabado: String!
    var direccionGoogleMaps: String!
    var direccionNavegador: String!
    var telefono: String!
    
    
    init(titulo:String, direccion : String, image: String, imageDetail: String, horarioLunes: String, horarioJueves: String , horarioSabado: String, direccionGoogleMaps: String, direccionNavegador: String, telefono: String ){
        self.titulo = titulo
        self.direccion = direccion
        self.image = image
        self.imageDetail = imageDetail
        self.horarioLunes = horarioLunes
        self.horarioJueves = horarioJueves
        self.horarioSabado = horarioSabado
        self.direccionGoogleMaps = direccionGoogleMaps
        self.direccionNavegador = direccionNavegador
        self.telefono = telefono
    }
    
    init(lugaresDictionary: [String: AnyObject]){
        self.titulo = lugaresDictionary["titulo"] as? String
        self.direccion = lugaresDictionary["direccion"] as! String
        self.image = lugaresDictionary["image"] as! String
        self.imageDetail = lugaresDictionary["imagedetail"] as! String
        self.horarioLunes = lugaresDictionary["horarioslunes"] as! String
        self.horarioJueves = lugaresDictionary["horariosjueves"] as! String
        self.horarioSabado = lugaresDictionary["horariossabados"] as! String
        self.direccionGoogleMaps = lugaresDictionary["direcciongooglemaps"] as! String
        self.direccionNavegador = lugaresDictionary["direccionnavegador"] as! String
        self.telefono = lugaresDictionary["telefono"] as! String
    }
    
    static func localAllLugares() -> [Lugar] {
        var lugares = [Lugar]()
        let jsonFile = Bundle.main.path(forResource: "lugares", ofType: "json")
        let jsonData = try? Data(contentsOf: URL(fileURLWithPath: jsonFile!))
        if let jsonDictionary = NetworkService.parseJSONFromData(jsonData) {
            let lugarDictionaries = jsonDictionary["lugares"] as! [[String : AnyObject]]
            for lugDictionary in lugarDictionaries {
                let newLugar = Lugar(lugaresDictionary: lugDictionary)
                lugares.append(newLugar)
            }
        }
        
        return lugares
    }
    
    
}
