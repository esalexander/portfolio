//
//  miHistorialViewController.swift
//  mimaps
//
//  Created by Elias velasco on 6/12/16.
//  Copyright © 2016 Elias velasco. All rights reserved.
//
import UIKit
import Alamofire
import SwiftyJSON
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class miHistorialViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var historialTable: UICollectionView!
    
    var hystoriales = [History]()
    var user = User()
    let preft = UserDefault()
    var spinner = UIActivityIndicatorView()
    var arrRes = [[String:AnyObject]]()
    
    func fetchHistoriales(_ userCodigo: String){
        let urlDatos : String = "URLALSERVIDOR+PUERTO+PARA"
        Alamofire.request(urlDatos).responseJSON { (responseData) -> Void in
            if((responseData.result.value) != nil){
                
                let swiftyJsonVar = JSON(responseData.result.value!)
            
            if let resData = swiftyJsonVar.arrayObject {
                self.arrRes = resData as! [[String:AnyObject]]
            }
            if self.arrRes.count > 0 {
                self.hystoriales = [History]()
                for histoDictionary in self.arrRes {
                    
                    let history = History()
                    history.idPedido = histoDictionary["idPedido"] as! String
                    history.fecha = histoDictionary["fecha"] as? String
                    history.nickPedido = histoDictionary["nickpedido"] as? String
                    
                    self.hystoriales.append(history)
                    
                }
                self.historialTable.reloadData()
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.user = Variables.user
        
        historialTable.dataSource = self
        historialTable.delegate = self
        
        
        if user.username != nil{
            if Reachability.isConnectedToNetwork() == true{
                fetchHistoriales(user.cod_generado_user)
            }else{
                Reachability.displayMessage("KOI Sushi", message: "No hay conexión a Internet")
            }
            
        }else if UserDefault.isLoggin(preft)() == true{
            let codigoUser = UserDefault.gettingCodigoUser(preft)()
            if codigoUser != "" {
                
                if Reachability.isConnectedToNetwork() == true{
                    fetchHistoriales(codigoUser)
                }else{
                    Reachability.displayMessage("KOI Sushi", message: "No hay conexión a Internet")
                }
                
            }
        }
        
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return hystoriales.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.width/2)-1, height: 175)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = historialTable.dequeueReusableCell(withReuseIdentifier: "historialcell", for: indexPath) as! historialCell
        if cell.isSelected{
            cell.backgroundColor = UIColor.white;
        }else{
            //cell.backgroundColor = UIColor.redColor()
        }
        
        let history = hystoriales[indexPath.row]
        cell.titleHist.text = history.nickPedido
        cell.codigoHist.text = history.idPedido
        cell.fechaHist.text = history.fecha
        
        return cell
        
    }
    
    func getIndexPathForSelectedCell() -> IndexPath? {
        var indexPath: IndexPath?
        if historialTable.indexPathsForSelectedItems?.count > 0{
            indexPath = historialTable.indexPathsForSelectedItems![0] as IndexPath
        }
        return indexPath
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let indexPath = getIndexPathForSelectedCell(){
        
            let detallePedidoVC:DetallePedidoViewController = segue.destination as! DetallePedidoViewController
            let history = hystoriales[indexPath.row]
            detallePedidoVC.paramRecived = history.idPedido
        }
    }
    
    
    
}
