//
//  Producto.swift
//  koisushi
//
//  Created by Elias velasco on 13/12/16.
//  Copyright © 2016 Elias velasco. All rights reserved.
//

import Foundation

class Producto {
    var nombre: String!
    var codigo: String!
    var precio: String!
    var descripcion: String!
    var cantidad: String!
    var link: String!
    
    init(nombre: String,codigo: String,precio: String, descripcion:String ,cantidad: String, link: String){
        self.nombre = nombre
        self.codigo = codigo
        self.precio = precio
        self.descripcion = descripcion
        self.cantidad = cantidad
        self.link = link
     }
}
