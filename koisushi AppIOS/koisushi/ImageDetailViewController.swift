//
//  ImageDetailViewController.swift
//  koisushi
//
//  Created by Elias velasco on 29/1/17.
//  Copyright © 2017 Elias velasco. All rights reserved.
//

import UIKit

class ImageDetailViewController: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imageScroll: UIImageView!
    var imageDetail: UIImage!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //scrollView.delegate = self
        imageScroll.image = imageDetail
        self.scrollView.minimumZoomScale = 1.0
        self.scrollView.maximumZoomScale = 10.0
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ImageDetailViewController.dismissViewController))
        imageScroll.addGestureRecognizer(tap)

    }
    
    func dismissViewController(){
        self.dismiss(animated: true, completion: {});
    }
    
    func viewForZooming(in scrollView : UIScrollView) -> UIView? {
        return self.imageScroll
    }
    
    
    
    

}
