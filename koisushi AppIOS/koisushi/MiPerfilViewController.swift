//
//  MiPerfilViewController.swift
//  mimaps
//
//  Created by Elias velasco on 6/12/16.
//  Copyright © 2016 Elias velasco. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class MiPerfilViewController: UIViewController {

    @IBOutlet weak var userNickname: UILabel!
    @IBOutlet weak var userEmail: UILabel!
    
    @IBOutlet weak var userNumPedidos: UILabel!
    
    @IBOutlet weak var userCodigo: UILabel!
    
    var user =  User()
    let preft = UserDefault()
    //var spinner = UIActivityIndicatorView()
    
    var urlStr: String = "URLALSERVIDOR+PUERTO+PARA"

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        if UserDefault.isLoggin(preft)() == true{

            let nombreUser = UserDefault.gettingNameUser(preft)()
             if nombreUser != "" {
                
                if Reachability.isConnectedToNetwork() == true{
                    //print("llega a user defaul " + nombreUser + "hay espacio")
                    loadingDatos(nombreUser)
                }else{
                    Reachability.displayMessage("KOI Sushi", message: "No hay conexión a Internet")
                }
                
            }
        }else if user.username != nil {
            
            if Reachability.isConnectedToNetwork() == true{
                //print("llega variable user " + user.username + "hay espacio")
                loadingDatos(user.username)
            }else{
                Reachability.displayMessage("KOI Sushi", message: "No hay conexión a Internet")
            }
            
        }else{
            //print("Usuario no logueado")
            self.performSegue(withIdentifier: "loggedOut", sender: self)
        }

    }
    @IBAction func seeHistorial(_ sender: AnyObject) {
        self.performSegue(withIdentifier: "verHistorial", sender: self)
    }

    @IBAction func loggedOut(_ sender: AnyObject) {
            user = User()
            self.performSegue(withIdentifier: "loggedOut", sender: self)
    }
   
    
    func loadingDatos(_ userName:String){
        let parameters: Parameters = ["user":userName]
        let urlDatos: String = "URLALSERVIDOR+PUERTO+PARA"
        
        Alamofire.request(urlDatos,parameters: parameters).responseJSON { (responseData) -> Void in
            if((responseData.result.value) != nil){
            
            let swiftyJsonVar = JSON(responseData.result.value!)
            let codigo = swiftyJsonVar[0]["coduser"].stringValue
            let email = swiftyJsonVar[0]["emailuser"].stringValue
            let codgen = swiftyJsonVar[0]["cod_generado_user"].stringValue
            let cantidad = swiftyJsonVar[0]["cantidad"].stringValue
            
            self.user = User()
            self.user.cod_generado_user = codgen
            self.user.username = codigo
            self.user.diruser = email
            
            //print("este \(codigo) usuario \(email) codgenerado \(codgen)")
            self.userNickname.text = codigo
            self.userEmail.text = email
            self.userNumPedidos.text = "Numero de Pedidos \(cantidad)"
            self.userCodigo.text = codgen
            self.preft.createEmail(email)
                //print(codigo + email + cantidad + codgen)
            
            }
        }
    }

    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "verHistorial"){
            let destino = segue.destination as! miHistorialViewController
            destino.user = self.user
        }
        
        if (segue.identifier == "loggedOut"){
            UserDefault.logoutUser(preft)()
            _ = segue.destination as! LoginViewController
            //destino.user = self.user
        }
    }

}
