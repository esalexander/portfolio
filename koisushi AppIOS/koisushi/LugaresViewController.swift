//
//  ViewController.swift
//  mimaps
//
//  Created by Elias velasco on 1/11/16.
//  Copyright © 2016 Elias velasco. All rights reserved.
//

import UIKit

class LugaresViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    var lugares = [Lugar!]()
    
    @IBOutlet weak var tablePlaces: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        lugares = Lugar.localAllLugares()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lugares.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tablePlaces.dequeueReusableCell(withIdentifier: "milocation", for: indexPath) as! locationcell

        let lugar = lugares[indexPath.row]!
        
        
        cell.placeTitle.text = lugar.titulo!
        cell.placeSubTitle.text = lugar.direccion!
        cell.palceImage.image =  UIImage(named: "\(lugar.image!)")
        
        return cell
    }
    
    func getIndexPathForSelectedCell() -> IndexPath? {
        let numero = tablePlaces.indexPathForSelectedRow!
        return numero
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "irDetalleLugar"){
            if let indexPath = getIndexPathForSelectedCell(){
                let detalleLugarVC :DetalleLugarViewController = segue.destination as! DetalleLugarViewController
                
                let lugar = lugares[indexPath.row]!
                detalleLugarVC.Lugar = lugar
            }
        }
    }
    
    


}

