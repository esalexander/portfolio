//
//  miAlertDialog.swift
//  koisushi
//
//  Created by Elias velasco on 16/12/16.
//  Copyright © 2016 Elias velasco. All rights reserved.
//

import Foundation

extension UIViewController{
    
    /**
    Shows an alert with a title and a message
    */
    func showAlert(_ title: String, message:String) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        // show the alert with a "ok" button that will close the alert
        let okAction = UIAlertAction(title: "ok", style: UIAlertActionStyle.default) { (action)-> Void in
            self.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(okAction)
        
        // show alert controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    /**
    Shows a loading indicator and disables user interaction with the app until it is hidden
    */
    func showModalSpinner()->UIActivityIndicatorView{
        
        // user cannot interact with the app while the spinner is visible
        UIApplication.shared.beginIgnoringInteractionEvents()
        
        var indicator = UIActivityIndicatorView()
        
        indicator = UIActivityIndicatorView(frame: self.view.frame)
        indicator.center = self.view.center
        indicator.backgroundColor = UIColor(white: 0.0, alpha: 0.5)
        indicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        indicator.hidesWhenStopped = true
        indicator.startAnimating()
        
        self.view.addSubview(indicator)
        
        return indicator
    }
    
    /**
    Hides the loading indicator and enables user interaction with the app
    */
    func hideModalSpinner(_ indicator: UIActivityIndicatorView){
        
        indicator.stopAnimating()
        indicator.isHidden = true
        
        // user can interact again with the app
        UIApplication.shared.endIgnoringInteractionEvents()
    }
}
