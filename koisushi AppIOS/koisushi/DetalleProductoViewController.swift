//
//  DetalleProductoViewController.swift
//  mimaps
//
//  Created by Elias velasco on 8/12/16.
//  Copyright © 2016 Elias velasco. All rights reserved.
//

import UIKit
import CoreData

class DetalleProductoViewController: UIViewController, UITextFieldDelegate, UIScrollViewDelegate, UIGestureRecognizerDelegate {

    @IBOutlet weak var imgProducto: UIImageView!
    @IBOutlet weak var nombreProducto: UILabel!
    @IBOutlet weak var precioProducto: UILabel!
    @IBOutlet weak var descProducto: UILabel!
    
    @IBOutlet weak var txtComentario: UITextField!
    @IBOutlet weak var txtCantidad: UITextField!
    
    @IBOutlet weak var btnMenos: UIButton!
    @IBOutlet weak var btnMas: UIButton!
    @IBOutlet weak var btnAgregar: UIButton!
    @IBOutlet weak var btnIrCarrito: UIButton!
    
    //@IBOutlet weak var btnContador: UIBarButtonItem!
    var carService: CarritoService?
    
    @IBOutlet weak var acvtivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var scrollView: UIScrollView!
    var l_miproducto:Producto!
    var carritoObj: Carrito!
    
    var delay4s = Timer()
    var alertController = UIAlertController()
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        let app = Reachability.getContext()
        carService = CarritoService(context: app)
        
        self.contadorButton()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        roundButtons()
        acvtivityIndicator.startAnimating()
        
        if l_miproducto != nil{
            let strUrl = l_miproducto?.link
            let trimmedString = strUrl!.trimmingCharacters(
                in: CharacterSet.whitespacesAndNewlines
            )
            
            let urlRecortada = removeWord(trimmedString,word: "thum")
            
            let imageUrl = URL(string: urlRecortada)
            imgProducto.sd_setImage(with: imageUrl){ (image,error,imageCacheType,imageUrl) in
                if image != nil{
                    self.acvtivityIndicator.stopAnimating()
                    self.imgProducto.contentMode = UIViewContentMode.scaleAspectFill
                }else{
                    self.acvtivityIndicator.stopAnimating()
                    self.imgProducto.contentMode = UIViewContentMode.scaleAspectFill
                    self.imgProducto.image = UIImage(named:"generica")
                }
            }
            
            nombreProducto.text = l_miproducto!.nombre
            precioProducto.text = l_miproducto!.precio
            descProducto.text = l_miproducto!.descripcion
            
        }
        
        txtComentario.delegate = self
        scrollView.delegate = self
        
        let allow_multiple_touches = UITapGestureRecognizer(target: self, action: #selector(DetalleProductoViewController.dismissKeyboard))
        allow_multiple_touches.numberOfTapsRequired = 1
        allow_multiple_touches.cancelsTouchesInView = false
        
        scrollView.addGestureRecognizer(allow_multiple_touches)
        /*let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(DetalleProductoViewController.dismissKeyboard))
        scrollView.addGestureRecognizer(tap)*/
    }
    
    @IBAction func imgDetailAction(_ sender: Any) {
        self.openDetailImage()
    }
    
    
    func removeWord( _ text:String, word:String ) -> String {
        var result = ""
        let textCharArr = Array( text.characters )
        let wordCharArr = Array( word.characters )
        var possibleMatch = ""
        var i = 0, j = 0
        while i < textCharArr.count {
            if textCharArr[ i ] == wordCharArr[ j ] {
                if j == wordCharArr.count - 1 {
                    possibleMatch = ""
                    j = 0
                }
                else {
                    possibleMatch.append( textCharArr[ i ] )
                    j += 1
                }
            }
            else {
                
                result.append( possibleMatch ) 
                possibleMatch = ""
                if j == 0 {
                    result.append( textCharArr[ i ] )
                }
                else {
                    j = 0
                    i -= 1
                }
            }
            i += 1
        }
        return result
    }
    
    func roundButtons(){
        self.btnMenos.layer.cornerRadius = 6
        self.btnMas.layer.cornerRadius = 6
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == txtComentario {
            textField.resignFirstResponder()
        }
        
        return true
    }
    
    
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        scrollView.endEditing(true)
        
        self.imgProducto.resignFirstResponder()
        self.imgProducto.becomeFirstResponder()
    }
    
    func openDetailImage(){
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "imageDetail") as! ImageDetailViewController
        if let imagen = self.imgProducto.image{
            vc.imageDetail = imagen
        }
        self.present(vc, animated: true, completion: nil)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        scrollView.setContentOffset(CGPoint(x: 0, y: 220), animated: true)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
    
    func clearFields(){
        self.txtComentario.text = ""
    }
    
    @IBAction func incrementAction(_ sender: AnyObject) {
        var valor: Int! = Int(txtCantidad.text!)
        if valor >= 1 {
            valor = valor + 1
            txtCantidad.text = String(valor)
        }
        //print(valor)
    }

    @IBAction func decrementAction(_ sender: AnyObject) {
        var valor:Int! = Int(txtCantidad.text!)
        if valor > 1 {
        valor = valor - 1
        txtCantidad.text = String(valor)
        }
    }
    
    @IBAction func addProductAction(_ sender: AnyObject) {
        let comentario = txtComentario.text!
        
        let nombre = nombreProducto.text
        //let descript = descProducto.text
        
        let stringCantidad = txtCantidad.text
        /*if let enteroCantidad = Int(stringCantidad!) {
            
        }
        let numberCantidad = NSNumber(value: Int(stringCantidad!)!)
        */
        if nombre != "" {
            
            let precioString = l_miproducto.precio!.trimmingCharacters (
                in: CharacterSet.whitespacesAndNewlines
            )
            
            let resultado = (Double(precioString)! * Double(txtCantidad.text!)!)
            //self.alertDialog()
            
            if let price = Double(precioString){
               _ = carService!.create(
                    (l_miproducto!.codigo)!,
                    nombre: (l_miproducto!.nombre)!,
                    desc: l_miproducto!.descripcion!,
                    //cant: (Int(txtCantidad.text!))!,
                    cant: NSNumber(value: Int(stringCantidad!)!),
                    precio: NSNumber(value:price),
                    coment: comentario,
                    total: NSNumber(value:resultado)
                )
                
                clearFields()
                self.alertDialog()
                carService!.saveChanges()
            }else{
                print("no se ha guardado el valor")
            }
            
        }
    }
    
    func contadorButton(){
        let cont = self.carService!.contandoRegistros()
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "carritoicon"), for: .normal)
        button.setTitle("\(cont)", for: .normal)
        button.sizeToFit()
        button.addTarget(self,action: #selector(DetalleProductoViewController.verCarrito(_:)), for: .touchUpInside)
        let rightBarButton = UIBarButtonItem()
        rightBarButton.customView = button
        self.navigationItem.rightBarButtonItem = rightBarButton
        
    }
    
    func closeDialog(){
        delay4s = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(DetalleProductoViewController.dismissDialog), userInfo: nil, repeats: false)
    }
    
    func dismissDialog(){
        self.alertController.dismiss(animated: true, completion: nil)
        self.contadorButton()
    }
    
    func verCarrito(_ sender: UIBarButtonItem){
        DispatchQueue.main.async(execute: { () -> Void in
            
            self.performSegue(withIdentifier: "seeCarShopping", sender: self)
        })
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "seeCarShopping"){
            _ = segue.destination as! MiPedidoViewController
        }
    }

    
    func alertDialog(){
        alertController = UIAlertController(title: "Guardando...", message: "Producto guardado", preferredStyle: UIAlertControllerStyle.alert)
        self.present(alertController, animated: true, completion: nil)
        self.closeDialog()
    }

}
