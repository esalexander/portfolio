//
//  UserDefault.swift
//  mimaps
//
//  Created by Elias velasco on 6/12/16.
//  Copyright © 2016 Elias velasco. All rights reserved.
//
import UIKit

class UserDefault{
    
    var prefName = "nameuser"
    var prefIsLogin = "IsLoggedIn"
    var prefCodigo = "codigoapp"
    var prefEmail = "emailuser"
    let prefs = UserDefaults.standard
    
    init(){
        
    }
    
    func createLogin(_ name: String, loged: Bool, codigo: String, email: String){
        prefs.setValue(name, forKey: prefName)
        prefs.setValue(loged, forKey: prefIsLogin)
        prefs.setValue(codigo, forKey: prefCodigo)
        prefs.setValue(email, forKey: prefEmail)
    }
    
    func createEmail(_ email:String){
        prefs.setValue(email, forKey: prefEmail)
    }
    
    func gettingNameUser() -> String {
        let nombre = prefs.string(forKey: prefName)
        return nombre!
    }
    
    func gettingCodigoUser() -> String {
        let codigo = prefs.string(forKey: prefCodigo)
        return codigo!
    }
    
    
    func gettingEmailUser() -> String {
        let email = prefs.string(forKey: prefEmail)
        return email!
    }
    
    func isLoggin () -> Bool{
        let validar = prefs.bool(forKey: prefIsLogin)
        return validar
    }
    
    func logoutUser(){
        prefs.setValue(nil, forKey: prefName)
        prefs.setValue(nil, forKey: prefIsLogin)
        prefs.setValue(nil, forKey: prefCodigo)
        prefs.setValue(nil, forKey: prefEmail)
    }
    
}

