//
//  ProcesarPedidoViewControlle.swift
//  koisushi
//
//  Created by Elias velasco on 1/3/17.
//  Copyright © 2017 Elias velasco. All rights reserved.
//

import UIKit

class ProcesarPedidoViewControl: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var txtTelefono: UITextField!
    @IBOutlet weak var txtNombrePedido: UITextField!
    @IBOutlet weak var txtDireccionPedido: UILabel!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var backUiView: UIView!
    @IBOutlet weak var uiViewLabel: UIView!
    
    
    var carritos = [Carrito]()
    var carService: CarritoService?
    
    var user = User()
    let preft = UserDefault()
    var pedidoTelString = ""
    var pedidoTitleString = ""
    var pedidoDireccionString = ""
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.user = Variables.user
        
        if user.username != nil{
            if Reachability.isConnectedToNetwork() == true{
                //fetchDirecciones(user.cod_generado_user)
            }else{
                Reachability.displayMessage("KOI Sushi", message: "No hay conexión a Internet")
            }
            
        }else if UserDefault.isLoggin(preft)() == true{
            let codigoUser = UserDefault.gettingCodigoUser(preft)()
            if codigoUser != "" {
                
                if Reachability.isConnectedToNetwork() == true{
                    //fetchDirecciones(codigoUser)
                }else{
                    Reachability.displayMessage("KOI Sushi", message: "No hay conexión a Internet")
                }
            }
        }
        //self.procesarTable.register(UITableViewCell.self, forCellReuseIdentifier: "procesarCell")
        
        if let dirSelected = Variables.direccionObjGlobal.titleDir{
            txtDireccionPedido.text = dirSelected
        }
        
        let app = Reachability.getContext()
        carService = CarritoService(context: app)
        self.carritos = carService!.getAll()
        
    }


    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tapDir: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ProcesarPedidoViewControl.segueAction))
        uiViewLabel.addGestureRecognizer(tapDir)

        // Do any additional setup after loading the view.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ProcesarPedidoViewControl.dismissKeyboard))
        backUiView.addGestureRecognizer(tap)
    }
    
    func segueAction() {
        self.performSegue(withIdentifier: "getDirSegue", sender: self)
    }

    func dismissKeyboard() {
        backUiView.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtTelefono {
            textField.resignFirstResponder()
            txtNombrePedido.becomeFirstResponder()
        }
        if textField == txtNombrePedido{
            textField.resignFirstResponder()
        }
        
        return true
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
    }
    
    func clearFields(){
        self.txtTelefono.text = ""
        self.txtNombrePedido.text = ""
        self.txtDireccionPedido.text = "Dirección"
        Variables.direccionObjGlobal = Direccion()
    }
    
    @IBAction func goingDireccionAction(_ sender: Any) {
        self.performSegue(withIdentifier: "getDirSegue", sender: self)
    }

    @IBAction func goingSendingAction(_ sender: Any) {
        if self.carritos.count >= 1{
            self.datosAEnviar()
        }else{
            Reachability.displayMessage("KOI Sushi", message: "Debe de seleccionar producto")
        }
        
    }
    
    func datosAEnviar(){
        var varBool : Bool = false
        pedidoTelString = txtTelefono.text!
        pedidoTitleString = txtNombrePedido.text!
        
        if let dirtxt = Variables.direccionObjGlobal.detailDir{
            self.pedidoDireccionString = dirtxt
        }
        
        
        let telString = pedidoTelString.trimmingCharacters (
            in: CharacterSet.whitespacesAndNewlines
        )
        
        let titleString = pedidoTitleString.trimmingCharacters (
            in: CharacterSet.whitespacesAndNewlines
        )
        
        if txtDireccionPedido.text == "Dirección"{
            Reachability.displayMessage("KOI Sushi", message: "Debe de Seleccionar una Dirección")
            
        }else if pedidoTelString.isEmpty || pedidoTitleString.isEmpty{
            Reachability.displayMessage("KOI Sushi", message: "Hay algun campo vacio")
            
        }else if telString.characters.count > 8 || telString.characters.count < 8   {
            Reachability.displayMessage("KOI Sushi", message: "Número de teléfono debe de ser de 8 dígitos")
        }else if titleString.characters.count < 5 {
            Reachability.displayMessage("KOI Sushi", message: "Título del pedido debe tener longitud más de 5 dígitos No espacios")
        }else if Reachability.isConnectedToNetwork() == true{
            varBool = true
        }else{
            Reachability.displayMessage("KOI Sushi", message: "No hay conexión a Internet")
        }
        
        if varBool == true{
            
            self.pedidoTelString = telString
            self.pedidoTitleString = titleString
            //self.pedidoDireccionString = itemsCheck[0]
            self.clearFields()
            self.performSegue(withIdentifier: "sendingSegue", sender: self)
        }
        
    }
    
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "sendingSegue"){
            let sendingVC = segue.destination as! SendingJsonViewController
            sendingVC.titlePedido = self.pedidoTitleString
            sendingVC.telefono = self.pedidoTelString
            sendingVC.direccion = self.pedidoDireccionString
            
        }
    }


}
