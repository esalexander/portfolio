//
//  ContactViewController.swift
//  mimaps
//
//  Created by Elias velasco on 8/11/16.
//  Copyright © 2016 Elias velasco. All rights reserved.
//

import UIKit

class ContactViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    let miPlace = ["Plaza Madero", "Las Azaleas","Plaza Futura","Santa Rosa","EVENTOS"]
    let miNumero = ["2246-0193", "2263-7222","2264-4294", "2525-7500","7888-9056"]
    let miImg = [UIImage(named: "plazamaderoicon"),UIImage(named: "asaleasicon"),UIImage(named: "torrefuturaicon"),UIImage(named: "santarosamapa"),UIImage(named: "plazamaderoicon")]
    
    @IBOutlet weak var contactList: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        contactList.dataSource = self
        contactList.delegate = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return miPlace.count
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        //
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = contactList.dequeueReusableCell(withIdentifier: "contactcell", for: indexPath) as! contactCell
        
        cell.titleContact.text = miPlace[indexPath.row]
        cell.subtitleContact.text = miNumero[indexPath.row]
        cell.imgContact.image = miImg [indexPath.row]
        
        cell.contactButton.tag = indexPath.row
        
        cell.contactButton.addTarget(self, action: #selector(ContactViewController.callAction(_:)),for: .touchUpInside)
        
        
        return cell
    }
    
    func compartirAction(){
        let firstActivityItem  = "KOI Sushi"
        let acitivityViewController: UIActivityViewController = UIActivityViewController(activityItems: [firstActivityItem], applicationActivities: nil)
        self.present(acitivityViewController, animated: true, completion: nil)
    }
    
    @IBAction func callAction(_ sender:UIButton){
        let phonePosition = self.miNumero[sender.tag]
        if let url = URL(string: "tel://\(phonePosition)"), UIApplication.shared.canOpenURL(url){
            open(scheme: "tel://\(phonePosition)")
        }else {
            print("No se puede por el dispositivo")
        }
    }
    
    @IBAction func callDeliveryAction(_ sender: Any) {
            open(scheme: "tel://2263-7222")
        
    }
    
    func open(scheme: String) {
        if let url = URL(string: scheme) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:],
                                          completionHandler: {
                                            (success) in
                })
            } else {
                let success = UIApplication.shared.openURL(url)
                print("Open \(scheme): \(success)")
            }
        }
    }

}
