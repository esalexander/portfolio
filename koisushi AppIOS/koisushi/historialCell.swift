//
//  historialCell.swift
//  mimaps
//
//  Created by Elias velasco on 6/12/16.
//  Copyright © 2016 Elias velasco. All rights reserved.
//
import UIKit

class historialCell: UICollectionViewCell {
    
    @IBOutlet weak var imgHist: UIImageView!
    
    @IBOutlet weak var titleHist: UILabel!
    
    @IBOutlet weak var codigoHist: UILabel!
    
    @IBOutlet weak var fechaHist: UILabel!
    
}

