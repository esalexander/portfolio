//
//  getDireccionViewController.swift
//  koisushi
//
//  Created by Elias velasco on 1/3/17.
//  Copyright © 2017 Elias velasco. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class getDireccionViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var procesarTable: UITableView!
    
    var arrRes = [[String:AnyObject]]()
    var direcciones = [Direccion]()
    var items = [String]()
    var itemsCheck = [String]()
    var user = User()
    let preft = UserDefault()
    var direccionObj = Direccion()
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.user = Variables.user
        
        if user.username != nil{
            if Reachability.isConnectedToNetwork() == true{
                fetchDirecciones(user.cod_generado_user)
            }else{
                Reachability.displayMessage("KOI Sushi", message: "No hay conexión a Internet")
            }
            
        }else if UserDefault.isLoggin(preft)() == true{
            let codigoUser = UserDefault.gettingCodigoUser(preft)()
            if codigoUser != "" {
                
                if Reachability.isConnectedToNetwork() == true{
                    fetchDirecciones(codigoUser)
                }else{
                    Reachability.displayMessage("KOI Sushi", message: "No hay conexión a Internet")
                }
            }
        }
        self.procesarTable.register(UITableViewCell.self, forCellReuseIdentifier: "procesarCell")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.procesarTable.delegate = self
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.direcciones.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:UITableViewCell = self.procesarTable.dequeueReusableCell(withIdentifier: "procesarCell")! as UITableViewCell
        cell.accessoryType = UITableViewCellAccessoryType.none
        
        let view = UIView()
        view.backgroundColor = UIColor.lightGray.withAlphaComponent(0.1)
        cell.selectedBackgroundView = view
        direccionObj = self.direcciones[indexPath.row]
        cell.textLabel?.text = direccionObj.titleDir
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath)
        
        if (cell?.accessoryType == UITableViewCellAccessoryType.none){
            cell!.accessoryType = UITableViewCellAccessoryType.checkmark;
            
            direccionObj = self.direcciones[indexPath.row]
            
            itemsCheck.append(direccionObj.detailDir)
            //print("Se agrego " + direccionObj.detailDir)
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath as IndexPath)?.accessoryType = .none
        let direccionObj = self.direcciones[indexPath.row]
        
        if itemsCheck.contains(direccionObj.detailDir) {
            //print("Se elimino " + direccionObj.detailDir)
            itemsCheck.remove(at: 0)
        }
    }
    
    func countCheck(){
        if itemsCheck.count == 1{
            print("Seleccion de un valor \(itemsCheck[0]) ")
        }else{
            print("debe de seleccionar solo una direccion")
        }
    }
    
    func deleteUitableElements(){
        if direcciones.count >= 1{
            self.direcciones.removeAll()
            self.procesarTable.reloadData()
        }
    }

    func clearFields(){
        self.items.removeAll()
        self.itemsCheck.removeAll()
    }
    
    func fetchDirecciones(_ userCodigo: String){
        let urlDatos : String = "URLALSERVIDOR+PUERTO+PARA"
        Alamofire.request(urlDatos).responseJSON { (responseData) -> Void in
            if((responseData.result.value) != nil){
                
                let swiftyJsonVar = JSON(responseData.result.value!)
                //print(swiftyJsonVar)
                if let resData = swiftyJsonVar.arrayObject {
                    self.arrRes = resData as! [[String:AnyObject]]
                }
                if self.arrRes.count > 0 {
                    self.direcciones = [Direccion]()
                    for dirDictionary in self.arrRes {
                        
                        let direccionObj = Direccion()
                        direccionObj.idDir = dirDictionary["idDir"] as! String
                        direccionObj.titleDir = dirDictionary["titleDir"] as! String
                        direccionObj.detailDir = dirDictionary["detailDir"] as! String
                        //let detailDir = dirDictionary["detailDir"] as! String
                        
                        //self.items.append(detailDir)
                        /*if self.itemsCheck.count >= 1 {
                         self.itemsCheck.removeAll()
                         }*/
                        self.direcciones.append(direccionObj)
                        
                    }
                    self.procesarTable.reloadData()
                }
            }
        }
    }
    
    
    @IBAction func goingDireccionAction(_ sender: Any) {
        self.deleteUitableElements()
        if itemsCheck.count >= 1{
            itemsCheck.removeAll()
        }
        self.performSegue(withIdentifier: "addDireccionSeg", sender: self)
    }
    
    @IBAction func goingBTNAction(_ sender: Any) {
        //print("llega aqui")
        self.deleteUitableElements()
        if itemsCheck.count >= 1{
            itemsCheck.removeAll()
        }
        self.performSegue(withIdentifier: "addDireccionSeg", sender: self)
    }
    
    @IBAction func selectDireccion(_ sender: Any) {
        
        
        self.gettingItemCheck()
        /*DispatchQueue.main.async {
            [unowned self] in
            _ = self.navigationController?.popViewController(animated: true)
        }*/
        //self.performSegue(withIdentifier: "addDireccionSeg", sender: self)
    }
    
    func gettingItemCheck(){
        if itemsCheck.count >= 1{
            Variables.direccionObjGlobal = self.direccionObj
            //print(direccionObj.detailDir)
            DispatchQueue.main.async {
                [unowned self] in
                _ = self.navigationController?.popViewController(animated: true)
            }
        }else{
            Reachability.displayMessage("Koi Sushi", message: "Debe de seleccionar una dirección")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "addDireccionSeg"){
            _ = segue.destination as! AgregarDireccionViewController
        }
    }
    

}
