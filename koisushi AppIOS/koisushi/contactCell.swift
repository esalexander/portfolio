//
//  contactCell.swift
//  mimaps
//
//  Created by Elias velasco on 8/11/16.
//  Copyright © 2016 Elias velasco. All rights reserved.
//

import UIKit

class contactCell: UITableViewCell {

    @IBOutlet weak var titleContact: UILabel!
    
    @IBOutlet weak var subtitleContact: UILabel!
    
    @IBOutlet weak var imgContact: UIImageView!
    
    @IBOutlet weak var contactButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
