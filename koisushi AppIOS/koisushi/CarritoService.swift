//
//  CarritoService.swift
//  koisushi
//
//  Created by Elias velasco on 13/12/16.
//  Copyright © 2016 Elias velasco. All rights reserved.
//

import Foundation
import CoreData

class CarritoService {
    var context : NSManagedObjectContext
    // nombre,codigo, descripcion, cantidad, precio
    //
    
    
    init(context : NSManagedObjectContext){
        self.context = context
    }
    
    func create(_ codigo: String, nombre: String, desc: String, cant: NSNumber,precio: NSNumber,coment: String, total: NSNumber) -> Carrito{
        
        let newCarrito = NSEntityDescription.insertNewObject(forEntityName: Carrito.entityName, into: context) as! Carrito
        
        newCarrito.codigo = codigo
        newCarrito.nombre = nombre
        newCarrito.descripcion = desc
        newCarrito.cantidad = cant
        newCarrito.precio = precio
        newCarrito.comentario = coment
        newCarrito.total = total
        
        return newCarrito
    }
    
    func getById(_ id: NSManagedObjectID) -> Carrito?{
        return context.registeredObject(for: id) as? Carrito
    }
    
    func getAll() -> [Carrito]{
        return get(withPredicate : NSPredicate(value:true) )
    }
    
    func get(withPredicate queryPredicate:NSPredicate ) -> [Carrito]{
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: Carrito.entityName)
        
        fetchRequest.predicate = queryPredicate
        
        do{
            let response = try context.fetch(fetchRequest)
            return response as! [Carrito]
        }catch let error as NSError{
            print(error)
            return [Carrito]()
        }
    }
    
    func update(_ updateCarrito: Carrito){
        if let car = getById(updateCarrito.objectID){
            car.cantidad = updateCarrito.cantidad
            car.precio = updateCarrito.precio
            car.total = updateCarrito.total
        }
    }
    
    func contandoRegistros() -> Int{
        let var_cont = self.getAll()
        return var_cont.count
    }
    
    
    func delete(_ id: NSManagedObjectID){
        if let carToDelete = getById(id){
            context.delete(carToDelete)
        }
    }
    
    func deleteAllRequest(){
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: Carrito.entityName)
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        
        do {
            let _ = try context.execute(deleteRequest)
        } catch let error as NSError {
            print(error)
        }
    }
    
    func saveChanges(){
        do{
            try
                context.save()
        }catch let error as NSError{
            print(error)
        }
    }
    
    
    
    
}
