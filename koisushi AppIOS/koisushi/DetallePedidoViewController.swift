//
//  DetallePedidoViewController.swift
//  mimaps
//
//  Created by Elias velasco on 7/12/16.
//  Copyright © 2016 Elias velasco. All rights reserved.
//

import UIKit

class DetallePedidoViewController: UIViewController, UITableViewDataSource, UITableViewDelegate,XMLParserDelegate {
    
    @IBOutlet weak var dataPedido: UITableView!
    var parser = XMLParser()
    var posts = NSMutableArray()
    var elements = NSMutableDictionary()
    var element = NSString()
    var ids = NSMutableString()
    var title1 = NSMutableString()
    var date = NSMutableString()
    var link = NSMutableString()
    var desc = NSMutableString()
    var url = "URLALSERVIDOR+PUERTO+PARA"
    var urlapp = ""
    
    var paramRecived = ""
    var spinner = UIActivityIndicatorView()
    var delay4s = Timer()
    
    var carService: CarritoService?
    var carritoObj: Carrito!
    var p_miproducto: Producto!

    override func viewDidLoad() {
        super.viewDidLoad()

        dataPedido.dataSource = self
        dataPedido.delegate = self
        
        if Reachability.isConnectedToNetwork() == true{
            tiempoEspera()
            let app = Reachability.getContext()
            carService = CarritoService(context: app)
        }else{
            Reachability.displayMessage("KOI Sushi", message: "No hay conexión a Internet")
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)

    }
    
    func tiempoEspera(){
        delay4s = Timer.scheduledTimer(timeInterval: 0.2, target: self, selector: #selector(MenuTBDetalleViewController.dismissDialog), userInfo: nil, repeats: false)
    }
    
    func dismissDialog(){
        //self.alertController.dismiss(animated: true, completion: nil)
        self.beginParsin()
    }
    
    func beginParsin(){
        urlapp = "\(url)\(paramRecived)"
        //print(urlapp)
        posts = []
        parser = XMLParser(contentsOf:(URL(string:urlapp))!)!
        parser.delegate = self
        parser.parse()
        dataPedido!.reloadData()
    }
    
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        element = elementName as NSString
        if (elementName as NSString).isEqual(to: "item") {
            elements = NSMutableDictionary()
            elements = [:]
            
            ids = NSMutableString()
            ids = ""
            
            title1 = NSMutableString()
            title1 = ""
            
            date = NSMutableString()
            date = ""
            
            link = NSMutableString()
            link = ""
            
            desc = NSMutableString()
            desc = ""
        }
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        if element.isEqual(to: "id"){
            ids.append(string)
        }else if element.isEqual(to: "title") {
            title1.append(string)
        } else if element.isEqual(to: "pubDate") {
            date.append(string)
        }else if element.isEqual(to: "desc") {
            desc.append(string)
        }
        else if element.isEqual(to: "link"){
            link.append(string)
        }
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if (elementName as NSString).isEqual(to: "item") {
            if !ids.isEqual(nil){
                elements.setObject(ids, forKey: "id" as NSCopying)
            }
            if !title1.isEqual(nil) {
                elements.setObject(title1, forKey: "title" as NSCopying)
            }
            if !date.isEqual(nil) {
                elements.setObject(date, forKey: "date" as NSCopying)
            }
            if !desc.isEqual(nil) {
                elements.setObject(desc, forKey: "desc" as NSCopying)
            }
            if !link.isEqual(nil){
                elements.setObject(link, forKey: "link" as NSCopying)
            }
            
            posts.add(elements)
        }
    }
    
    func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellpedido = dataPedido.dequeueReusableCell(withIdentifier: "custompedidocell", for: indexPath) as! CustomPedidoCell
        
        cellpedido.titlePedido.text = (posts.object(at: indexPath.row) as AnyObject).value(forKey: "title") as? String
        cellpedido.subTitlePedido.text = (posts.object(at: indexPath.row) as AnyObject).value(forKey: "date") as? String
        
        let strUrl = (posts.object(at: indexPath.row) as AnyObject).value(forKey: "link") as? String
        let trimmedString = strUrl!.trimmingCharacters(
            in: CharacterSet.whitespacesAndNewlines
        )
        let imageUrl = URL(string: trimmedString)
        cellpedido.imgPedido.sd_setImage(with: imageUrl, completed: nil)
        
        return cellpedido
    }
    
    func guardandoLista(){
        if posts.count > 0 {
            
            var i = 0
            repeat {
                //print(i)
                let l_nombre = (posts.object(at: i) as AnyObject).value(forKey: "title") as! String
                let l_codigo = (posts.object(at: i) as AnyObject).value(forKey: "id") as! String
                let l_precio = (posts.object(at: i) as AnyObject).value(forKey: "date") as! String
                let l_strUrl = (posts.object(at: i) as AnyObject).value(forKey: "link") as! String
                let l_cant = (posts.object(at: i) as AnyObject).value(forKey: "desc") as! String
                
                p_miproducto = Producto(nombre: l_nombre, codigo: l_codigo, precio: l_precio, descripcion: "", cantidad: l_cant,link: l_strUrl)
                
                if p_miproducto!.nombre != "" {
                    
                    let precioString = p_miproducto!.precio!.trimmingCharacters (
                        in: CharacterSet.whitespacesAndNewlines
                    )
                    let cantidaString = p_miproducto!.cantidad!.trimmingCharacters (
                        in: CharacterSet.whitespacesAndNewlines
                    )
                    let precioParametro = Double(precioString)!
                    let resultado = (precioParametro * Double(cantidaString)!)
                    
                    
                        let objetoReturn = carService!.create(
                            (p_miproducto!.codigo),
                            nombre: (p_miproducto!.nombre)!,
                            desc: p_miproducto!.descripcion!,
                            cant: NSNumber(value: Int(cantidaString)!),
                            precio: NSNumber(value: precioParametro),
                            coment: "",
                            total: NSNumber(value:resultado)
                        )
                    
                    if objetoReturn.nombre != "" {
                        //clearFields()
                        //self.alertDialog()
                        carService!.saveChanges()
                        
                        
                        
                    }else{
                        print("no se ha guardado los productos")
                    }
                }
                i = i + 1
            } while i < posts.count
            
            DispatchQueue.main.async(execute: { () -> Void in
                
                self.performSegue(withIdentifier: "historialAPedido", sender: self)
            })
        }
    }
    
    @IBAction func historialPedidoAction(_ sender: AnyObject) {
        carService!.deleteAllRequest()
        guardandoLista()
    }
    
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "historialAPedido"){
            _ = segue.destination as! MiPedidoViewController
        }
    }
    
    
}


