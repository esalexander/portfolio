//
//  AgregarDireccionViewController.swift
//  koisushi
//
//  Created by PROGRAMACIÓN on 20/1/17.
//  Copyright © 2017 Elias velasco. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class AgregarDireccionViewController: UIViewController, UITextFieldDelegate {


    @IBOutlet weak var txtTituloDireccion: UITextField!
    @IBOutlet weak var txtDescripDireccion: UITextField!
    
    @IBOutlet weak var btnGuardar: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    var user = User()
    let preft = UserDefault()
    
    var tituloDireccionStr = ""
    var descripDireccionStr = ""
    var codigo = ""
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        txtTituloDireccion.delegate = self
        txtDescripDireccion.delegate = self
        
        self.user = Variables.user
        if user.cod_generado_user != nil{
            if Reachability.isConnectedToNetwork() == true{
                self.codigo = user.cod_generado_user
            }else{
                Reachability.displayMessage("KOI Sushi", message: "No hay conexión a Internet")
            }
            
        }else if UserDefault.isLoggin(preft)() == true{
            let codigoUser = UserDefault.gettingCodigoUser(preft)()
            if codigoUser != "" {
                
                if Reachability.isConnectedToNetwork() == true{
                    self.codigo = codigoUser
                }else{
                    Reachability.displayMessage("KOI Sushi", message: "No hay conexión a Internet")
                }
            }
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        roundCorners()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(DetalleProductoViewController.dismissKeyboard))
        scrollView.addGestureRecognizer(tap)

    }
    
    func roundCorners(){
        self.txtTituloDireccion.layer.cornerRadius = 6
        self.txtDescripDireccion.layer.cornerRadius = 6
        self.btnGuardar.layer.cornerRadius = 6
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtTituloDireccion {
            textField.resignFirstResponder()
            txtDescripDireccion.becomeFirstResponder()
        }else if textField == txtDescripDireccion{
           textField.resignFirstResponder()
        }
        return true
    }
    
    
    func dismissKeyboard() {
        scrollView.endEditing(true)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        //scrollView.setContentOffset(CGPoint(x: 0, y: 2), animated: true)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
    
    func clearFields(){
        self.txtTituloDireccion.text = ""
        self.txtDescripDireccion.text = ""
    }
    
    func validandoCampos(){
        tituloDireccionStr = txtTituloDireccion.text!
        descripDireccionStr = txtDescripDireccion.text!
        
        let title = tituloDireccionStr.trimmingCharacters (
            in: CharacterSet.whitespacesAndNewlines
        )
        
        let desc = descripDireccionStr.trimmingCharacters (
            in: CharacterSet.whitespacesAndNewlines
        )
        
        if tituloDireccionStr.isEmpty ||  descripDireccionStr.isEmpty {
            Reachability.displayMessage("KOI Sushi", message: "Hay algun campo vacio")
        }else{
            self.registerDireccion(titulo: title, descrip: desc)
        }
    }
    
    func registerDireccion(titulo: String, descrip:String){
        
        let urlBase : String = "URLALSERVIDOR+PUERTO+PARA"
        
        
        let parameters : Parameters = [
            "titulodireccion": titulo,
            "descdireccion":descrip,
            "codgenerado": codigo
        ]
        
        Alamofire.request(urlBase,parameters: parameters).responseJSON { (responseData) -> Void in
            if((responseData.result.value) != nil){
                
                let swiftyJsonVar = JSON(responseData.result.value!)
                //print(swiftyJsonVar)
                
                let resultado  = swiftyJsonVar[0]["resultado"].intValue
                //print(resultado)
                if resultado == 0{
                    self.clearFields()
                    Reachability.displayMessage("KOI Sushi", message: "Dirección agregada")
                }else if resultado == 1{
                    Reachability.displayMessage("KOI Sushi", message: "Hubo algun problema al guardar")
                }
            }
        }
    }
    
    @IBAction func savingDireccionAction(_ sender: Any) {
        validandoCampos()
    }

}
