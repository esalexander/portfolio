//
//  Categoria.swift
//  koisushi
//
//  Created by Elias velasco on 24/1/17.
//  Copyright © 2017 Elias velasco. All rights reserved.
//

import UIKit

class Categoria {
    
    var id: String!
    var titulo: String!
    var descripcion: String!
    var image: String!
    
    init(id: String, titulo: String, descripcion:String, image: String){
        self.id = id
        self.titulo = titulo
        self.descripcion = descripcion
        self.image = image
    }
    
    init(categoriasDictionary: [String: AnyObject]){
        self.id = categoriasDictionary["id"] as? String
        self.titulo = categoriasDictionary["titulo"] as! String
        self.descripcion = categoriasDictionary["descripcion"] as! String
        self.image = categoriasDictionary["image"] as! String
    }
    
    static func localAllCategorias() -> [Categoria] {
        var categorias = [Categoria]()
        let jsonFile = Bundle.main.path(forResource: "categorias", ofType: "json")
        let jsonData = try? Data(contentsOf: URL(fileURLWithPath: jsonFile!))
        if let jsonDictionary = NetworkService.parseJSONFromData(jsonData) {
            let categoryDictionaries = jsonDictionary["categorias"] as! [[String : AnyObject]]
            for catDictionary in categoryDictionaries {
                let newCategoria = Categoria(categoriasDictionary: catDictionary)
                categorias.append(newCategoria)
            }
        }
        
        return categorias
    }
}
