//
//  MenuTBViewController.swift
//  mimaps
//
//  Created by Elias velasco on 24/11/16.
//  Copyright © 2016 Elias velasco. All rights reserved.
//

import UIKit

class MenuTBViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    
    @IBOutlet weak var tableMiMenu: UITableView!
    
    var Categorias = [Categoria!]()
    
    var position = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        
        Categorias = Categoria.localAllCategorias()
        
        tableMiMenu.dataSource = self
        tableMiMenu.delegate = self
        
        let hour = Calendar.current.component(.hour, from: Date())
        if hour < 11{
            Reachability.displayMessage("Koi Sushi", message: "Horario de delivery es 11 am a 10 pm")
        }else if hour > 22{
            Reachability.displayMessage("Koi Sushi", message: "Horario de delivery es 11 am a 10 pm")
        }
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Categorias.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableMiMenu.dequeueReusableCell(withIdentifier: "menutbcell", for: indexPath) as! MenuTBViewCell
        
        let Categoria = Categorias[indexPath.row]!
        
        
        cell.titleBackMenu.text = Categoria.titulo!
        cell.subTitleBackMenu.text = Categoria.descripcion!
        cell.imgBackMenu.image = UIImage(named: "\(Categoria.image!)")
        
        return cell
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "goParametro") {
            if let indexPath = getIndexPathForSelectedCell(){
                let category = Categorias[indexPath.row]!
                let detalleMenuVC :MenuTBDetalleViewController = segue.destination as! MenuTBDetalleViewController
                detalleMenuVC.paramRecived = category.id
            }
        }else if (segue.identifier == "goShoping"){
                //let pedidoVC :MiPedidoViewController = segue.destination as! MiPedidoViewController
           _ = segue.destination as! MiPedidoViewController
        }
    }
    
    func getIndexPathForSelectedCell() -> IndexPath? {
        let numero = tableMiMenu.indexPathForSelectedRow!
        return numero
    }
    
    @IBAction func goShoping(_ sender: AnyObject) {
      performSegue(withIdentifier: "goShoping", sender: self)
    }

}
