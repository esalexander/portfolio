//
//  Historial.swift
//  mimaps
//
//  Created by Elias velasco on 6/12/16.
//  Copyright © 2016 Elias velasco. All rights reserved.
//
import UIKit

class historial{
    
    var idPedido:String?
    var fecha: String?
    var hora: String?
    var status : String?
    var nickPedido: String?
    
    init(idPedido: String,fecha: String,hora: String, status:String ,nickPedido: String){
        self.idPedido = idPedido
        self.fecha = fecha
        self.hora = hora
        self.status = status
        self.nickPedido = nickPedido
    }
    
    
    init(historialDictionary: [String: AnyObject]){
        self.idPedido = historialDictionary["idPedido"] as? String
        self.fecha = historialDictionary["fecha"] as? String
        self.hora = historialDictionary ["hora"] as? String
        self.status = historialDictionary["estatus"] as? String
        self.nickPedido = historialDictionary["nickpedido"] as? String
    }
    

    
    static func localAllHistorial() -> [historial] {
        var historiales = [historial]()
        let jsonFile = Bundle.main.path(forResource: "historiales", ofType: "json")
        let jsonData = try? Data(contentsOf: URL(fileURLWithPath: jsonFile!))
        if let jsonDictionary = NetworkService.parseJSONFromData(jsonData) {
            let hystoDictionaries = jsonDictionary["historiales"] as! [[String : AnyObject]]
            for histoDictionary in hystoDictionaries {
                let newHistory = historial(historialDictionary: histoDictionary)
                historiales.append(newHistory)
            }
        }
        
        return historiales
    }
    
    static func gettingHistory() -> [historial] {
        var historiales = [historial]()
        
        let url = URL(string: "URLALSERVIDOR+PUERTO+PARA")!
        URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
            if error != nil {
                return
            }
            
            do{
                let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers)
                //let hystoDictionaries = jsonDictionary["historiales"] as! [[String : AnyObject]]
                for histoDictionary in json as! [[String : AnyObject]]  {
                    let newHistory = historial(historialDictionary: histoDictionary)
                    //print(newHistory.fecha)
                    historiales.append(newHistory)
                }
                print(historiales.count)
                
            }
            catch let jsonError{
                print(jsonError)
            }
        }) .resume()
        
        return historiales
    }
    
    
    
    
}

