//
//  MiPedidoViewController.swift
//  mimaps
//
//  Created by Elias velasco on 9/12/16.
//  Copyright © 2016 Elias velasco. All rights reserved.
//

import UIKit
import CoreData

class MiPedidoViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    
    @IBOutlet weak var subTotalLabel: UILabel!
    @IBOutlet weak var masEnvioLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var procesarBtn: UIButton!
    
    @IBOutlet weak var productTable: UITableView!
    var carritos = [Carrito]()
    var carService: CarritoService?
    var user =  User()
    let preft = UserDefault()
    var checkLoggin : Bool = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.procesarBtn.layer.cornerRadius = 4
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        let app = Reachability.getContext()
        carService = CarritoService(context: app)
        self.completeGetCarritos()
        calcularTotales()
        productTable.reloadData()
        self.logginCheck()
    }
    
    func completeGetCarritos(){
        self.carritos = carService!.getAll()
    }
    
    func logginCheck() {
        
        
        if UserDefault.isLoggin(preft)() == true{
            
            let nombreUser = UserDefault.gettingNameUser(preft)()
            if nombreUser != "" {
                if Reachability.isConnectedToNetwork() == true{
                    self.checkLoggin = true
                }else{
                    Reachability.displayMessage("KOI Sushi", message: "No hay conexión a Internet")
                }
                
            }
        }else if let username = Variables.user.username {
                        if Reachability.isConnectedToNetwork() == true{
                                print(username)
                                self.checkLoggin = true
                        }else{
                            Reachability.displayMessage("KOI Sushi", message: "No hay conexión a Internet")
                        }
            
                }else{
                        //print("la variable esta vacia")
                    //Reachability.displayActionSheet("KOI Sushi", message: "Debe estar logueado para procesar pedido")
                    //Reachability.displayMessage("KOI Sushi", message: "Debe estar logueado para procesar pedido")
                }
        
    }
    
    func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return carritos.count
    }
    
    func calcularTotales(){
        var propina = 0.0
        var subtotal = 0.0
        //var masenvio = 0.0
        var total = 0.0
        if carritos.count > 0 {
            for carro in carritos {
                subtotal = subtotal + Double(carro.total!).roundTo(places: 2)
            }
        }
        propina = (((subtotal/113)*100)*0.10)
        //masenvio = subtotal * 0.10
        total = subtotal + propina
        
        self.subTotalLabel.text = "$\(subtotal.roundTo(places: 2))"
        self.masEnvioLabel.text = "$\(propina.roundTo(places: 2))"
        self.totalLabel.text = "$\(total.roundTo(places: 2))"
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cellproduct = productTable.dequeueReusableCell(withIdentifier: "pedidocell", for: indexPath) as? PedidoCell{
            
            let car = carritos[indexPath.row]
            
            cellproduct.productTitle.text = car.nombre
            let precioText = Double(car.total!).roundTo(places:2)
            cellproduct.txtCantidad.text = String(describing: car.cantidad!)
            cellproduct.productPrecio.text = "$\(precioText)"
            cellproduct.btnRestar.tag = indexPath.row
            cellproduct.btnRestar.layer.cornerRadius = 4
            cellproduct.btnRestar.addTarget(self,action: #selector(MiPedidoViewController.restarAction(_:)), for: .touchUpInside)
            
            cellproduct.btnSumar.tag = indexPath.row
            cellproduct.btnSumar.layer.cornerRadius = 4
            cellproduct.btnSumar.addTarget(self,action: #selector(MiPedidoViewController.sumarAction(_:)), for: .touchUpInside)
            
            cellproduct.btnBorrar.tag = indexPath.row
            cellproduct.btnBorrar.addTarget(self,action: #selector(MiPedidoViewController.borrarAction(_:)), for: .touchUpInside)
            
            return cellproduct
        }else{
                return PedidoCell()
        }
        
    }
    
    @IBAction func restarAction(_ sender: UIButton){
        let car = self.carritos[sender.tag]
        var cnt = Int (car.cantidad!)
        if cnt > 1 {
        cnt = cnt - 1
        let resultado = (Double(car.precio!) * Double(cnt))
        car.cantidad = cnt as NSNumber?
        car.total = resultado as NSNumber?
        carService?.update(car)
        carService?.saveChanges()
        self.carritos = carService!.getAll()
        self.calcularTotales()
        self.productTable.reloadData()
        }
        
    }
    
    @IBAction func sumarAction(_ sender: UIButton){
        let car = self.carritos[sender.tag]
        var cnt = Int (car.cantidad!)
        cnt = cnt + 1
        let resultado = (Double(car.precio!) * Double(cnt))
        car.cantidad = cnt as NSNumber?
        car.total = resultado as NSNumber?
        carService?.update(car)
        carService?.saveChanges()
        self.carritos = carService!.getAll()
        self.calcularTotales()
        self.productTable.reloadData()
    }
    
    @IBAction func borrarAction(_ sender: UIButton){
        let car = self.carritos[sender.tag]
        carService?.delete(car.objectID)
         carService?.saveChanges()
        self.carritos.remove(at: sender.tag)
        self.calcularTotales()
        self.productTable.reloadData()
    }

    @IBAction func goingToProcesar(_ sender: Any) {
        self.completeGetCarritos()
        
        let hour = Calendar.current.component(.hour, from: Date())
        if hour < 11{
            Reachability.displayMessage("KOI SUSHI", message: "Horario de delivery es 11 am a 10 pm")
        }else if hour > 22{
            Reachability.displayMessage("KOI SUSHI", message: "Horario de delivery es 11 am a 10 pm")
        }else if self.carritos.count >= 1 {
                if self.checkLoggin == true {
                DispatchQueue.main.async(execute: { () -> Void in
                    self.performSegue(withIdentifier: "procesarSegue", sender: self)
                })
                }else{
                //Reachability.displayMessage("KOI SUSHI", message: "Debe de loguearse con un Usuario")
                self.openDetailImage()
                }
            }else{
                Reachability.displayMessage("KOI SUSHI", message: "Debe de agregar productos")
            }
        
    }
    
    func openDetailImage(){
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "idLoginVC") as! LoginViewController
        vc.procesarPedido = true
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "procesarSegue"){
           _ = segue.destination as! ProcesarPedidoViewControl
        }
    }
}
