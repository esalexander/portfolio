//
//  RedesSocialesViewController.swift
//  koisushi
//
//  Created by Elias velasco on 24/1/17.
//  Copyright © 2017 Elias velasco. All rights reserved.
//

import UIKit

class RedesSocialesViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func open(scheme: String) {
        if let url = URL(string: scheme) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:],
                                          completionHandler: {
                                            (success) in
                                            //print("Open \(scheme): \(success)")
                })
            } else {
                let success = UIApplication.shared.openURL(url)
                print("Open \(scheme): \(success)")
            }
        }
    }
    
    
    @IBAction func goingFacebook(_ sender: Any) {
        open(scheme:"https://www.facebook.com/KoiSV")
    }
    
    
    @IBAction func goingTwitter(_ sender: Any) {
        open(scheme:"https://www.twitter.com/KoiSushiSV")
    }
    
    @IBAction func goingInstagram(_ sender: Any) {
        open(scheme:"https://www.instagram.com/koisushisv")
    
    }
    
    @IBAction func goingYoutube(_ sender: Any) {
        open(scheme:"https://www.youtube.com/channel/UCsxsgH65C2hN6XnZ4Oonzvw")
    }
    
    @IBAction func goingTripAdvisor(_ sender: Any) {
        open(scheme:"https://www.tripadvisor.com.mx/Restaurant_Review-g294476-d4055946-Reviews-Koi_Sushi-San_Salvador_San_Salvador_Department.html")
    }
    
    
    
    

}
