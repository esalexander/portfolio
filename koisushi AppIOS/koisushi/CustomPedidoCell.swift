//
//  CustomPedidoCell.swift
//  mimaps
//
//  Created by Elias velasco on 7/12/16.
//  Copyright © 2016 Elias velasco. All rights reserved.
//

import UIKit

class CustomPedidoCell: UITableViewCell {
    
    @IBOutlet weak var titlePedido: UILabel!
    
    @IBOutlet weak var subTitlePedido: UILabel!
    
    @IBOutlet weak var imgPedido: UIImageView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
