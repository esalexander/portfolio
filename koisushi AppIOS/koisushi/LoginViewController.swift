//
//  LoginViewController.swift
//  mimaps
//
//  Created by Elias velasco on 6/12/16.
//  Copyright © 2016 Elias velasco. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class LoginViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var txtUsername: UITextField!
    
    @IBOutlet weak var txtUserClave: UITextField!
    
    @IBOutlet weak var switchLogin: UISwitch!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    var userNameString = ""
    var userClaveString = ""
    var procesarPedido : Bool? = false
    var isLoggin: Bool? = false
    var user = User()
    let preft = UserDefault()

    override func viewDidLoad() {
        super.viewDidLoad()
        changedBorder()
        
        txtUsername.delegate = self
        txtUserClave.delegate = self
        
        //NSNotificationCenter.defaultCenter().postNotificationName("keyboardWillShow", object: nil)
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.dismissKeyboard))
        scrollView.addGestureRecognizer(tap)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        if UserDefault.isLoggin(preft)() == true{
            //print("true")
            let nombreUser = UserDefault.gettingNameUser(preft)()
            if nombreUser != "" {
                //print(nombreUser)
                //spinner = self.showModalSpinner()
                //loadingDatos(nombreUser)
                self.performSegue(withIdentifier: "verPerfil", sender: self)
            }
        }/*else if user.username != nil {
            //spinner = self.showModalSpinner()
            //loadingDatos(user.username)
            self.performSegueWithIdentifier("verPerfil", sender: self)
        }else{
            //print("Usuario no logueado")
            self.performSegueWithIdentifier("verPerfil", sender: self)
        }*/
    }
    
    func changedBorder(){
        self.txtUsername.layer.cornerRadius = 6
        self.txtUserClave.layer.cornerRadius = 6
    }
    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtUsername {
            textField.resignFirstResponder()
            txtUserClave.becomeFirstResponder()
        }
        if textField == txtUserClave{
            textField.resignFirstResponder()
            //textField.becomeFirstResponder()
        }
        return true
    }
    
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        scrollView.endEditing(true)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        scrollView.setContentOffset(CGPoint(x: 0, y: 100), animated: true)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
         scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
    
    func clearFields(){
        self.txtUsername.text = ""
        self.txtUserClave.text = ""
    }
    
    
    @IBAction func loginAction(_ sender: AnyObject) {
        userNameString = txtUsername.text!
        userClaveString = txtUserClave.text!
        let userString = userNameString.trimmingCharacters (
            in: CharacterSet.whitespacesAndNewlines
        )
        
        let passString = userClaveString.trimmingCharacters (
            in: CharacterSet.whitespacesAndNewlines
        )
        
        
        if userNameString.isEmpty || userClaveString.isEmpty{
            Reachability.displayMessage("KOI Sushi", message: "Hay algun campo vacio")
        }else{
            if Reachability.isConnectedToNetwork() == true{
                queryServerLogin(user: userString,pass: passString)
            }else{
                Reachability.displayMessage("KOI Sushi", message: "No hay conexión a Internet")
            }
            
        }
    }
    
    func queryServerLogin(user: String, pass:String){
        self.clearFields()
        let parameters : Parameters = [
        "coduserphone": user,
        "pass": pass
        ]
        let url = "URLALSERVIDOR+PUERTO+PARA"
        //debugPrint(parameters)
        Alamofire.request(url,parameters: parameters).responseJSON { (responseData) -> Void in
            if((responseData.result.value) != nil){
            //print("llega aqui")
            let swiftyJsonVar = JSON(responseData.result.value!)
            //print(swiftyJsonVar)
            let codgen = swiftyJsonVar["cod_generado_user"].stringValue
            
            if codgen == "neles"  {
                    Reachability.displayMessage("KOI Sushi", message: "Usuario o Clave erronea")
            }else{
                    let email = swiftyJsonVar["email"].stringValue
                    let phone = swiftyJsonVar["phoneuser"].stringValue
                
                self.user = User()
                self.user.cod_generado_user = codgen
                self.user.username = user
                self.user.diruser = email
                self.user.phoneuser = phone
                Variables.user = self.user
                
                if self.switchLogin.isOn {
                    
                    self.preft.createLogin(self.user.username!,loged: true,codigo: self.user.cod_generado_user!,email: self.user.diruser!)
                    self.isLoggin = true
                    
                    self.goingToPerfil()
                    
                }else{
                   self.goingToPerfil()
                }
            }
          }
        }
        //debugPrint(request)
     
    }
    
    func goingToPerfil(){
        if procesarPedido == true {
            DispatchQueue.main.async {
                [unowned self] in
                _ = self.navigationController?.popViewController(animated: true)
            }
            
        }else{
            DispatchQueue.main.async {
                [unowned self] in
                self.performSegue(withIdentifier: "verPerfil", sender: self)
            }
        }
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "verPerfil"){
            let destino = segue.destination as! MiPerfilViewController
            destino.user = self.user
        }
    }


}
