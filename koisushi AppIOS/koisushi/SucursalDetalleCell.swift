//
//  SucursalDetalleCell.swift
//  mimaps
//
//  Created by Elias velasco on 2/12/16.
//  Copyright © 2016 Elias velasco. All rights reserved.
//

import UIKit

class SucursalDetalleCell: UITableViewCell {

    @IBOutlet weak var horarioTitle: UILabel!
    @IBOutlet weak var horarioSubTitle: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
