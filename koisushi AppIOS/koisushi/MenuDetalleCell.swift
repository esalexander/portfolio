//
//  MenuDetalleCell.swift
//  mimaps
//
//  Created by Elias velasco on 7/12/16.
//  Copyright © 2016 Elias velasco. All rights reserved.
//

import UIKit

class MenuDetalleCell: UITableViewCell {
    
    @IBOutlet weak var textTitle: UILabel!
    
    @IBOutlet weak var textSubtitle: UILabel!
    
    @IBOutlet weak var imgPlato: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.textTitle.alpha = 0.0
        self.textSubtitle.alpha = 0.0
        self.imgPlato.alpha = 0.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
