//
//  MenuTBDetalleViewController.swift
//  mimaps
//
//  Created by Elias velasco on 7/12/16.
//  Copyright © 2016 Elias velasco. All rights reserved.
//

import UIKit
import CoreData

class MenuTBDetalleViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, XMLParserDelegate {
    
    @IBOutlet weak var tbData: UITableView!
    var parser = XMLParser()
    var posts = NSMutableArray()
    var elements = NSMutableDictionary()
    var element = NSString()
    var ids = NSMutableString()
    var title1 = NSMutableString()
    var date = NSMutableString()
    var link = NSMutableString()
    var desc = NSMutableString()
    var url = "URLALSERVIDOR+PUERTO+PARA"
    var urlapp = ""
    
    //var timer: NSTimer?
    var paramRecived: String! = ""
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    var spinner = UIActivityIndicatorView()
    var delay4s = Timer()
    //var spinner = self.showModalSpinner()
    
    var p_miproducto: Producto?

    override func viewDidLoad() {
        super.viewDidLoad()

        tbData.dataSource = self
        tbData.delegate = self
        
        if Reachability.isConnectedToNetwork() == true{
            //showModalSpinner()
            activityIndicator.startAnimating()
            tiempoEspera()
        }else{
            Reachability.displayMessage("KOI Sushi", message: "No hay conexión a Internet")
        }
        
    }
    
    func tiempoEspera(){
        delay4s = Timer.scheduledTimer(timeInterval: 0.2, target: self, selector: #selector(MenuTBDetalleViewController.dismissDialog), userInfo: nil, repeats: false)
    }
    
    func dismissDialog(){
        //hideModalSpinner(spinner)
        //self.alertController.dismiss(animated: true, completion: nil)
        self.beginParsing()
    }

    func beginParsing() {
        /*if paramRecived == 0{
            paramRecived=1
        }else{
            paramRecived += 1
        }*/
        urlapp = "\(url)\(paramRecived!)"
        //print(urlapp)
        posts = []
        parser = XMLParser(contentsOf:(URL(string:urlapp))!)!
        parser.delegate = self
        parser.parse()
        
        activityIndicator.stopAnimating()
        tbData!.reloadData()
        //hideModalSpinner(spinner)
        
    }
    
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        element = elementName as NSString
        if (elementName as NSString).isEqual(to: "item") {
            elements = NSMutableDictionary()
            elements = [:]
            
            ids = NSMutableString()
            ids = ""
            
            title1 = NSMutableString()
            title1 = ""
            
            date = NSMutableString()
            date = ""
            
            link = NSMutableString()
            link = ""
            
            desc = NSMutableString()
            desc = ""
            
        }
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        if element.isEqual(to: "id"){
            ids.append(string)
        }else if element.isEqual(to: "title") {
            title1.append(string)
        } else if element.isEqual(to: "pubDate") {
            date.append(string)
        }else if element.isEqual(to: "desc") {
                desc.append(string)
        }
        else if element.isEqual(to: "link"){
            link.append(string)
        }
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if (elementName as NSString).isEqual(to: "item") {
            if !ids.isEqual(nil){
                elements.setObject(ids, forKey: "id" as NSCopying)
            }
            if !title1.isEqual(nil) {
                elements.setObject(title1, forKey: "title" as NSCopying)
            }
            if !date.isEqual(nil) {
                elements.setObject(date, forKey: "date" as NSCopying)
            }
            if !desc.isEqual(nil) {
                elements.setObject(desc, forKey: "desc" as NSCopying)
            }
            if !link.isEqual(nil){
                elements.setObject(link, forKey: "link" as NSCopying)
            }
            
            posts.add(elements)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : MenuDetalleCell = tableView.dequeueReusableCell(withIdentifier: "menudetallecell", for: indexPath) as! MenuDetalleCell
        
        cell.textTitle.text = (posts.object(at: indexPath.row) as AnyObject).value(forKey: "title") as? String
        cell.textSubtitle.text = (posts.object(at: indexPath.row) as AnyObject).value(forKey: "date") as? String
        
        
        let strUrl = (posts.object(at: indexPath.row) as AnyObject).value(forKey: "link") as? String
        let trimmedString = strUrl!.trimmingCharacters(
            in: CharacterSet.whitespacesAndNewlines
        )
        let imageUrl = URL(string: trimmedString)
        cell.imgPlato.sd_setImage(with: imageUrl){ (image,error,imageCacheType,imageUrl) in
            if image != nil{
                
                UIView.animate(withDuration: 0.4, animations: {
                    cell.textTitle.alpha = 1
                    cell.textSubtitle.alpha = 1
                    cell.imgPlato.alpha = 1
                })
                
                
            }else{
                UIView.animate(withDuration: 0.4, animations: {
                    cell.textTitle.alpha = 1
                    cell.textSubtitle.alpha = 1
                    cell.imgPlato.alpha = 1
                })
                cell.imgPlato.image = UIImage(named:"noimage")
            }
        }
        
    
        return cell
    }
    

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let indexPath = tableView.indexPathForSelectedRow!
        let currentCell = tableView.cellForRow(at: indexPath) as! MenuDetalleCell
        let l_codigo = (posts.object(at: indexPath.row) as AnyObject).value(forKey: "id") as! String
        let l_precio = (posts.object(at: indexPath.row) as AnyObject).value(forKey: "date") as! String
        let l_strUrl = (posts.object(at: indexPath.row) as AnyObject).value(forKey: "link") as! String
        let l_desc = (posts.object(at: indexPath.row) as AnyObject).value(forKey: "desc") as! String
        
        if let l_nombre = currentCell.textTitle.text{
            
            p_miproducto = Producto(nombre: l_nombre, codigo: l_codigo, precio: l_precio, descripcion: l_desc, cantidad: "0",link: l_strUrl)
            
        } else {
            print("One or more of the optionals don’t contain a value")
        }

        performSegue(withIdentifier: "productSeg", sender: self)
    }

    


    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "productSeg"){
            let detalleproducto = segue.destination as! DetalleProductoViewController
            detalleproducto.l_miproducto = p_miproducto
        }
    }


}
