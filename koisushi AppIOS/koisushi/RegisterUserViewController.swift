//
//  RegisterUserViewController.swift
//  mimaps
//
//  Created by Elias velasco on 6/12/16.
//  Copyright © 2016 Elias velasco. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class RegisterUserViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var userNickname: UITextField!
    @IBOutlet weak var userEmail: UITextField!
    @IBOutlet weak var userPassword: UITextField!
    
    @IBOutlet weak var scrollView: UIScrollView!
    var toolBar = UIToolbar()
    
    var varVerifyUser : Bool! = false
    
    var str_coduser: String = ""
    var str_nameuser: String = ""
    var str_correo : String = ""
    var str_pass : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        userName.delegate = self
        userNickname.delegate = self
        userEmail.delegate = self
        userPassword.delegate = self
        
        if Reachability.isConnectedToNetwork() == true{
            
        }else{
            Reachability.displayMessage("KOI Sushi", message: "No hay conexión a Internet")
        }
        
        toolBar = UIToolbar().ToolbarPiker(mySelect: #selector(RegisterUserViewController.dismissPicker))
        
        
        changedBorder()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(RegisterUserViewController.dismissKeyboard))
        scrollView.addGestureRecognizer(tap)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        scrollView.setContentOffset(CGPoint(x: 0, y: 40), animated: true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == userNickname {
            textField.resignFirstResponder()
            userName.becomeFirstResponder()
        }else if textField == userName{
            textField.resignFirstResponder()
            userEmail.becomeFirstResponder()
        }else if textField == userEmail{
            
                textField.resignFirstResponder()
                userPassword.becomeFirstResponder()
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag == 3{
            self.gettingValues()
        }
        scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
    
    
    
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        scrollView.endEditing(true)
    }
    
    func changedBorder(){
        let border = 6
        self.userNickname.layer.cornerRadius = CGFloat(border)
        self.userName.layer.cornerRadius = CGFloat(border)
        self.userEmail.layer.cornerRadius = CGFloat(border)
        self.userPassword.layer.cornerRadius = CGFloat(border)
    }
    
    func gettingValues(){
        
        self.str_coduser = userNickname.text!
        self.str_correo = userEmail.text!
        
        if isValidEmail(self.str_correo) == true && self.str_coduser != "" {
            let codtemp = str_coduser.trimmingCharacters (
                in: CharacterSet.whitespacesAndNewlines
            )
            
            let ematemp = str_correo.trimmingCharacters (
                in: CharacterSet.whitespacesAndNewlines
            )
            let parametersValid : Parameters = [
                "coduserphone":codtemp,
                "mailuser": ematemp]
            self.verifyUser(parameters: parametersValid)
            
        }else{
            Reachability.displayMessage("KOI Sushi", message: "Email invalido o codusuario vacio")
        }
    }
    
    func validandoCampos(){
        
        str_coduser = userNickname.text!
        str_nameuser = userName.text!
        str_correo = userEmail.text!
        str_pass = userPassword.text!
        
        if  str_nameuser != "" && str_pass != "" {
            //if isValidEmail(self.str_correo) == true{
               
                let cod = self.str_coduser.trimmingCharacters (
                    in: CharacterSet.whitespacesAndNewlines
                )
                
                let nam = self.str_nameuser.trimmingCharacters (
                    in: CharacterSet.whitespacesAndNewlines
                )
                
                let ema = self.str_correo.trimmingCharacters (
                    in: CharacterSet.whitespacesAndNewlines
                )
                
                let pas = self.str_pass.trimmingCharacters (
                    in: CharacterSet.whitespacesAndNewlines
                )
                
            
                let parameters : Parameters = [
                    "coduser": cod,
                    "nameuser": nam,
                    "userphoneid": "",
                    "mailuser": ema,
                    "pass": pas,
                    "tipo_phone":"3"
                ]
                if self.varVerifyUser == false{
                    self.clearFields()
                    self.registerUsuario(parameters: parameters )
                }else{
                    
                    Reachability.displayMessage("KOI Sushi", message: "Email o usuario ya registrado")
                }
                
            /*}else{
                Reachability.displayMessage("KOI Sushi", message: "Email es invalido")
            }*/
        }else{
                Reachability.displayMessage("KOI Sushi", message: "Hay algun campo vacio")
        }
    }
    
    func verifyUser(parameters : Parameters) {
        let urlBase : String = "URLALSERVIDOR+PUERTO+PARA"
        
        Alamofire.request(urlBase, parameters: parameters).responseJSON { (responseData) -> Void in
            if((responseData.result.value) != nil){
                
                let swiftyJsonVar = JSON(responseData.result.value!)
                
                let resultado  = swiftyJsonVar["resultado"].intValue
                
                if resultado == 1 {
                    self.clearFields()
                    Reachability.displayMessage("KOI Sushi", message: "Email o usuario ya registrado")
                    self.varVerifyUser = true
                }else{
                    self.varVerifyUser = false
                }
            }
        }
    }
    
    
    func registerUsuario(parameters : Parameters){
        let urlBase : String = "URLALSERVIDOR+PUERTO+PARA"
        
        
        Alamofire.request(urlBase, parameters: parameters).responseJSON { (responseData) -> Void in
            if((responseData.result.value) != nil){
                
            let swiftyJsonVar = JSON(responseData.result.value!)
            //print(swiftyJsonVar)
            
            let resultado  = swiftyJsonVar["resultado"].intValue
                if resultado == 1 {
                    Reachability.displayMessage("KOI Sushi", message: "Registro Exitoso")
                }else{
                    Reachability.displayMessage("KOI Sushi", message: "Hubo un problema al registrar usuario")
                }
            }
        }
    }
    
    func isValidEmail(_ testStr: String) -> Bool {
        //print(testStr)
        do {
            let regex = try NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
            return regex.firstMatch(in: testStr, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, testStr.characters.count)) != nil
        } catch {
            return false
        }
    }
    
    func dismissPicker() {
        self.view.endEditing(true)
    }

    @IBAction func userRegisterAction(_ sender: AnyObject) {
        if Reachability.isConnectedToNetwork() == true{
            self.validandoCampos()
        }else{
            Reachability.displayMessage("KOI Sushi", message: "No hay conexión a Internet")
        }
        
    }
    
    @IBAction func userCleanAction(_ sender: AnyObject) {
        clearFields()
    }
    
    func clearFields(){
        self.userName.text = ""
        self.userNickname.text = ""
        self.userEmail.text = ""
        self.userPassword.text = ""
    }

    

}
