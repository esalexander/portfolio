//
//  PedidoCell.swift
//  mimaps
//
//  Created by Elias velasco on 9/12/16.
//  Copyright © 2016 Elias velasco. All rights reserved.
//

import UIKit

class PedidoCell: UITableViewCell {
    
    @IBOutlet weak var productTitle: UILabel!
    @IBOutlet weak var productPrecio: UILabel!
    
    @IBOutlet weak var btnRestar: UIButton!
    @IBOutlet weak var txtCantidad: UITextField!
    @IBOutlet weak var btnSumar: UIButton!

    @IBOutlet weak var btnBorrar: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    @IBAction func restarBtnAction(_ sender: AnyObject) {
    
    }
    
    @IBAction func sumarBtnAction(_ sender: AnyObject) {
    
    }
    
    @IBAction func borrarBtnAction(_ sender: AnyObject) {
    
    }


}
