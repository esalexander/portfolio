//
//  Carrito+CoreDataProperties.swift
//  koisushi
//
//  Created by Elias velasco on 16/12/16.
//  Copyright © 2016 Elias velasco. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Carrito {

    @NSManaged var cantidad: NSNumber?
    @NSManaged var codigo: String?
    @NSManaged var descripcion: String?
    @NSManaged var nombre: String?
    @NSManaged var precio: NSNumber?
    @NSManaged var comentario: String?
    @NSManaged var total: NSNumber?

}
