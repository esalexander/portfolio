<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Bocadeli</title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/cover.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
  <!--<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script> -->

   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    
</head>
<body>
 <div class="site-wrapper">

      <div class="site-wrapper-inner">

        <div class="cover-container">

    <nav class="navbar navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#"><img src="<?php echo base_url(); ?>assets/img/logo.png" class="img-responsive" width="60px" height="60px" alt="" /></a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav pull-right">
            <li><a href="?php echo base_url(); ?>"><span class="glyphicon glyphicon-home"></span> INICIO</a></li>
            <li><a href="<?php echo base_url(); ?>participantes/action"><span class="glyphicon glyphicon-comment"></span> Participar</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>


          <div class="inner cover">
            <h1 class="cover-heading">Promocion Bocadeli</h1>
            <p class="lead">Verifica tu codigo de sticker que aparece dentro de tus empaques de los productos participantes... Ven y Juega</p>
            <p class="lead">
              <!--<a href="#" class="btn btn-lg btn-default">Learn more</a>-->
              <button type="button" style="margin-bottom: 15px;" data-toggle="modal" data-target="#userModal" class="btn btn-info btn-md">
                                 Verificar
                                </button>
            </p>
          </div>

          <div class="mastfoot">
            <div class="inner">
              <p>hecho por <a href="http://lafabricadbm.com">LaFabricaDBM</a></p>
            </div>
          </div>

        </div>

      </div>

    </div>

     <!--Bootstrap core JavaScript 
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>-->
</body>
</html>