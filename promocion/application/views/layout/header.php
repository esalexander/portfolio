<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Bocadeli</title>
    <!--<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/cover.css">-->
	  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style.css">
	  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
    
</head>

  <body>

    <nav class="navbar navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">BOCADELI</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="?php echo base_url(); ?>"><span class="glyphicon glyphicon-home"></span> INICIO</a></li>
            <li><a href="<?php echo base_url(); ?>participantes/action"><span class="glyphicon glyphicon-comment"></span> Participar</a></li>
            <li><a href="<?php echo base_url(); ?>participantes/fetch_users"><span class="glyphicon glyphicon-list"></span> PARTICIPANTES</a></li>
            <li><a href="<?php echo base_url(); ?>auth/create_user"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
            <li><a href="<?php echo base_url(); ?>auth/login"><span class="glyphicon glyphicon-log-in"></span> LOGIN</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <div class="container" style="margin: auto; max-width: 900px;">
     <div class="row col-lg-6 col-md-6 col-lg-offset-3" style=" padding: 40px 15px; text-align: center;">