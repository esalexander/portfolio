<div id="userModal" class="modal fade">  
      <div class="modal-dialog">  
           <form method="post" id="user_form">  
                <div class="modal-content">  
                     <div class="modal-header">  
                          <button type="button"  class="close" data-dismiss="modal">&times;</button>  
                          <h4 class="modal-title">Ingreso de Usuario</h4>  
                     </div>  
                     <div class="modal-body">  

                    <div class="col-md-6 col-xs-12">
                      <div class="form-group">   
                          <label class="control-label">CODIGO</label>  
                          <input type="text" name="codigo" id="codigo" class="form-control" />  
                        </div>

                      <div class="form-group">
                          <label class="control-label">NOMBRE</label>  
                          <input type="text" name="nombre" id="nombre" class="form-control" />  
                        </div>

                        <div class="form-group">   
                          <label class="control-label">APELLIDO</label>  
                          <input type="text" name="apellido" id="apellido" class="form-control" />  
                        </div>
                    </div>


                     <div class="col-md-6 col-xs-12">
                      <div class="form-group">   
                          <label class="control-label">TELEFONO</label>  
                          <input type="text" name="telefono" id="telefono" class="form-control" />  
                        </div>

                      <div class="form-group">
                          <label class="control-label">EMAIL</label>  
                          <input type="email" name="email" id="email" class="form-control" />  
                        </div>

                        <div class="form-group" style="margin: 35px">
                          <input type="submit" style="margin-top: -1px" name="action" id="action" class="btn btn-success" value="Agregar" />  
                          <button type="button" class="btn btn-default" data-dismiss="modal">cerrar</button>  
                          <input type="hidden" name="user_id" id="user_id" />  
                          <input type="hidden" name="do" id="do" value="Do" />
                        </div>
                     </div>

                          
                     </div>  
                     <div class="modal-footer">
                          <label div="respuesta" class="control-label">*<strong>Todos</strong> los campos son requeridos...</label> 
                              
                     </div>  
                </div>  
           </form>  
      </div>  
 </div>  


 <script type="text/javascript" language="javascript" > 
 $(document).ready(function(){  
    $(document).on('submit', '#user_form', function(event){  
           event.preventDefault();  
           var codigo = $('#codigo').val();  
           var nombre = $('#nombre').val(); 
           var apellido = $('#apellido').val();  
           var tel = $('#telefono').val(); 
           var email = $('#email').val();
           if(codigo != '' && nombre != '' && apellido != '' && tel != '' email !='')  
           {  
              $.ajax({  
                    url:"http://localhost/promocion/participantes/action",   
                     method:'POST',  
                     data:new FormData(this),  
                     contentType:false,  
                     processData:false,  
                     success:function(data)  
                     {  
                          alert(data);  
                          //$('#user_form')[0].reset();  
                          //$('#userModal').modal('hide'); 
                     }  
                }); 
           }  
           else  
           {  
                alert("Los campos son requeridos");  
           }  
      }); 
 });  
 </script> 