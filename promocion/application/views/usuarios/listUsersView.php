<h1><?php echo "Lista de Usuario";?></h1>
<p><?php echo "Usuarios los cuales dse han registrado y validado su sticker...";?></p>

<table class="table table-hover table-responsive">
	<tr>
		<th><?php echo 'Nombre';?></th>
		<th><?php echo 'Apellido';?></th>
		<th><?php echo 'Email';?></th>
		<th><?php echo 'Tel';?></th>
		<th><?php echo 'codigo';?></th>
	</tr>
	<?php foreach ($users as $user):?>
		<tr>
            <td><?php echo htmlspecialchars($user->nombre,ENT_QUOTES,'UTF-8');?></td>
            <td><?php echo htmlspecialchars($user->apellido,ENT_QUOTES,'UTF-8');?></td>
            <td><?php echo htmlspecialchars($user->email,ENT_QUOTES,'UTF-8');?></td>
			<td><?php echo htmlspecialchars($user->telefono,ENT_QUOTES,'UTF-8');?></td>
			<td><?php echo htmlspecialchars($user->codigo,ENT_QUOTES,'UTF-8');?></td>
		</tr>
	<?php endforeach;?>
</table>
