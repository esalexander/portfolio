<?php 
$attributes = array('class' => 'form-signin');

 ?>

<ul class="breadcrumb">
<h1><?php echo 'Registra Tu Codigo';?></h1>
<p><?php echo 'Bocadeli';?></p>

<div id="infoMessage"><?php echo validation_errors(); ?></div>

<?php echo form_open('participantes/action',$attributes);?> 

                    <div class="col-md-6 col-xs-12">
                      <div class="form-group">
                          <label class="control-label">NOMBRE</label>  
                          <input type="text" name="nombre" id="nombre" class="form-control" />  
                        </div>

                        <div class="form-group">   
                          <label class="control-label">APELLIDO</label>  
                          <input type="text" name="apellido" id="apellido" class="form-control" />  
                        </div>
                    </div>

                     <div class="col-md-6 col-xs-12">
                      <div class="form-group">   
                          <label class="control-label">TELEFONO</label>  
                          <input type="text" name="telefono" id="telefono" class="form-control" />  
                        </div>

                      <div class="form-group">
                          <label class="control-label">EMAIL</label>  
                          <input type="email" name="email" id="email" class="form-control" />  
                      </div>
                     </div>
                     
                     <div class="col-md-12 col-xs-12">
                       <div class="form-group">   
                          <label class="control-label">CODIGO</label>  
                          <input type="text" name="codigo" id="codigo" class="form-control" />  
                        </div>
                     </div>

                          <label div="respuesta" class="control-label">*<strong>Todos</strong> los campos son requeridos...</label> 
  
<p><?php echo form_submit('submit', 'Enviar',"class='btn btn-primary'");?></p>

<?php echo form_close();?>

</ul>