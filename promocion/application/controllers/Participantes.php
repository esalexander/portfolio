<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Participantes extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		
	}

	function index(){
		echo 'Bienvenido';
	}

	public function setupUI()
	{
		$this->load->view('layout/header.php');
		$this->load->view('registerView.php');
		$this->load->view('layout/footer.php');
	}

	function action(){
		$this->form_validation->set_rules('nombre','nombre','trim|required');
		$this->form_validation->set_rules('apellido','apellido','trim|required');
		$this->form_validation->set_rules('telefono','telefono','trim|required|min_length[8]|max_length[9]');
		$this->form_validation->set_rules('email','email','trim|required|valid_email');
		$this->form_validation->set_rules('codigo','codigo','trim|required|min_length[5]|max_length[7]');
		
		if ($this->form_validation->run()) {
			$codigo = $this->input->post('codigo');
			$data = array(
					'nombre' => $this->input->post('nombre'),
					'apellido' => $this->input->post('apellido'),
					'telefono'=>$this->input->post('telefono'),
					'email'=>$this->input->post('email'),
					'codigo' => $codigo
			 );

			$this->load->model("participantes_model");
			$this->participantes_model->insert_participante($data);
			$this->rewards($codigo);		//redirect(base_url());
			//var_dump($data);
		}else{
			$this->setupUI();
		}
		
	}

	function fetch_users()
	{
		$this->load->model("participantes_model");
		$data ['users']= $this->participantes_model->fetch_participante();
		$this->load->view('layout/header.php');
		$this->load->view('usuarios/listUsersView.php',$data);
		$this->load->view('layout/footer.php');
	}

	function rewards($codigo)
	{
		$this->load->model("participantes_model");
		$data ['rewards'] = $this->participantes_model->check_reward($codigo);
		$this->load->view('layout/header.php');
		$this->load->view('usuarios/rewardResultView.php',$data);
		$this->load->view('layout/footer.php');
	}


}