<?php

Class participantes_model extends CI_Model
{
	var $table= "participantes";

	function insert_participante($data)
	{
		$this->db->insert($this->table, $data);
	}	

	function fetch_participante()
	{
		$query = $this->db->get($this->table);
		return $query->result();
	}

	function check_reward($codigo)
	{
		$this->db->where('codigo',$codigo);
		$this->db->from('premios');
		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			return $query->result();
		}else{
			return False;
		}
	}
}