<!---------------------------------------------------------------------------
Example client script for JQUERY:AJAX -> PHP:MYSQL example
---------------------------------------------------------------------------->

<html>
  <head>
  <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <script language="javascript" type="text/javascript" src="jquery.js"></script>
	<link rel="stylesheet" type="text/css" media="all" href="css/style.css" />
  </head>
  <body>
  
  <div class="container">

  <div class="rigthpanel">
  <div class="title">TURNO</div>
  <div id="output" class="output">0</div>
		<script>
		$(document).ready(function() {
			setInterval("ajaxcall()",2000);
		});

		function ajaxcall() { 
		 $.ajax({
		 data: "",
		 type:'POST',
		 url: "api.php",
		 dataType: 'json',
		 success: function(response){
			 var json_object = JSON.parse(response);
			 $('#output').text(""+response);
		 }
		 });
		 }
		 
		 </script>
  </div>
  <div class="footer"></div>
  
  </div>

  </body>
</html>